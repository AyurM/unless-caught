class StringChoiceEvents {
  //Описания событий, предлагающих выбор из нескольких вариантов

  //СОБЫТИЯ, СВЯЗАННЫЕ С НАВЫКАМИ
  static const String drivingChoiceEvent01 =
      "Здесь неподалеку можно прилично срезать через несколько узких проулков";
  static const String driving01InstantButtonText = "Ехать прямо";
  static const String driving01InstantResult =
      "Мы вроде не особо торопимся, да и тачку царапать не хочется";
  static const String driving01SkillResult01 =
      "Водитель успешно срезал, выиграв нам немного времени";
  static const String driving01SkillResult02 =
      "Продираясь окольными путями, водитель поцарапал машину";

  static const String drivingChoiceEvent02 =
      "В тачке есть кнопка ускорения. Может, стоит прокатиться с ветерком?";
  static const String driving02InstantButtonText = "Игнорировать";
  static const String driving02InstantResult = "Тише едешь - дальше будешь";
  static const String driving02SkillResult01 =
      "Отлично прокатились! Кое-кто со страху даже пристегнулся";
  static const String driving02SkillResult02 =
      "Водитель не справился с управлением и прилично помял крыло";

  static const String lockpickChoiceEvent01 =
      "Здесь какой-то хитрый замок. Говорят, можно по-быстрому проморозить его жидким азотом";
  static const String lockpick01InstantButtonText = "Игнорировать";
  static const String lockpick01InstantResult =
      "Сейчас не время экспериментировать";
  static const String lockpick01SkillResult01 =
      "Замок и впрямь не выдержал и сломался";
  static const String lockpick01SkillResult02 =
      "Азот на замок не подействовал. Теперь вскрывать его придется еще дольше";

  static const String lockpickChoiceEvent02 =
      "Взломщик хочет опробовать \"суперотмычку\", которую он недавно прикупил у приятеля";
  static const String lockpick02InstantButtonText = "Отказать";
  static const String lockpick02InstantResult = "Действуем по стандарту";
  static const String lockpick02SkillResult01 =
      "Получилось даже быстрее, чем ключом!";
  static const String lockpick02SkillResult02 =
      "\"Джеймсбондовский\" девайс сломался и застрял в замке";

  static const String hackingChoiceEvent01 =
      "На терминале сработала защита, через несколько секунд включится сигнализация";
  static const String hacking01InstantButtonText = "Убегать";
  static const String hacking01InstantResult =
      "Пришлось в спешке уходить, зато все обошлось";
  static const String hacking01SkillResult01 =
      "Хакер вовремя успел отключить тревогу";
  static const String hacking01SkillResult02 =
      "Тревогу отключить не получилось, мы еле унесли ноги";

  static const String hackingChoiceEvent02 =
      "Хакер говорит, что можно попытаться взломать и другие терминалы в этой локальной сети";
  static const String hacking02InstantButtonText = "Отказаться";
  static const String hacking02InstantResult =
      "Пока что ограничимся синицей в руке";
  static const String hacking02SkillResult01 =
      "Уязвимости нашлись, и мы стали немного богаче";
  static const String hacking02SkillResult02 =
      "Фаервол обнаружил вторжение и заблокировал доступ";

  static const String gunsChoiceEvent01 =
      "Обстановка резко накалилась, и теперь против нас несколько вооруженных типов";
  static const String guns01InstantButtonText = "Отступить";
  static const String guns01InstantResult = "Не хватало еще на пулю напороться";
  static const String guns01SkillResult01 =
      "Стрелок подстрелил двоих, и остальные разбежались";
  static const String guns01SkillResult02 =
      "Одного из наших ранили, и мы предпочли отступить";

  static const String gunsChoiceEvent02 =
      "Один из них начал кричать, что вызывает копов. Пуля в ноге может помочь ему передумать";
  static const String guns02InstantButtonText = "Сбежать";
  static const String guns02InstantResult =
      "Не хочется лишний раз пускать в ход оружие";
  static const String guns02SkillResult01 =
      "Один меткий выстрел вернул ситуацию под наш контроль";
  static const String guns02SkillResult02 =
      "Стрелок промахнулся, и жертва скрылась за укрытием. Нужно сворачиваться";

  static const String speechChoiceEvent01 =
      "Кажется, свидетель запомнил наши лица. Надо бы что-то предпринять";
  static const String speech01InstantButtonText = "Уйти";
  static const String speech01InstantResult =
      "Не будем показываться в этом районе какое-то время";
  static const String speech01SkillResult01 =
      "Свидетелю весьма убедительно внушили, что нам известно, кто он и где живет. Проблем он не создаст";
  static const String speech01SkillResult02 =
      "Угрозы на свидетеля не подействовали и он поднял шум";

  static const String speechChoiceEvent02 =
      "Вон из того простофили, похоже, можно выудить нужные нам сведения";
  static const String speech02InstantButtonText = "Игнорировать";
  static const String speech02InstantResult =
      "Придерживаемся плана";
  static const String speech02SkillResult01 =
      "Вежливость - лучшее оружие вора. Мы получили важную информацию без всяких подозрений";
  static const String speech02SkillResult02 =
      "Этот тип оказался не так прост. Он что-то заподозрил, нам бы поторопиться";
}
