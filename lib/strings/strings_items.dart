class StringsItems{
  static const String lockpickItem01 = "Стетоскоп";
  static const String lockpickItem02 = "Отмычка";
  static const String lockpickItem03 = "X-Ray очки";

  static const String drivingItem01 = "Кожаная куртка";
  static const String drivingItem02 = "AR-очки";
  static const String drivingItem03 = "Бортовой AI";

  static const String gunsItem01 = "Бронежилет";
  static const String gunsItem02 = "Компенсатор отдачи";
  static const String gunsItem03 = "Система прицеливания";

  static const String hackingItem01 = "Карта памяти";
  static const String hackingItem02 = "Глушитель сигнала";
  static const String hackingItem03 = "Кибердека";

  static const String speechItem01 = "Психология для чайников";
  static const String speechItem02 = "Деловой костюм";
  static const String speechItem03 = "Анализатор речи";

  static const String commonItem01 = "Перчатки";
}