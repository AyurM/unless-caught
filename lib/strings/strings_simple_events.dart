class StringSimpleEvents{
  //ОПИСАНИЯ СОБЫТИЙ НАВЫКОВ ВО ВРЕМЯ МИССИЙ
  //Положительные события
  static const String drivingGoodEvent01 =
      "Водитель успешно срезал пару углов по дороге к цели";
  static const String drivingGoodEvent02 =
      "Заметив хвост, водитель успешно оторвался от слежки";
  static const String drivingGoodEvent03 =
      "Странно, но на всех встреченных светофорах горел зеленый свет";

  static const String lockpickGoodEvent01 =
      "На вскрытом замке даже не осталось следов взлома";
  static const String lockpickGoodEvent02 =
      "Замок оказался сложнее, чем ожидалось, но взломщик свое дело сделал";
  static const String lockpickGoodEvent03 =
      "Такие замки наш взломщик \"видел уже тыщу раз\"";

  static const String hackingGoodEvent01 =
      "Бумажка с паролем была приклеена на монитор";
  static const String hackingGoodEvent02 =
      "Обновления антивирусных баз были отключены. Наш хакер был не против";
  static const String hackingGoodEvent03 =
      "На взломанном терминале нашлась ценная информация";

  static const String gunsGoodEvent01 =
      "При виде оружия \"клиенты\" как-то сразу притихли";
  static const String gunsGoodEvent02 =
      "Кое-кто из них решил покорчить из себя героя. Наш стрелок метким выстрелом показал, что это плохая идея";
  static const String gunsGoodEvent03 =
      "В завязавшейся перестрелке мы одержали уверенную победу";

  static const String speechGoodEvent01 =
      "Жертву удалось заболтать настолько, что она даже не поняла, что что-то не так";
  static const String speechGoodEvent02 =
      "Бедолага, запуганный нашими угрозами, выложил ценные сведения";
  static const String speechGoodEvent03 =
      "Наш человек каким-то образом убедил их, что он и есть полиция";

  //Отрицательные события
  static const String drivingBadEvent01 =
      "Мы вляпались в небольшое ДТП, но обошлось без копов. Правда, пришлось дать немного денег";
  static const String drivingBadEvent02 =
      "Эта колымага заглохла прямо посреди дороги";
  static const String drivingBadEvent03 =
      "Неслись по каким-то чертовым ухабам. Тачка немного пострадала";

  static const String lockpickBadEvent01 = "Замок заклинило. Кое-как выломали";
  static const String lockpickBadEvent02 =
      "Отмычка сломалась и кусок застрял в замке. Пришлось ломать";
  static const String lockpickBadEvent03 =
      "Замок оказался нашему взломщику не по зубам. Хорошо, что хоть горелку захватили";

  static const String hackingBadEvent01 =
      "Фаервол обойти не удалось, терминал заблокирован";
  static const String hackingBadEvent02 =
      "На этом терминале биометрическая защита. Разве что силой вскрывать";
  static const String hackingBadEvent03 =
      "Здесь довольно серьезный антивирусный софт, придется попотеть";

  static const String gunsBadEvent01 =
      "Осечка случилась в самый неподходящий момент. Мы чудом уцелели";
  static const String gunsBadEvent02 =
      "Стрелок не смог достать их за укрытием. Пришлось уходить с тем, что успели взять";
  static const String gunsBadEvent03 =
      "Один из наших подставился под пулю. Бронежилет выручил, но ребро, похоже, сломано";

  static const String speechBadEvent01 =
      "Отвлечь внимание разговорами не удалось. Поступим по-плохому...";
  static const String speechBadEvent02 =
      "Один из наших обратился к другому по настоящему имени. Будем надеяться, все обойдется";
  static const String speechBadEvent03 =
      "\"Клиент\" попался чересчур проницательный - на болтовню не купился. Пришлось импровизировать";

  //ОПИСАНИЯ СОБЫТИЙ, ЗАВИСЯЩИХ ОТ ТИПА МИССИИ
  //Положительные события
  static const String drivingLockpickGoodEvent01 = "В бардачке нашлась заначка";
  static const String drivingLockpickGoodEvent02 = "В багажнике было ценное барахло";

  static const String drivingHackingGoodEvent01 =
      "Банкомат оказался под завязку набит деньгами";
  static const String drivingHackingGoodEvent02 =
      "Камера наблюдения за банкоматом оказалась неисправна";

  static const String drivingGunsGoodEvent01 =
      "В фуре даже больше груза, чем мы рассчитывали";
  static const String drivingGunsGoodEvent02 =
      "Здесь более ценный груз, чем ожидалось";

  static const String drivingSpeechGoodEvent01 =
      "В панике водитель даже не успел позвать копов";
  static const String drivingSpeechGoodEvent02 =
      "Тачка в отличном состоянии, без единой царапины, да и салон роскошный";

  static const String lockpickHackingGoodEvent01 =
      "В сейфе гораздо больше добра, чем мы думали";
  static const String lockpickHackingGoodEvent02 =
      "В сейфе нашлась целая куча драгоценностей";

  static const String lockpickGunsGoodEvent01 =
      "Похоже, сегодня у этого ломбарда отличная выручка!";
  static const String lockpickGunsGoodEvent02 =
      "Помимо того, что было в кассе, мы захватили кое-какого барахлишка";

  static const String lockpickSpeechGoodEvent01 =
      "Касса магазина прямо таки набита деньгами!";
  static const String lockpickSpeechGoodEvent02 =
      "Помимо того, что было в кассе, мы захватили кое-что с витрин";

  static const String gunsHackingGoodEvent01 =
      "Бумажниками посетителей мы брезговать не стали";
  static const String gunsHackingGoodEvent02 =
      "Персонал магазина оказался на удивление послушным и не пытался сопротивляться";

  static const String gunsSpeechGoodEvent01 =
      "\"Клиент\" и впрямь оказался при деньгах!";
  static const String gunsSpeechGoodEvent02 =
      "В кошельке было полно наличности";

  static const String hackingSpeechGoodEvent01 = "Да на этом счету полно бабла!";
  static const String hackingSpeechGoodEvent02 =
      "С этого счета можно получить доступ к запасному";

  //Отрицательные события
  static const String drivingLockpickBadEvent01 = "Эта тачка в ужасном состоянии";
  static const String drivingLockpickBadEvent02 =
      "В поисках GPS-маячка пришлось перекопать половину тачки";

  static const String drivingHackingBadEvent01 =
      "В банкомате почти не осталось налички";
  static const String drivingHackingBadEvent02 =
      "Новомодная система охраны заблокировала банкомат";

  static const String drivingGunsBadEvent01 =
      "В фуре гораздо меньше груза, чем мы рассчитывали";
  static const String drivingGunsBadEvent02 =
      "С наводкой мы прокололись, здесь одно барахло";

  static const String drivingSpeechBadEvent01 =
      "Противоугонная система разослала сигнал тревоги ближайшим патрулям";
  static const String drivingSpeechBadEvent02 =
      "Хозяин своими криками привлек кучу внимания";

  static const String lockpickHackingBadEvent01 =
      "Видимо, они все перепрятали. Сейф почти пустой";
  static const String lockpickHackingBadEvent02 = "В сейфе хоть шаром покати";

  static const String lockpickGunsBadEvent01 =
      "Неудачный денек мы выбрали, касса почти пустая";
  static const String lockpickGunsBadEvent02 =
      "Кажется, наши рожи попали на камеры наблюдения";

  static const String lockpickSpeechBadEvent01 =
      "Выручку из этого магазина уже вывезли";
  static const String lockpickSpeechBadEvent02 =
      "В магазине слишком людно, надо подождать подходящего момента";

  static const String gunsHackingBadEvent01 =
      "В кассе почти ничего нет. Экономический кризис!";
  static const String gunsHackingBadEvent02 = "Охрану пришлось долго усмирять";

  static const String gunsSpeechBadEvent01 =
      "Это не наш \"клиент\"! И с деньгами у него не очень";
  static const String gunsSpeechBadEvent02 =
      "Вокруг целая куча свидетелей, выжидаем...";

  static const String hackingSpeechBadEvent01 =
      "Почти все средства с этого счета куда-то перевели";
  static const String hackingSpeechBadEvent02 = "Да тут бабок кот наплакал";
}