class Strings {
  static const String title = "Unless Caught";

  //UI
  static const String newGame = "Новая игра";
  static const String gameOver = "Игра окончена";
  static const String continueGame = "Продолжить";
  static const String loading = "Загрузка";
  static const String exitGame = "Выйти из игры?";
  static const String exit = "Выйти";
  static const String cancel = "Отмена";
  static const String help = "Помощь";
  static const String chooseCharacter = "Выберите вашего персонажа";
  static const String startGame = "Начать игру";
  static const String startMission = "Начать миссию";
  static const String pauseMission = "Пауза";
  static const String resumeMission = "Продолжить";
  static const String missionCompleted = "Миссия\nвыполнена";
  static const String missionFailed = "Миссия\nпровалена";
  static const String close = "Закрыть";
  static const String done = "Готово";
  static const String map = "Карта";
  static const String items = "Инвентарь";
  static const String clothes = "Одежда";
  static const String misc = "Прочее";
  static const String wrongSpec = "Неподходящая специализация";
  static const String noSlot = "Нет свободных слотов";
  static const String stats = "Характеристики";
  static const String onMission = "На миссии";
  static const String reward = "Награда";
  static const String requirements = "Требования";
  static const String participants = "Участники";
  static const String addParticipants = "Назначьте исполнителей для миссии";
  static const String addParticipant = "Добавить участника";
  static const String chooseItem = "Выберите предмет";
  static const String remove = "Убрать";
  static const String owner = "Владелец";
  static const String info = "Инфо";
  static const String gang = "Банда";
  static const String easy = "Легко";
  static const String medium = "Средне";
  static const String hard = "Сложно";
  static const String skills = "Навыки";
  static const String leader = "Лидер";
  static const String members = "Банда";
  static const String hire = "Нанять";
  static const String buy = "Купить";
  static const String bought = "Куплено";
  static const String sell = "Продать";
  static const String sold = "Продано";
  static const String forHire = "Наемники";
  static const String availableMissions = "Доступно";
  static const String inProgressMissions = "Выполняется";
  static const String missionsFinished = "Миссий пройдено/провалено";
  static const String emptyItemsScreen =
      "Здесь будут храниться предметы, которые вы приобретете в процессе игры";
  static const String noItemsToWear = "Нет доступных предметов";
  //Копы
  static const String wanted = "В розыске";
  static const String police = "Полиция";
  static const String dirtyPolice = "\"Грязные копы\"";
  static const String arrest = "Арест";
  static const String arrestInfo = "Копы взяли кое-кого из наших людей";
  static const String noArrested = "Все участники банды на свободе.\nПока что.";
  static const String bailAmount = "Залог";
  static const String bailDeadline = "Срок";
  static const String bailOut = "Внести залог";
  static const String bailed = "Залог\nвнесен";
  static const String bribe = "Взятка";
  static const String bribeDone =
      "Надеемся на долгосрочное и взаимовыгодное сотрудничество";

  //Описания навыков
  static const String drivingSkillName = "Вождение";
  static const String hackingSkillName = "Хакерство";
  static const String lockpickSkillName = "Взлом";
  static const String gunsSkillName = "Стрельба";
  static const String speechSkillName = "Красноречие";

//  static const String drivingSkillDesc = "Навык вождения влияет на способность"
//      " уходить от погони или, наоборот, преследовать цель. Кроме того, опытный водитель"
//      " доставит важный груз в целости и сохранности";
//  static const String hackingSkillDesc = "Навык взлома компьютерных сетей и "
//      "электронных устройств";
//  static const String lockpickSkillDesc =
//      "Навык обращения с отмычками для отпирания "
//      "механических замков";
//  static const String gunsSkillDesc = "Навык стрельбы из всевозможного "
//      "огнестрельного оружия";
//  static const String speechSkillDesc = "Виртуозно владея этим навыком, можно"
//      " заболтать охранника, запугать свидетеля, договориться с конкурентами";

  //Описания специализаций
  static const String driverName = "Водитель";
  static const String burglarName = "Взломщик";
  static const String hackerName = "Хакер";
  static const String gunfighterName = "Стрелок";
  static const String talkerName = "Болтун";

  static const String driverDesc = "Водитель – самый опасный узел машины";
  static const String burglarDesc =
      "Взломать можно все, особенно если это придумано человеком";
  static const String hackerDesc = "Информация правит миром";
  static const String gunfighterDesc = "Guns. Lots of guns";
  static const String talkerDesc =
      "Речь - великая сила: она убеждает, обращает, принуждает";

  //Названия миссий
  static const String drivingLockpickMission = "Угон";
  static const String drivingHackingMission = "Кража из банкомата";
  static const String drivingGunsMission = "Ограбление фуры";
  static const String drivingSpeechMission = "Угон средь бела дня";
  static const String lockpickHackingMission = "Кража из сейфа";
  static const String lockpickGunsMission = "Ограбление ломбарда";
  static const String lockpickSpeechMission = "Кража в магазине";
  static const String gunsHackingMission = "Ограбление магазина";
  static const String gunsSpeechMission = "Ограбление на улице";
  static const String hackingSpeechMission = "Взлом банковского счета";

  //Клички персонажей
  static const String driverNickname01 = "Дизель";
  static const String driverNickname02 = "Турбо";
  static const String driverNickname03 = "Волк";
  static const String driverNickname04 = "Слепой";
  static const String driverNickname05 = "Джимми";

  static const String burglarNickname01 = "Кирпич";
  static const String burglarNickname02 = "Ржавый";
  static const String burglarNickname03 = "Змей";
  static const String burglarNickname04 = "Мелкий";
  static const String burglarNickname05 = "Гудини";

  static const String hackerNickname01 = "Нео";
  static const String hackerNickname02 = "Кейс";
  static const String hackerNickname03 = "Червь";
  static const String hackerNickname04 = "Жук";
  static const String hackerNickname05 = "Кащей";

  static const String gunfighterNickname01 = "Череп";
  static const String gunfighterNickname02 = "Топор";
  static const String gunfighterNickname03 = "Томми";
  static const String gunfighterNickname04 = "Молот";
  static const String gunfighterNickname05 = "Мясник";

  static const String talkerNickname01 = "Цезарь";
  static const String talkerNickname02 = "Хитрый";
  static const String talkerNickname03 = "Красавчик";
  static const String talkerNickname04 = "Серый";
  static const String talkerNickname05 = "Умник";
}
