import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

Map<Skill, String> skillNameMap = {
  Skill.driving: Strings.drivingSkillName,
  Skill.lockpick: Strings.lockpickSkillName,
  Skill.hacking: Strings.hackingSkillName,
  Skill.guns: Strings.gunsSkillName,
  Skill.speech: Strings.speechSkillName
};

//Map<Skill, String> skillDescriptionMap = {
//  Skill.driving: Strings.drivingSkillDesc,
//  Skill.lockpick: Strings.lockpickSkillDesc,
//  Skill.hacking: Strings.hackingSkillDesc,
//  Skill.guns: Strings.gunsSkillDesc,
//  Skill.speech: Strings.speechSkillDesc
//};

Map<Specialization, String> specNameMap = {
  Specialization.driver: Strings.driverName,
  Specialization.burglar: Strings.burglarName,
  Specialization.hacker: Strings.hackerName,
  Specialization.gunfighter: Strings.gunfighterName,
  Specialization.talker: Strings.talkerName
};

Map<Specialization, String> specSmallPicPathMap = {
  Specialization.driver: "images/characters/avatar_driver_small.jpg",
  Specialization.burglar: "images/characters/avatar_burglar_small.jpg",
  Specialization.hacker: "images/characters/avatar_hacker_small.jpg",
  Specialization.gunfighter: "images/characters/avatar_gunfighter_small.jpg",
  Specialization.talker: "images/characters/avatar_talker_small.jpg"
};

Map<Specialization, Skill> mainSkillMap = {
  Specialization.driver: Skill.driving,
  Specialization.burglar: Skill.lockpick,
  Specialization.hacker: Skill.hacking,
  Specialization.gunfighter: Skill.guns,
  Specialization.talker: Skill.speech
};

List<String> specNameList = [
  Strings.burglarName,
  Strings.driverName,
  Strings.gunfighterName,
  Strings.hackerName,
  Strings.talkerName
];

List<String> specDescriptionList = [
  Strings.burglarDesc,
  Strings.driverDesc,
  Strings.gunfighterDesc,
  Strings.hackerDesc,
  Strings.talkerDesc
];

List<String> specLargeImagePathList = [
  "images/characters/avatar_burglar.jpg",
  "images/characters/avatar_driver.jpg",
  "images/characters/avatar_gunfighter.jpg",
  "images/characters/avatar_hacker.jpg",
  "images/characters/avatar_talker.jpg"
];

List<Specialization> specList = [
  Specialization.burglar,
  Specialization.driver,
  Specialization.gunfighter,
  Specialization.hacker,
  Specialization.talker
];

List<String> difficultyName = [Strings.easy, Strings.medium, Strings.hard];

class Utils {
  static List<Skill> getMainSkills(List<Specialization> specs) {
    List<Skill> results = [];
    specs.forEach((spec) => results.add(mainSkillMap[spec]));
    return results;
  }

  static Color getItemColor(Item item) {
    if (item is ClassEquipment) {
      if (item.requiredClasses.isNotEmpty)
        return skillColor[mainSkillMap[item.requiredClasses[0]]];
    }

    return mainBackgroundColor;
  }

  static Color getSkillValueColor(Character character, Skill skill,
      {bool isColored = true}) {
    //Если на навык skill влияют предметы, показать это цветом
    int skillChange = character.getSkillChangeDegree(skill);
    if (skillChange == 0)
      return isColored ? skillColor[skill] : Colors.white;
    else if (skillChange == 1)
      return goodUITextColor;
    else
      return badUITextColor;
  }
}
