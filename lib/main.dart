import 'package:crime_game/model/game_loader.dart';
import 'package:crime_game/model/game_world.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/characters/character_select_screen.dart';
import 'package:crime_game/widgets/main_game_screen.dart';
import 'package:crime_game/widgets/start_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp(loader: GameLoader()));

class MyApp extends StatefulWidget {
  final GameLoader loader;

  MyApp({Key key, @required this.loader}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GameWorld _gameWorld;
  bool _savedGameFound;

  @override
  void initState() {
    super.initState();
    widget.loader.loadGame().then((GameWorld value) {
      setState(() {
        _gameWorld = value;
        _gameWorld.loader = widget.loader;
        _savedGameFound = true;
      });
    }).catchError((e) {
      print("Game Load failed: " + e.toString());
      setState(() {
        _gameWorld = GameWorld.newGame();
        _gameWorld.loader = widget.loader;
        _savedGameFound = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_gameWorld == null)
      return _buildLoadingUI();
    else
      return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: _gameWorld),
          ChangeNotifierProvider.value(value: _gameWorld.gang),
          ChangeNotifierProvider.value(value: _gameWorld.missionPool),
          ChangeNotifierProvider.value(value: _gameWorld.itemPool),
          ChangeNotifierProvider.value(value: _gameWorld.police),
          ChangeNotifierProvider.value(value: _gameWorld.dirtyPolice),
          ChangeNotifierProvider.value(value: _gameWorld.dirtyPolice.bribe),
          ChangeNotifierProvider.value(
              value: _gameWorld.missionPool.eventHandler),
          ChangeNotifierProvider.value(value: _gameWorld.police.eventHandler),
          ChangeNotifierProvider.value(value: _gameWorld.gang.itemStore),
          ChangeNotifierProvider.value(value: _gameWorld.gang.moneyStat)
        ],
        child: MaterialApp(
          title: Strings.title,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          routes: {
            '/': (context) => StartScreen(savedGameFound: _savedGameFound),
            '/character_select': (context) => CharacterSelectScreen(),
            '/game_screen': (context) => MainGameScreen()
          },
        ),
      );
  }

  Widget _buildLoadingUI() {
    return MaterialApp(
        title: Strings.title,
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(color: mainBackgroundColor),
              child: Center(
                child: Text(Strings.loading, style: mainButtonTextStyle),
              )),
        ));
  }

  @override
  void dispose() {
    _gameWorld.dispose();
    super.dispose();
  }
}
