import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//Colors
const Color buttonTextColor = Colors.white;
const Color mainButtonColor = Colors.purple;
const Color skipButtonColor = Colors.blueGrey;
const Color badUITextColor = Colors.red;
const Color goodUITextColor = Colors.green;
const Color mainBackgroundColor = Colors.black;
const Color gradientMiddleColor = Color(0xFF3c0d42);
const Color gradientEndColor = Color(0xFF1f1054);

Map<Skill, Color> skillColor = {
  Skill.driving: Colors.teal,
  Skill.lockpick: Colors.brown,
  Skill.hacking: Colors.deepPurple,
  Skill.guns: Color(0xffa31c1c),
  Skill.speech: Color(0xff37479e)
};

List<Color> difficultyColor = [
  Color(0xff4a8504),
  Color(0xffde8400),
  Color(0xffd63b2f)
];

//Icons
Map<Skill, String> skillIconPath = {
  Skill.driving: "images/skills/skill_driving.png",
  Skill.lockpick: "images/skills/skill_lockpick.png",
  Skill.hacking: "images/skills/skill_hacking.png",
  Skill.guns: "images/skills/skill_guns.png",
  Skill.speech: "images/skills/skill_speech.png"
};

Map<ItemSlot, String> itemSlotIconPath = {
  ItemSlot.hand: "images/items/slot_hand.png",
  ItemSlot.head: "images/items/slot_head.png",
  ItemSlot.backpack: "images/items/slot_backpack.png",
  ItemSlot.body: "images/items/slot_body.png",
};

const IconData moneyIcon = FontAwesomeIcons.coins;
const Color moneyIconColor = Colors.yellow;
const IconData gangIcon = FontAwesomeIcons.users;
const Color gangIconColor = Colors.indigo;
const IconData durationIcon = FontAwesomeIcons.stopwatch;
const Color durationIconColor = Colors.indigo;
const IconData xpIcon = FontAwesomeIcons.graduationCap;
const Color xpIconColor = Colors.blue;
const IconData mapIcon = FontAwesomeIcons.mapMarked;
const IconData itemsIcon = FontAwesomeIcons.diceD20;

//Police
const String wantedIconPath = "images/wanted.png";
const String wantedFilledIconPath = "images/wanted_filled.png";
const Color wantedIconColor = Colors.redAccent;
const String policeMarkerPath = "images/police/police_marker.png";
const String policeBackgroundPath = "images/police/police_background.jpg";
const String policeArrestPath = "images/police/police_arrest.jpg";
const String dirtyCopsMissionPath = "images/police/dirty_cops.jpg";
const String dirtyCopsMissionMarkerPath = "images/police/police_handcuffs.png";
const String dirtyCopsBribeMarkerPath = "images/police/police_bribe.png";
const String dirtyCopsBribeImagePath =
    "images/police/police_bribe_background.png";
const Color policeColor = Color(0xff213ab8);

//Start screens
const List<String> startScreens = [
  "images/start_screens/start_screen_01.jpg",
  "images/start_screens/start_screen_02.jpg",
  "images/start_screens/start_screen_03.jpg",
  "images/start_screens/start_screen_04.png"
];

//Exit screens
const List<String> exitScreens = [
  "images/exit_screens/exit_screen_01.jpg",
  "images/exit_screens/exit_screen_02.jpg",
  "images/exit_screens/exit_screen_03.jpg",
];

//TextStyles
const String mainFont = "BorkDisplay";
const String accentFont = "MilitiaSansPro";

const TextStyle appBarStatTextStyle =
    TextStyle(fontFamily: accentFont, color: Colors.white, fontSize: 18);

const TextStyle mainButtonTextStyle =
    TextStyle(fontFamily: accentFont, color: buttonTextColor, fontSize: 24);

const TextStyle smallButtonTextStyle = TextStyle(
    fontFamily: accentFont, color: buttonTextColor, fontSize: 16, height: 1.5);

const TextStyle sectionHeaderTextStyle =
    TextStyle(fontFamily: mainFont, color: Color(0xBF000000), fontSize: 18);

const TextStyle sectionHeaderBoldTextStyle = TextStyle(
    fontFamily: mainFont,
    color: Color(0xBF000000),
    fontSize: 18,
    fontWeight: FontWeight.bold);

const TextStyle listItemMissionNameTextStyle = TextStyle(
    fontFamily: mainFont,
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.bold);

const TextStyle listItemMissionRewardTextStyle =
    TextStyle(fontFamily: accentFont, color: Colors.black, fontSize: 20);

const TextStyle listItemSubHeaderTextStyle =
    TextStyle(fontFamily: mainFont, color: Colors.black, fontSize: 14);

const TextStyle smallMainTextStyle =
    TextStyle(fontFamily: mainFont, color: Colors.black, fontSize: 13);

const TextStyle infoScreenHeaderTextStyle =
    TextStyle(fontFamily: accentFont, color: Colors.white, fontSize: 22);

const TextStyle missionEndMessageTextStyle = TextStyle(
    fontFamily: accentFont, fontSize: 26, height: 1.5, letterSpacing: 1.2);

const TextStyle infoScreenSubHeaderTextStyle =
    TextStyle(fontFamily: mainFont, color: Colors.black, fontSize: 20);

const TextStyle infoScreenSkillTextStyle =
    TextStyle(fontFamily: accentFont, color: Colors.black, fontSize: 17);

const TextStyle smallSkillTextStyle =
    TextStyle(fontFamily: accentFont, color: Colors.black, fontSize: 14);

const TextStyle bottomNavigationTextStyle =
    TextStyle(fontFamily: mainFont, color: buttonTextColor, fontSize: 13);
