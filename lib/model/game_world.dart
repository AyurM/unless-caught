import 'dart:io';
import 'dart:math';

import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/game_loader.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/items/item_pool.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/police/dirty_police.dart';
import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:trotter/trotter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'game_world.g.dart';

@JsonSerializable()
class GameWorld extends ChangeNotifier {
  Gang gang;
  MissionPool missionPool;
  ItemPool itemPool;
  Police police;
  DirtyPolice dirtyPolice;

  @JsonKey(ignore: true)
  GameLoader loader;
  @JsonKey(ignore: true)
  bool gameOver = false;

  GameWorld(this.gang, this.itemPool, this.missionPool, this.police,
      this.dirtyPolice) {
    this.gang.checkGameOverCallback = checkGameOver;
    this.missionPool.onMissionCompleteWorldCallback = _onMissionComplete;
    this.missionPool.onMissionFailWorldCallback = _onMissionFail;
    this.dirtyPolice.missionPool = missionPool;
    this.dirtyPolice.gang = gang;
    dirtyPolice.loadBribe();
  }

  GameWorld.newGame() {
    this.gang =
        Gang(Gang.startMoney, [], [], ItemStore(ItemCollection(items: [])));
    this.gang.checkGameOverCallback = checkGameOver;
    this.itemPool = ItemPool([]);
    this.missionPool = MissionPool([], [],
        onMissionCompleteWorldCallback: _onMissionComplete,
        onMissionFailWorldCallback: _onMissionFail);
    this.police = Police([], MapPosition(85, 305));
    this.dirtyPolice = DirtyPolice(missionPool: missionPool, gang: gang)
      ..reset();
  }

  factory GameWorld.fromJson(Map<String, dynamic> json) =>
      _$GameWorldFromJson(json);

  Map<String, dynamic> toJson() => _$GameWorldToJson(this);

  void startNewGame(int index) {
    _reset();
    Specialization mainCharacterSpec = specList[index];
    gang.createMainCharacter(mainCharacterSpec);
    missionPool.createInitialMissions(
        mainCharacterSpec, gang.getForHireSpecs());
  }

  Future<File> saveGame() async {
    return loader.saveGame(this);
  }

  void checkGameOver() {
    gameOver = _isGameOver();
    notifyListeners();
  }

  bool _isGameOver() {
    List<Character> currentMembers = [gang.mainCharacter, ...gang.members];
    //Текущим составом банды можно начать хотя бы одну из доступных миссий
    if (missionPool.hasDoableMission(currentMembers)) return false;
    //Можно запустить хотя бы одну доступную миссию с кем-то из доступных
    //наемников или арестованных персонажей
    if (_canStartMissionWithExtraChars()) return false;
    return true;
  }

  bool _canStartMissionWithExtraChars() {
    if (gang.forHire.isEmpty && police.arrested.isEmpty) return false;

    bool result = false;
    int netWorth = gang.getNetWorth();
    print("NetWorth = $netWorth");
    List<Character> arrestedChars =
        police.arrested.map((arChar) => arChar.character).toList();
    List<Character> allExtraChars = [...gang.forHire, ...arrestedChars];
    //перебирает все комбинации доступных наемников и арестованных персонажей
    //длиной от 1 до 3 и проверяет, можно ли нанять/выкупить из тюрьмы этих
    //персонажей и запустить хотя бы одну доступную миссию
    for (int i = 1; i <= min(3, allExtraChars.length); i++) {
      var combos = Combinations(i, allExtraChars);
      for (var combo in combos()) {
        int totalCost = 0;
        for (Character char in combo) {
          if (arrestedChars.contains(char))
            totalCost += police.arrested
                .firstWhere((arChar) => arChar.character == char)
                .bail
                .bail;
          else
            totalCost += char.hireCost;
        }
        if (totalCost <= netWorth) {
          if (missionPool.hasDoableMission(
              [gang.mainCharacter, ...gang.members, ...combo])) result = true;
          break;
        }
      }
    }

    return result;
  }

  void _onMissionComplete(Mission mission) {
    gang.addMoney(mission.reward.credits);
    _onMissionFinish(mission);
  }

  void _onMissionFail(Mission mission) {
    _onMissionFinish(mission);
  }

  void _onMissionFinish(Mission mission) {
    gang.updateSkippedMissionsCounters();
    gang.addCharacters(missionPool.getTotalMissions());
    gang.refreshWantedLevel();
    itemPool.refreshShops(missionPool.getTotalMissions());

    if (police.isActive) police.onMissionFinished(gang);

    if (!police.isActive &&
        missionPool.getTotalMissions() == Police.missionsToAppear)
      police.show();

    dirtyPolice.onMissionFinish();
    checkGameOver();
  }

  void _reset() {
    gang.reset();
    missionPool.reset();
    itemPool.reset();
    police.reset();
    dirtyPolice.reset();
  }
}
