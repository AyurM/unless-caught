// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_world.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameWorld _$GameWorldFromJson(Map<String, dynamic> json) {
  return GameWorld(
    json['gang'] == null
        ? null
        : Gang.fromJson(json['gang'] as Map<String, dynamic>),
    json['itemPool'] == null
        ? null
        : ItemPool.fromJson(json['itemPool'] as Map<String, dynamic>),
    json['missionPool'] == null
        ? null
        : MissionPool.fromJson(json['missionPool'] as Map<String, dynamic>),
    json['police'] == null
        ? null
        : Police.fromJson(json['police'] as Map<String, dynamic>),
    json['dirtyPolice'] == null
        ? null
        : DirtyPolice.fromJson(json['dirtyPolice'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GameWorldToJson(GameWorld instance) => <String, dynamic>{
      'gang': instance.gang,
      'missionPool': instance.missionPool,
      'itemPool': instance.itemPool,
      'police': instance.police,
      'dirtyPolice': instance.dirtyPolice,
    };
