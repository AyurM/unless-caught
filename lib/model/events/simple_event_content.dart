import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';

class SimpleEventContent{
  final EventTime eventTime;
  final String description;
  final EventEffect effect;

  const SimpleEventContent(this.eventTime, this.description, this.effect);
}