import 'package:crime_game/model/events/simple_event_content.dart';
import 'package:crime_game/model/events/reward_event_effect.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/strings/strings_simple_events.dart';

Map<Skill, List<SimpleEventContent>> goodSkillEventDescriptions = {
  Skill.driving: goodDrivingEventDescriptions,
  Skill.lockpick: goodLockPickEventDescriptions,
  Skill.hacking: goodHackingEventDescriptions,
  Skill.guns: goodGunsEventDescriptions,
  Skill.speech: goodSpeechEventDescriptions
};

Map<Skill, List<SimpleEventContent>> badSkillEventDescriptions = {
  Skill.driving: badDrivingEventDescriptions,
  Skill.lockpick: badLockPickEventDescriptions,
  Skill.hacking: badHackingEventDescriptions,
  Skill.guns: badGunsEventDescriptions,
  Skill.speech: badSpeechEventDescriptions
};

Map<String, List<SimpleEventContent>> goodMissionEventDescriptions = {
  Strings.drivingLockpickMission: goodDrivingLockpickDescriptions,
  Strings.drivingHackingMission: goodDrivingHackingDescriptions,
  Strings.drivingGunsMission: goodDrivingGunsDescriptions,
  Strings.drivingSpeechMission: goodDrivingSpeechDescriptions,
  Strings.lockpickHackingMission: goodLockpickHackingDescriptions,
  Strings.lockpickGunsMission: goodLockpickGunsDescriptions,
  Strings.lockpickSpeechMission: goodLockpickSpeechDescriptions,
  Strings.gunsHackingMission: goodGunsHackingDescriptions,
  Strings.gunsSpeechMission: goodGunsSpeechDescriptions,
  Strings.hackingSpeechMission: goodHackingSpeechDescriptions
};

Map<String, List<SimpleEventContent>> badMissionEventDescriptions = {
  Strings.drivingLockpickMission: badDrivingLockpickDescriptions,
  Strings.drivingHackingMission: badDrivingHackingDescriptions,
  Strings.drivingGunsMission: badDrivingGunsDescriptions,
  Strings.drivingSpeechMission: badDrivingSpeechDescriptions,
  Strings.lockpickHackingMission: badLockpickHackingDescriptions,
  Strings.lockpickGunsMission: badLockpickGunsDescriptions,
  Strings.lockpickSpeechMission: badLockpickSpeechDescriptions,
  Strings.gunsHackingMission: badGunsHackingDescriptions,
  Strings.gunsSpeechMission: badGunsSpeechDescriptions,
  Strings.hackingSpeechMission: badHackingSpeechDescriptions
};

Map<Skill, String> goodSkillEventPictures = {
  Skill.driving: "images/simple_events/driving_good.jpg",
  Skill.lockpick: "images/simple_events/lockpick_good.jpg",
  Skill.hacking: "images/simple_events/hacking_good.jpg",
  Skill.guns: "images/simple_events/guns_good.jpg",
  Skill.speech: "images/simple_events/speech_good.png"
};

Map<Skill, String> badSkillEventPictures = {
  Skill.driving: "images/simple_events/driving_bad.jpg",
  Skill.lockpick: "images/simple_events/lockpick_bad.jpg",
  Skill.hacking: "images/simple_events/hacking_bad.jpg",
  Skill.guns: "images/simple_events/guns_bad.jpg",
  Skill.speech: "images/simple_events/speech_bad.jpg"
};

Map<String, String> goodMissionEventPictures = {
  Strings.drivingLockpickMission:
      "images/mission_events/mission_event_drivingLockpick_good.jpg",
  Strings.drivingHackingMission:
      "images/mission_events/mission_event_drivingHacking_good.jpg",
  Strings.drivingGunsMission:
      "images/mission_events/mission_event_drivingGuns_good.jpg",
  Strings.drivingSpeechMission:
      "images/mission_events/mission_event_drivingSpeech_good.jpg",
  Strings.lockpickHackingMission:
      "images/mission_events/mission_event_lockpickHacking_good.jpg",
  Strings.lockpickGunsMission:
      "images/mission_events/mission_event_lockpickGuns_good.jpg",
  Strings.lockpickSpeechMission:
      "images/mission_events/mission_event_lockpickSpeech_good.jpg",
  Strings.gunsHackingMission:
      "images/mission_events/mission_event_gunsHacking_good.jpg",
  Strings.gunsSpeechMission:
      "images/mission_events/mission_event_gunsSpeech_good.jpg",
  Strings.hackingSpeechMission:
      "images/mission_events/mission_event_hackingSpeech_good.jpg"
};

Map<String, String> badMissionEventPictures = {
  Strings.drivingLockpickMission:
      "images/mission_events/mission_event_drivingLockpick_bad.jpg",
  Strings.drivingHackingMission:
      "images/mission_events/mission_event_drivingHacking_bad.jpg",
  Strings.drivingGunsMission:
      "images/mission_events/mission_event_drivingGuns_bad.jpg",
  Strings.drivingSpeechMission:
      "images/mission_events/mission_event_drivingSpeech_bad.jpg",
  Strings.lockpickHackingMission:
      "images/mission_events/mission_event_lockpickHacking_bad.jpg",
  Strings.lockpickGunsMission:
      "images/mission_events/mission_event_lockpickGuns_bad.jpg",
  Strings.lockpickSpeechMission:
      "images/mission_events/mission_event_lockpickSpeech_bad.jpg",
  Strings.gunsHackingMission:
      "images/mission_events/mission_event_gunsHacking_bad.jpg",
  Strings.gunsSpeechMission:
      "images/mission_events/mission_event_gunsSpeech_bad.jpg",
  Strings.hackingSpeechMission:
      "images/mission_events/mission_event_hackingSpeech_bad.jpg"
};

//Описания ПРОСТЫХ положительных событий навыков
List<SimpleEventContent> goodDrivingEventDescriptions = [
  SimpleEventContent(EventTime.begin, StringSimpleEvents.drivingGoodEvent01,
      RewardEventEffect(missionPoints: 6, durationChange: -0.15)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.drivingGoodEvent02,
      RewardEventEffect(missionPoints: 6, durationChange: -0.1, wantedChange: -1)),
  SimpleEventContent(EventTime.begin, StringSimpleEvents.drivingGoodEvent03,
      RewardEventEffect(missionPoints: 6, durationChange: -0.15)),
];
List<SimpleEventContent> goodLockPickEventDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickGoodEvent01,
      RewardEventEffect(missionPoints: 6, creditsChange: 50, wantedChange: -1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickGoodEvent02,
      RewardEventEffect(missionPoints: 6, durationChange: -0.1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickGoodEvent03,
      RewardEventEffect(missionPoints: 6, durationChange: -0.15)),
];
List<SimpleEventContent> goodHackingEventDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingGoodEvent01,
      RewardEventEffect(missionPoints: 6, durationChange: -0.15)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingGoodEvent02,
      RewardEventEffect(missionPoints: 6, durationChange: -0.1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingGoodEvent03,
      RewardEventEffect(missionPoints: 6, creditsChange: 50)),
];
List<SimpleEventContent> goodGunsEventDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsGoodEvent01,
      RewardEventEffect(missionPoints: 6, durationChange: -0.1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsGoodEvent02,
      RewardEventEffect(missionPoints: 6, creditsChange: 30, durationChange: -0.05)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.gunsGoodEvent03,
      RewardEventEffect(missionPoints: 6, xpChange: 1)),
];
List<SimpleEventContent> goodSpeechEventDescriptions = [
  SimpleEventContent(EventTime.begin, StringSimpleEvents.speechGoodEvent01,
      RewardEventEffect(missionPoints: 6, wantedChange: -1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.speechGoodEvent02,
      RewardEventEffect(missionPoints: 6, creditsChange: 50)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.speechGoodEvent03,
      RewardEventEffect(missionPoints: 6, durationChange: -0.1, wantedChange: -1)),
];

//Описания ПРОСТЫХ отрицательных событий навыков
List<SimpleEventContent> badDrivingEventDescriptions = [
  SimpleEventContent(EventTime.begin, StringSimpleEvents.drivingBadEvent01,
      RewardEventEffect(missionPoints: -6, creditsChange: -40, durationChange: 0.1)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.drivingBadEvent02,
      RewardEventEffect(missionPoints: -6, durationChange: 0.15)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.drivingBadEvent03,
      RewardEventEffect(missionPoints: -6, creditsChange: -50)),
];
List<SimpleEventContent> badLockPickEventDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickBadEvent01,
      RewardEventEffect(missionPoints: -6, durationChange: 0.15)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickBadEvent02,
      RewardEventEffect(missionPoints: -6, creditsChange: -30, durationChange: 0.1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.lockpickBadEvent03,
      RewardEventEffect(missionPoints: -6, durationChange: 0.15)),
];
List<SimpleEventContent> badHackingEventDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingBadEvent01,
      RewardEventEffect(missionPoints: -6, creditsChange: -50)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingBadEvent02,
      RewardEventEffect(missionPoints: -6, creditsChange: -30, durationChange: 0.1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.hackingBadEvent03,
      RewardEventEffect(missionPoints: -6, durationChange: 0.15)),
];
List<SimpleEventContent> badGunsEventDescriptions = [
  SimpleEventContent(EventTime.end, StringSimpleEvents.gunsBadEvent01,
      RewardEventEffect(missionPoints: -6, xpChange: -1)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.gunsBadEvent02,
      RewardEventEffect(missionPoints: -6, creditsChange: -50)),
  SimpleEventContent(EventTime.end, StringSimpleEvents.gunsBadEvent03,
      RewardEventEffect(missionPoints: -6, xpChange: -1)),
];
List<SimpleEventContent> badSpeechEventDescriptions = [
  SimpleEventContent(EventTime.begin, StringSimpleEvents.speechBadEvent01,
      RewardEventEffect(missionPoints: -6, durationChange: 0.1, wantedChange: 1)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.speechBadEvent02,
      RewardEventEffect(missionPoints: -6, wantedChange: 2)),
  SimpleEventContent(EventTime.begin, StringSimpleEvents.speechBadEvent03,
      RewardEventEffect(missionPoints: -6, creditsChange: -40, durationChange: 0.1)),
];

//Описания положительных событий миссий
List<SimpleEventContent> goodDrivingLockpickDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingLockpickGoodEvent01,
      RewardEventEffect(creditsChange: 30)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingLockpickGoodEvent02,
      RewardEventEffect(creditsChange: 40))
];
List<SimpleEventContent> goodDrivingHackingDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingHackingGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingHackingGoodEvent02,
      RewardEventEffect(missionPoints: 6, wantedChange: -2))
];
List<SimpleEventContent> goodDrivingGunsDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingGunsGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingGunsGoodEvent02,
      RewardEventEffect(creditsChange: 50))
];
List<SimpleEventContent> goodDrivingSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingSpeechGoodEvent01,
      RewardEventEffect(wantedChange: -1, durationChange: -0.1)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingSpeechGoodEvent02,
      RewardEventEffect(creditsChange: 50))
];
List<SimpleEventContent> goodLockpickHackingDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickHackingGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickHackingGoodEvent02,
      RewardEventEffect(creditsChange: 50))
];
List<SimpleEventContent> goodLockpickGunsDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickGunsGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickGunsGoodEvent02,
      RewardEventEffect(creditsChange: 30))
];
List<SimpleEventContent> goodLockpickSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickSpeechGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickSpeechGoodEvent02,
      RewardEventEffect(creditsChange: 30))
];
List<SimpleEventContent> goodGunsHackingDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.gunsHackingGoodEvent01,
      RewardEventEffect(creditsChange: 30)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.gunsHackingGoodEvent02,
      RewardEventEffect(durationChange: -0.1, wantedChange: -1))
];
List<SimpleEventContent> goodGunsSpeechDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsSpeechGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsSpeechGoodEvent02,
      RewardEventEffect(creditsChange: 40))
];
List<SimpleEventContent> goodHackingSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.hackingSpeechGoodEvent01,
      RewardEventEffect(creditsChange: 50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.hackingSpeechGoodEvent02,
      RewardEventEffect(creditsChange: 30))
];

//Описания отрицательных событий миссий
List<SimpleEventContent> badDrivingLockpickDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingLockpickBadEvent01,
      RewardEventEffect(creditsChange: -40)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingLockpickBadEvent02,
      RewardEventEffect(creditsChange: -30, durationChange: 0.1))
];
List<SimpleEventContent> badDrivingHackingDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingHackingBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingHackingBadEvent02,
      RewardEventEffect(wantedChange: 1, creditsChange: -30))
];
List<SimpleEventContent> badDrivingGunsDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.drivingGunsBadEvent01,
      RewardEventEffect(creditsChange: -30)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.drivingGunsBadEvent02,
      RewardEventEffect(creditsChange: -50))
];
List<SimpleEventContent> badDrivingSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingSpeechBadEvent01,
      RewardEventEffect(wantedChange: 2)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.drivingSpeechBadEvent02,
      RewardEventEffect(wantedChange: 1))
];
List<SimpleEventContent> badLockpickHackingDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickHackingBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickHackingBadEvent02,
      RewardEventEffect(creditsChange: -40))
];
List<SimpleEventContent> badLockpickGunsDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickGunsBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickGunsBadEvent02,
      RewardEventEffect(wantedChange: 2))
];
List<SimpleEventContent> badLockpickSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickSpeechBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.lockpickSpeechBadEvent02,
      RewardEventEffect(durationChange: 0.15))
];
List<SimpleEventContent> badGunsHackingDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsHackingBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsHackingBadEvent02,
      RewardEventEffect(durationChange: 0.1, wantedChange: 1))
];
List<SimpleEventContent> badGunsSpeechDescriptions = [
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsSpeechBadEvent01,
      RewardEventEffect(creditsChange: -50)),
  SimpleEventContent(EventTime.middle, StringSimpleEvents.gunsSpeechBadEvent02,
      RewardEventEffect(durationChange: 0.15))
];
List<SimpleEventContent> badHackingSpeechDescriptions = [
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.hackingSpeechBadEvent01,
      RewardEventEffect(creditsChange: -30)),
  SimpleEventContent(
      EventTime.middle,
      StringSimpleEvents.hackingSpeechBadEvent02,
      RewardEventEffect(creditsChange: -50))
];
