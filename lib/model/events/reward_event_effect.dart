import 'package:crime_game/model/events/event_effect.dart';

class RewardEventEffect extends EventEffect {
  final int creditsChange;
  final int xpChange;
  final int wantedChange;
  final int missionPoints;
  //Изменение длительности миссии в % от исходной длительности
  final double durationChange;

  RewardEventEffect(
      {this.creditsChange = 0,
      this.xpChange = 0,
      this.wantedChange = 0,
      this.missionPoints = 0,
      this.durationChange = 0});

  void applyEffect() {
    mission.changeMissionParameters(
        creditsChange, xpChange, wantedChange, durationChange, missionPoints);
  }
}
