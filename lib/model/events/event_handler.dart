import 'package:crime_game/model/events/mission_event.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:flutter/foundation.dart';

class EventHandler extends ChangeNotifier {
  ///Здесь хранятся события для запущенных в настоящее время миссий
  final Map<Mission, Map<Duration, MissionEvent>> eventSchedules = {};

  //Коллбэки передаются из MissionPool
  final void Function(Mission) onMissionEventStart;
  final void Function(Mission) onMissionEventComplete;

  EventHandler(this.onMissionEventStart, this.onMissionEventComplete);

  Duration _activeEventTriggerTime;
  MissionEvent _activeEvent;
  MissionEvent get activeEvent => _activeEvent;

  //Наличие проваленной или завершенной миссии приведет к
  //появлению соответствующего диалогового окна
  Mission _failedMission;
  Mission _completedMission;
  Mission get failedMission => _failedMission;
  Mission get completedMission => _completedMission;

  void addEventSchedules(
      Mission mission, Map<Duration, MissionEvent> eventSchedule) {
    eventSchedules[mission] = eventSchedule;
  }

  void onMissionTick(Mission mission) {
    if (!eventSchedules.containsKey(mission)) return;

    Map<Duration, MissionEvent> eventSchedule = eventSchedules[mission];
    if (eventSchedule.length > 0 &&
        mission.elapsedTime >= eventSchedule.keys.first) {
      _activeEventTriggerTime = eventSchedule.keys.first;
      _activeEvent = eventSchedule[_activeEventTriggerTime];
      onMissionEventTrigger(mission);
      _activeEvent.trigger();
      notifyListeners();
    }
  }

  void onMissionEventTrigger(Mission mission) {
    onMissionEventStart(mission);
  }

  void onMissionEventEnd(Mission mission) {
    eventSchedules[mission].remove(_activeEventTriggerTime);
    _activeEventTriggerTime = null;
    _activeEvent = null;
    onMissionEventComplete(mission);
  }

  void onMissionEnd(Mission mission, bool isSuccess) {
    eventSchedules.remove(mission);
    onMissionEventStart(mission); //для постановки на паузу выполняемых миссий
    if (isSuccess)
      _completedMission = mission;
    else
      _failedMission = mission;
    notifyListeners();
  }

  void onDismissMissionCompleteDialog() {
    onMissionEventComplete(_completedMission);  //для возобновления миссий
    _completedMission = null;
  }

  void onDismissMissionFailDialog() {
    onMissionEventComplete(_failedMission); //для возобновления миссий
    _failedMission = null;
  }
}
