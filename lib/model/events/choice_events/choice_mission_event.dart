import 'package:crime_game/model/events/choice_events/choice_event_content.dart';
import 'package:crime_game/model/events/choice_events/choice_option.dart';
import 'package:crime_game/model/events/mission_event.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/missions/mission.dart';

class ChoiceMissionEvent extends MissionEvent {
  final List<ChoiceOption> options;

  ChoiceMissionEvent(EventTime eventTime, String description, String imagePath,
      Mission mission, this.options)
      : super(eventTime, description, imagePath, mission);

  ChoiceMissionEvent.fromEventContent(ChoiceEventContent eventContent,
      String imagePath, Mission mission, this.options)
      : super(eventContent.eventTime, eventContent.description, imagePath,
            mission);

  @override
  void trigger() {}

  @override
  String toString() {
    String result = "MissionEvent";
    result += "\nВремя события: ${eventTime.toString()}\n";
    result += "Описание: $description\n";
    result += "\n";
    return result;
  }
}
