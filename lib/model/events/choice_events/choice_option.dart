import 'package:crime_game/model/missions/mission.dart';

abstract class ChoiceOption{
  //Текст, который будет написан на кнопке с выбором этого варианта
  final String buttonText;
  final Mission mission;

  ChoiceOption(this.buttonText, this.mission);
}