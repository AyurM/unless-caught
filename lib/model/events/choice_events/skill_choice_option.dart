import 'dart:math';

import 'package:crime_game/model/events/choice_events/choice_event_content.dart';
import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/characters/skill.dart';

import 'choice_option.dart';

const List<int> _skillDifferenceThresholds = [5, 10];
const List<double> _bestCaseProbabilities = [0.05, 0.25, 0.5, 0.75, 0.95];

///При выборе этого варианта события эффект выбирается из списка [effects]
///на основании разницы между имеющимся и требующимся уровнем [skillValue]
///навыка [skill]
class SkillChoiceOption extends ChoiceOption {
  ///Эффекты должны быть отсортированы от самого лучшего к самому худшему
  final List<EventEffect> effects;
  final List<String> resultDescriptions;
  final Skill skill;
  final int skillValue;
  final Random _random = Random(DateTime.now().millisecond);

  SkillChoiceOption(this.effects, this.resultDescriptions, this.skill,
      this.skillValue, String buttonText, Mission mission)
      : super(buttonText, mission) {
    effects.forEach((e) => e.mission = mission);
  }

  SkillChoiceOption.fromChoiceContent(SkillChoiceContent choiceContent,
      this.skill, this.skillValue, String buttonText, Mission mission)
      : this.effects = choiceContent.effects,
        this.resultDescriptions = choiceContent.resultDescriptions,
        super(buttonText, mission) {
    effects.forEach((e) => e.mission = mission);
  }

  //пробная реализация с двумя вариантами исхода выбранной опции
  int chooseEffect() {
    double roll = _random.nextDouble();
    double bestCaseThreshold;
    int skillDifference = skillValue - mission.requirements[skill];
    if (skillDifference < -_skillDifferenceThresholds[1])
      bestCaseThreshold = _bestCaseProbabilities[0];
    else if (skillDifference < -_skillDifferenceThresholds[0] &&
        skillDifference >= -_skillDifferenceThresholds[1])
      bestCaseThreshold = _bestCaseProbabilities[1];
    else if (skillDifference < _skillDifferenceThresholds[0] &&
        skillDifference >= -_skillDifferenceThresholds[0])
      bestCaseThreshold = _bestCaseProbabilities[2];
    else if (skillDifference < _skillDifferenceThresholds[1] &&
        skillDifference >= _skillDifferenceThresholds[0])
      bestCaseThreshold = _bestCaseProbabilities[3];
    else
      bestCaseThreshold = _bestCaseProbabilities[4];

    return roll <= bestCaseThreshold ? 0 : 1;
  }
}
