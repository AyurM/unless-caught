import 'package:crime_game/model/events/choice_events/choice_event_content.dart';
import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/missions/mission.dart';

import 'choice_option.dart';

///При выборе этого варианта события эффект [effect]
///определен заранее
class InstantChoiceOption extends ChoiceOption {
  final EventEffect effect;
  final String resultDescription;

  InstantChoiceOption(
      this.effect, this.resultDescription, String buttonText, Mission mission)
      : super(buttonText, mission) {
    effect.mission = mission;
  }

  InstantChoiceOption.fromChoiceContent(
      InstantChoiceContent choiceContent, Mission mission)
      : this.effect = choiceContent.effect,
        this.resultDescription = choiceContent.resultDescription,
        super(choiceContent.buttonText, mission) {
    effect.mission = mission;
  }
}
