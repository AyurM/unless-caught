import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';

class ChoiceEventContent {
  final EventTime eventTime;
  final String description;
  final InstantChoiceContent instantChoiceContent;
  final SkillChoiceContent skillChoiceContent;

  const ChoiceEventContent(this.eventTime, this.description,
      this.instantChoiceContent, this.skillChoiceContent);
}

class InstantChoiceContent {
  final EventEffect effect;
  final String resultDescription;
  final String buttonText;

  const InstantChoiceContent(
      this.effect, this.resultDescription, this.buttonText);
}

class SkillChoiceContent {
  final List<EventEffect> effects;
  final List<String> resultDescriptions;

  const SkillChoiceContent(this.effects, this.resultDescriptions);
}
