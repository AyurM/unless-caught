import 'package:crime_game/model/events/choice_events/choice_event_content.dart';
import 'package:crime_game/model/events/reward_event_effect.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/strings/strings_choice_events.dart';

Map<Skill, List<ChoiceEventContent>> skillChoiceEvents = {
  Skill.driving: drivingChoiceEventContents,
  Skill.lockpick: lockPickChoiceEventContents,
  Skill.hacking: hackingChoiceEventContents,
  Skill.guns: gunsChoiceEventContents,
  Skill.speech: speechChoiceEventContents
};

Map<Skill, String> skillChoiceEventsImagePaths = {
  Skill.driving: "images/choice_events/choice_event_driving.jpg",
  Skill.lockpick: "images/choice_events/choice_event_lockpick.jpg",
  Skill.hacking: "images/choice_events/choice_event_hacking.jpg",
  Skill.guns: "images/choice_events/choice_event_guns.jpg",
  Skill.speech: "images/choice_events/choice_event_speech.png"
};

List<ChoiceEventContent> drivingChoiceEventContents = [
  ChoiceEventContent(
      EventTime.end,
      StringChoiceEvents.drivingChoiceEvent01,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.driving01InstantResult,
          StringChoiceEvents.driving01InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, durationChange: -0.1, wantedChange: -1),
        RewardEventEffect(missionPoints: -6, creditsChange: -50, durationChange: -0.1)
      ], [
        StringChoiceEvents.driving01SkillResult01,
        StringChoiceEvents.driving01SkillResult02
      ])),
  ChoiceEventContent(
      EventTime.end,
      StringChoiceEvents.drivingChoiceEvent02,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.driving02InstantResult,
          StringChoiceEvents.driving02InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, durationChange: -0.15),
        RewardEventEffect(missionPoints: -6, creditsChange: -50, durationChange: -0.1)
      ], [
        StringChoiceEvents.driving02SkillResult01,
        StringChoiceEvents.driving02SkillResult02
      ]))
];

List<ChoiceEventContent> lockPickChoiceEventContents = [
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.lockpickChoiceEvent01,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.lockpick01InstantResult,
          StringChoiceEvents.lockpick01InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, durationChange: -0.15),
        RewardEventEffect(missionPoints: -6, durationChange: 0.15)
      ], [
        StringChoiceEvents.lockpick01SkillResult01,
        StringChoiceEvents.lockpick01SkillResult02
      ])),
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.lockpickChoiceEvent02,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.lockpick02InstantResult,
          StringChoiceEvents.lockpick02InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, durationChange: -0.15),
        RewardEventEffect(missionPoints: -6, creditsChange: -30, durationChange: 0.1)
      ], [
        StringChoiceEvents.lockpick02SkillResult01,
        StringChoiceEvents.lockpick02SkillResult02
      ]))
];

List<ChoiceEventContent> hackingChoiceEventContents = [
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.hackingChoiceEvent01,
      InstantChoiceContent(
          RewardEventEffect(creditsChange: -30),
          StringChoiceEvents.hacking01InstantResult,
          StringChoiceEvents.hacking01InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, xpChange: 1),
        RewardEventEffect(missionPoints: -6, creditsChange: -40, wantedChange: 2)
      ], [
        StringChoiceEvents.hacking01SkillResult01,
        StringChoiceEvents.hacking01SkillResult02
      ])),
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.hackingChoiceEvent02,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.hacking02InstantResult,
          StringChoiceEvents.hacking02InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, creditsChange: 60),
        RewardEventEffect(missionPoints: -6, wantedChange: 2)
      ], [
        StringChoiceEvents.hacking02SkillResult01,
        StringChoiceEvents.hacking02SkillResult02
      ]))
];

List<ChoiceEventContent> gunsChoiceEventContents = [
  ChoiceEventContent(
      EventTime.end,
      StringChoiceEvents.gunsChoiceEvent01,
      InstantChoiceContent(
          RewardEventEffect(creditsChange: -40),
          StringChoiceEvents.guns01InstantResult,
          StringChoiceEvents.guns01InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, xpChange: 1),
        RewardEventEffect(missionPoints: -6, creditsChange: -50, xpChange: -1)
      ], [
        StringChoiceEvents.guns01SkillResult01,
        StringChoiceEvents.guns01SkillResult02
      ])),
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.gunsChoiceEvent02,
      InstantChoiceContent(
          RewardEventEffect(wantedChange: 1),
          StringChoiceEvents.guns02InstantResult,
          StringChoiceEvents.guns02InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, wantedChange: -2),
        RewardEventEffect(missionPoints: -6, creditsChange: -50, wantedChange: 2)
      ], [
        StringChoiceEvents.guns02SkillResult01,
        StringChoiceEvents.guns02SkillResult02
      ]))
];

List<ChoiceEventContent> speechChoiceEventContents = [
  ChoiceEventContent(
      EventTime.middle,
      StringChoiceEvents.speechChoiceEvent01,
      InstantChoiceContent(
          RewardEventEffect(creditsChange: -30, wantedChange: 1),
          StringChoiceEvents.speech01InstantResult,
          StringChoiceEvents.speech01InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, wantedChange: -2),
        RewardEventEffect(missionPoints: -6, creditsChange: -50, wantedChange: 2)
      ], [
        StringChoiceEvents.speech01SkillResult01,
        StringChoiceEvents.speech01SkillResult02
      ])),
  ChoiceEventContent(
      EventTime.begin,
      StringChoiceEvents.speechChoiceEvent02,
      InstantChoiceContent(
          RewardEventEffect(),
          StringChoiceEvents.speech02InstantResult,
          StringChoiceEvents.speech02InstantButtonText),
      SkillChoiceContent([
        RewardEventEffect(missionPoints: 6, creditsChange: 40, wantedChange: -1),
        RewardEventEffect(missionPoints: -6, creditsChange: -30, wantedChange: 1)
      ], [
        StringChoiceEvents.speech02SkillResult01,
        StringChoiceEvents.speech02SkillResult02
      ]))
];
