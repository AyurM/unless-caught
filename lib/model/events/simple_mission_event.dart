import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/events/mission_event.dart';
import 'package:crime_game/model/events/simple_event_content.dart';
import 'package:crime_game/model/missions/mission.dart';

enum EventTime { begin, middle, end }

class SimpleMissionEvent extends MissionEvent {
  final EventEffect effect;

  SimpleMissionEvent(EventTime eventTime, String description, String imagePath,
      Mission mission, this.effect)
      : super(eventTime, description, imagePath, mission) {
    effect.mission = mission;
  }

  SimpleMissionEvent.fromEventContent(
      SimpleEventContent eventContent, String imagePath, Mission mission)
      : this.effect = eventContent.effect,
        super(eventContent.eventTime, eventContent.description, imagePath,
            mission) {
    effect.mission = mission;
  }

  @override
  void trigger() {
    effect.applyEffect();
  }

  @override
  String toString() {
    String result = "MissionEvent";
    result += "\nВремя события: ${eventTime.toString()}\n";
    result += "Описание: $description\n";
    result += "\n";
    return result;
  }
}
