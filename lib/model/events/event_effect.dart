import 'package:crime_game/model/missions/mission.dart';

abstract class EventEffect{
  Mission mission;

  void applyEffect();
}