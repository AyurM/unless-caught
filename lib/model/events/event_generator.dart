import 'dart:collection';
import 'dart:math';

import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/events/choice_events/choice_event_content.dart';
import 'package:crime_game/model/events/choice_events/choice_event_descriptions.dart';
import 'package:crime_game/model/events/choice_events/choice_mission_event.dart';
import 'package:crime_game/model/events/choice_events/instant_choice_option.dart';
import 'package:crime_game/model/events/choice_events/skill_choice_option.dart';
import 'package:crime_game/model/events/mission_event.dart';
import 'package:crime_game/model/events/simple_event_content.dart';
import 'package:crime_game/model/events/simple_event_descriptions.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/utils/utils.dart';

///Вероятности генерации событий навыков с соотв-м порядковым номером для
///миссий разных сложностей
const List<List<double>> _skillEventProbabilities = [
  [1.0, 0.5],
  [1.0, 1.0, 0.0],
  [1.0, 1.0, 1.0, 0.0]
];

///Вероятность того, что событие навыка будет событием с выбором вариантов действия.
///В противном случае событие будет простым
const double _skillChoiceEventProbability = 0.75;

//Простые события навыков генерируются, исходя из разницы skillDifference между
//максимальным уровнем данного навыка среди участников миссии и уровнем,
//требуемым для миссии. Если разница положительная, то основным (Major)
//событием является хорошее для игрока событие. Если разница отрицательная,
//то плохое.
//Вероятность наступления Major события в пределах skillDifference.abs() от 0 до
//_maxSkillDifference рассчитывается по линейному закону. Если skillDifference.abs()
//выше _maxSkillDifference, то вероятность равна _maxEventProbability.
//Если Major-событие не наступило, то генерируется Minor-событие.
const int _maxSkillDifference = 30;
const double _maxMajorEventProbability = 0.9;
const double _minMajorEventProbability = 0.5;

class EventGenerator {
  final Random _random = Random(DateTime.now().millisecond);

  //Временной зазор между интервалами begin, middle, end
  final Duration _durationOffset = Duration(milliseconds: 250);

  SplayTreeMap<Duration, MissionEvent> createEventsSchedule(Mission mission) {
    List<MissionEvent> events = _createMissionEvents(mission);

    //Назначить случайные (в пределах соответствующих интервалов begin/middle/end)
    //времена наступления событий миссии, и поместить в SplayTreeMap для
    //автоматической сортировки по возрастанию времени наступления события
    SplayTreeMap<Duration, MissionEvent> result = SplayTreeMap();
    events.forEach((event) =>
        result[_getEventTiming(mission.duration, event.eventTime)] = event);
    return result;
  }

  List<MissionEvent> _createMissionEvents(Mission mission) {
    List<MissionEvent> result = [];
    //Перемешать список требуемых для миссии навыков для последующей генерации
    //событий с вероятностями eventProbabilities
    List<Skill> missionSkills = mission.requirements.keys.toList()..shuffle();
    List<double> eventProbabilities =
        _skillEventProbabilities[mission.difficulty];

    for (int i = 0; i < missionSkills.length; i++) {
      //Решить, генерировать ли событие навыка
      if (_random.nextDouble() <= eventProbabilities[i]) {
        //Решить, будет ли сгенерированное событие событием с выбором вариантов
        if (_random.nextDouble() <= _skillChoiceEventProbability) {
          result.add(_generateChoiceEvent(mission, missionSkills[i]));
        } else {
          //Сгенерировать простое событие навыка
          int skillDifference = mission.getMaxSkillValue(missionSkills[i]) -
              mission.requirements[missionSkills[i]];
          double majorProbability = _getMajorEventProbability(skillDifference);
          double roll = _random.nextDouble();
          bool isGoodEvent =
              (skillDifference >= 0 && roll <= majorProbability) ||
                  (skillDifference < 0 && roll > majorProbability);

          result
              .add(_generateSkillEvent(mission, missionSkills[i], isGoodEvent));
        }
      }
    }

    //События миссий отключены для миссий "грязных копов" (пока что?)
    if (mission.baseReward.wantedLevel > 0) {
      //Пока что 50/50
      if (_random.nextDouble() <= 0.5) result.add(_createMissionEvent(mission));
    }

    result.forEach((event) => print(event.toString()));
    return result;
  }

  MissionEvent _generateSkillEvent(Mission mission, Skill skill, bool isGood) {
    print("Генерирую " +
        (isGood ? "хорошее" : "плохое") +
        " событие для навыка ${skill.toString()}");

    return SimpleMissionEvent.fromEventContent(
        _getSkillEventContent(skill, isGood),
        _getSkillEventPicturePath(skill, isGood),
        mission);
  }

  MissionEvent _createMissionEvent(Mission mission) {
    //Пока что 50/50
    bool isGoodEventGenerated = _random.nextDouble() >= 0.5;
    return _generateMissionEvent(mission, isGoodEventGenerated);
  }

  MissionEvent _generateMissionEvent(Mission mission, bool isGood) {
    print("Генерирую " +
        (isGood ? "хорошее" : "плохое") +
        " событие для миссии ${mission.name}");

    return SimpleMissionEvent.fromEventContent(
        _getMissionEventContent(mission, isGood),
        _getMissionEventPicturePath(mission, isGood),
        mission);
  }

  double _getMajorEventProbability(int skillDifference) {
    if (skillDifference >= _maxSkillDifference)
      return _maxMajorEventProbability;

    return ((_maxMajorEventProbability - _minMajorEventProbability) *
            skillDifference.abs() /
            _maxSkillDifference) +
        _minMajorEventProbability;
  }

  SimpleEventContent _getSkillEventContent(Skill skill, bool isGood) {
    List<SimpleEventContent> contents = isGood
        ? goodSkillEventDescriptions[skill]
        : badSkillEventDescriptions[skill];
    return contents[_random.nextInt(contents.length)];
  }

  SimpleEventContent _getMissionEventContent(Mission mission, bool isGood) {
    List<SimpleEventContent> contents = isGood
        ? goodMissionEventDescriptions[mission.name]
        : badMissionEventDescriptions[mission.name];
    return contents[_random.nextInt(contents.length)];
  }

  ///EventContent задает примерное время наступления событий - начало миссии,
  ///середина или конец. Здесь генерируется конкретное время наступления события
  ///[missionDuration], исходя из его примерного времени [eventTime]
  Duration _getEventTiming(Duration missionDuration, EventTime eventTime) {
    //TODO: если длительность миссии была уменьшена во время событий, то некоторые события с EventTime.end не успевают произойти
    Duration oneThird = missionDuration ~/ 3;
    Duration random = (oneThird - (_durationOffset * 2)) * _random.nextDouble();
    Duration result;
    switch (eventTime) {
      case EventTime.begin:
        result = _durationOffset + random;
        break;
      case EventTime.middle:
        result = oneThird + _durationOffset + random;
        break;
      case EventTime.end:
        result = oneThird * 2 + _durationOffset + random;
        break;
    }
    print(result.toString());
    return result;
  }

  String _getSkillEventPicturePath(Skill skill, bool isGood) {
    return isGood
        ? goodSkillEventPictures[skill]
        : badSkillEventPictures[skill];
  }

  String _getMissionEventPicturePath(Mission mission, bool isGood) {
    return isGood
        ? goodMissionEventPictures[mission.name]
        : badMissionEventPictures[mission.name];
  }

  String _getSkillChoiceEventPicturePath(Skill skill) {
    return skillChoiceEventsImagePaths[skill];
  }

  ChoiceMissionEvent _generateChoiceEvent(Mission mission, Skill skill) {
    List<ChoiceEventContent> choiceContentList = skillChoiceEvents[skill];
    ChoiceEventContent choiceContent =
        choiceContentList[_random.nextInt(choiceContentList.length)];

    return ChoiceMissionEvent.fromEventContent(
        choiceContent, _getSkillChoiceEventPicturePath(skill), mission, [
      InstantChoiceOption.fromChoiceContent(
          choiceContent.instantChoiceContent, mission),
      SkillChoiceOption.fromChoiceContent(choiceContent.skillChoiceContent,
          skill, mission.getMaxSkillValue(skill), skillNameMap[skill], mission)
    ]);
  }
}
