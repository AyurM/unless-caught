import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/missions/mission.dart';

abstract class MissionEvent{
  final EventTime eventTime;
  final String description;
  final String imagePath;
  final Mission mission;

  MissionEvent(this.eventTime, this.description, this.imagePath, this.mission);

  void trigger();
}