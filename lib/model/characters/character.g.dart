// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Character _$CharacterFromJson(Map<String, dynamic> json) {
  return Character(
    json['name'] as String,
    (json['skills'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(_$enumDecodeNullable(_$SkillEnumMap, k), e as int),
    ),
    (json['specializations'] as List)
        ?.map((e) => _$enumDecodeNullable(_$SpecializationEnumMap, e))
        ?.toList(),
    json['items'] == null
        ? null
        : ItemCollection.fromJson(json['items'] as Map<String, dynamic>),
    json['hireCost'] as int,
    json['wantedLevel'] as int,
  )
    ..availableXp = json['availableXp'] as int
    ..idleForMissions = json['idleForMissions'] as int
    ..completedMissions =
        (json['completedMissions'] as List)?.map((e) => e as int)?.toList()
    ..failedMissions =
        (json['failedMissions'] as List)?.map((e) => e as int)?.toList()
    ..isBusy = json['isBusy'] as bool
    ..isHired = json['isHired'] as bool
    ..isArrested = json['isArrested'] as bool;
}

Map<String, dynamic> _$CharacterToJson(Character instance) => <String, dynamic>{
      'name': instance.name,
      'skills': instance.skills?.map((k, e) => MapEntry(_$SkillEnumMap[k], e)),
      'specializations': instance.specializations
          ?.map((e) => _$SpecializationEnumMap[e])
          ?.toList(),
      'items': instance.items,
      'hireCost': instance.hireCost,
      'availableXp': instance.availableXp,
      'wantedLevel': instance.wantedLevel,
      'idleForMissions': instance.idleForMissions,
      'completedMissions': instance.completedMissions,
      'failedMissions': instance.failedMissions,
      'isBusy': instance.isBusy,
      'isHired': instance.isHired,
      'isArrested': instance.isArrested,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$SkillEnumMap = {
  Skill.driving: 'driving',
  Skill.hacking: 'hacking',
  Skill.lockpick: 'lockpick',
  Skill.guns: 'guns',
  Skill.speech: 'speech',
};

const _$SpecializationEnumMap = {
  Specialization.driver: 'driver',
  Specialization.hacker: 'hacker',
  Specialization.burglar: 'burglar',
  Specialization.gunfighter: 'gunfighter',
  Specialization.talker: 'talker',
};
