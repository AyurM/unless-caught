import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/strings/strings.dart';

Map<Specialization, List<String>> characterNickNames = {
  Specialization.driver: driverNicknames,
  Specialization.burglar: burglarNicknames,
  Specialization.hacker: hackerNicknames,
  Specialization.gunfighter: gunfighterNicknames,
  Specialization.talker: talkerNicknames
};

List<String> driverNicknames = [
  Strings.driverNickname01,
  Strings.driverNickname02,
  Strings.driverNickname03,
  Strings.driverNickname04,
  Strings.driverNickname05
];

List<String> burglarNicknames = [
  Strings.burglarNickname01,
  Strings.burglarNickname02,
  Strings.burglarNickname03,
  Strings.burglarNickname04,
  Strings.burglarNickname05
];

List<String> hackerNicknames = [
  Strings.hackerNickname01,
  Strings.hackerNickname02,
  Strings.hackerNickname03,
  Strings.hackerNickname04,
  Strings.hackerNickname05
];

List<String> gunfighterNicknames = [
  Strings.gunfighterNickname01,
  Strings.gunfighterNickname02,
  Strings.gunfighterNickname03,
  Strings.gunfighterNickname04,
  Strings.gunfighterNickname05
];

List<String> talkerNicknames = [
  Strings.talkerNickname01,
  Strings.talkerNickname02,
  Strings.talkerNickname03,
  Strings.talkerNickname04,
  Strings.talkerNickname05
];
