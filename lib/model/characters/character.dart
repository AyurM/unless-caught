import 'dart:math';

import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character.g.dart';

@JsonSerializable()
class Character extends ChangeNotifier {
  static const int mainSkillUpgradeAmount = 1;
  static const int maxSkillValue = 100;
  static const int maxWantedLevel = 100;

  //"Залечь на дно"
  static const int _minWantedIdleReduction = 0;
  static const int _maxWantedIdleReduction = 5;
  static const int _minSkippedMissions = 2;
  static const int _maxSkippedMissions = 7;
  static const double _idleReductionPower = 1.4;

  final String name;
  final String smallPicPath;
  final Map<Skill, int> skills;
  final List<Specialization> specializations;
  final ItemCollection<Equipment> items;
  final int hireCost;

  int availableXp = 0;
  int wantedLevel;

  ///Количество миссий подряд, пройденных без участия персонажа. Для механики
  ///"Залечь на дно", при которой уровень разыскиваемости постепенно снижается,
  ///если персонаж был неактивен.
  int idleForMissions = 0;

  List<int> completedMissions = [0, 0, 0];
  List<int> failedMissions = [0, 0, 0];

  bool isBusy = false;
  bool isHired = false;
  bool isArrested = false;

  ///Показывает, нужно ли обновлять [idleForMissions] для данного персонажа.
  ///Равно false, если персонаж только что закончил миссию.
  bool _updateIdle = true;

  Character(this.name, this.skills, this.specializations, this.items,
      this.hireCost, this.wantedLevel)
      : this.smallPicPath = specSmallPicPathMap[specializations[0]] {
    //выставить ограничения на предметы инвентаря по типу слотов
    items.limits = ItemCollection.characterSlotLimits;
  }

  factory Character.fromJson(Map<String, dynamic> json) =>
      _$CharacterFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterToJson(this);

  int getSkillValue(Skill skill) {
    if (skills.containsKey(skill)) return skills[skill];
    return 0;
  }

  set setHired(bool value) {
    isHired = value;
    notifyListeners();
  }

  void addXp(int xpAmount) {
    availableXp += xpAmount;
    notifyListeners();
  }

  void addWanted(int amount) {
    //Уровень разыскиваемости лежит в пределах [0, maxWantedLevel]
    wantedLevel = max(min(wantedLevel + amount, maxWantedLevel), 0);
    notifyListeners();
  }

  void updateWantedIdle() {
    //Персонаж только что закончил миссию
    if (!_updateIdle) {
      _updateIdle = true;
      return;
    }

    if (isBusy) return;

    idleForMissions++;
    wantedLevel = max(wantedLevel - _getWantedIdleReduction(), 0);
    notifyListeners();
  }

  void upgradeSkill(Skill skill) {
    if (!skills.containsKey(skill)) return;

    if (availableXp <= 0) return;

    if (Utils.getMainSkills(specializations).contains(skill))
      skills[skill] =
          min(skills[skill] + mainSkillUpgradeAmount, maxSkillValue);
    else
      skills[skill]++;

    availableXp--;
    notifyListeners();
  }

  ///Оценивает, в какую сторону уровень навыка [skill] изменен надетыми
  ///предметами. 0 - уровень не изменен, 1 - уровень увеличен,
  ///-1 - уровень уменьшен
  int getSkillChangeDegree(Skill skill) {
    int totalSkillChange = 0;
    if (!skills.containsKey(skill)) return totalSkillChange;

    items.items.forEach((item) {
      if (item.stats.containsKey(skill)) totalSkillChange += item.stats[skill];
    });

    return totalSkillChange == 0 ? 0 : (totalSkillChange > 0 ? 1 : -1);
  }

  bool canBeHired(int money) {
    return money >= hireCost;
  }

  void onArrest() {
    isArrested = true;
    //при аресте весь инвентарь изымается
    items.items.forEach((item) {
      for (final Skill skill in item.stats.keys) {
        if (skills.containsKey(skill)) skills[skill] -= item.stats[skill];
      }
    });
    items.clear();
    notifyListeners();
  }

  void onMissionStart(Mission mission) {
    isBusy = true;
    idleForMissions = 0; //сбросить счетчик "Залечь на дно"
    notifyListeners();
  }

  void onMissionComplete(Mission mission) {
    completedMissions[mission.difficulty]++;
    availableXp += mission.reward.xp;
    //Уровень разыскиваемости лежит в пределах [0, maxWantedLevel]
    wantedLevel =
        max(min(wantedLevel + mission.reward.wantedLevel, maxWantedLevel), 0);
    _onMissionEnd(mission);
  }

  void onMissionFail(Mission mission) {
    failedMissions[mission.difficulty]++;
    //Есть миссии "грязных копов" с отрицательным wantedLevel, при провале
    //которых уровень разыскиваемости должен расти. Поэтому wantedLevel.abs()
    wantedLevel =
        min(wantedLevel + mission.reward.wantedLevel.abs(), maxWantedLevel);
    _onMissionEnd(mission);
  }

  void _onMissionEnd(Mission mission) {
    isBusy = false;
    _updateIdle = false;
    notifyListeners();
  }

  void equip(Equipment item) {
    if (item.canBeEquippedBy(this) && items.canBeAdded(item)) {
      items.addItem(item);
      for (final Skill skill in item.stats.keys) {
        if (skills.containsKey(skill)) skills[skill] += item.stats[skill];
      }
      notifyListeners();
    }
  }

  void unequip(Equipment item) {
    items.removeItem(item);
    for (final Skill skill in item.stats.keys) {
      if (skills.containsKey(skill)) skills[skill] -= item.stats[skill];
    }
    notifyListeners();
  }

  bool hasFreeSlot(Equipment item) {
    return items.canBeAdded(item);
  }

  bool hasFinishedMissions() {
    return completedMissions.fold(0, (p, c) => p + c) +
            failedMissions.fold(0, (p, c) => p + c) >
        0;
  }

  ///Нелинейное снижение уровня разыскиваемости в зависимости от числа
  ///пропущенных миссий
  int _getWantedIdleReduction() {
    if (idleForMissions < _minSkippedMissions) return _minWantedIdleReduction;
    if (idleForMissions > _maxSkippedMissions) return _maxWantedIdleReduction;

    return ((1 -
                pow(
                    1 -
                        ((idleForMissions - _minSkippedMissions) /
                            (_maxSkippedMissions - _minSkippedMissions)),
                    _idleReductionPower)) *
            (_maxWantedIdleReduction - _minWantedIdleReduction))
        .round();
  }
}
