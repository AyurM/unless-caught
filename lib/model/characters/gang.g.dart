// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gang.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Gang _$GangFromJson(Map<String, dynamic> json) {
  return Gang(
    json['money'] as int,
    (json['members'] as List)
        ?.map((e) =>
            e == null ? null : Character.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['forHire'] as List)
        ?.map((e) =>
            e == null ? null : Character.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['itemStore'] == null
        ? null
        : ItemStore.fromJson(json['itemStore'] as Map<String, dynamic>),
  )
    ..wantedLevel = (json['wantedLevel'] as num)?.toDouble()
    ..mainCharacter = json['mainCharacter'] == null
        ? null
        : Character.fromJson(json['mainCharacter'] as Map<String, dynamic>);
}

Map<String, dynamic> _$GangToJson(Gang instance) => <String, dynamic>{
      'money': instance.money,
      'wantedLevel': instance.wantedLevel,
      'mainCharacter': instance.mainCharacter,
      'members': instance.members,
      'forHire': instance.forHire,
      'itemStore': instance.itemStore,
    };
