import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/character_generator.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/model/police/police.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'gang.g.dart';

@JsonSerializable()
class Gang extends ChangeNotifier {
  static const int startMoney = 200;

  int money = startMoney;
  double wantedLevel = 0.0;

  Character mainCharacter;

  @JsonKey(ignore: true)
  GangStat moneyStat;

  CharacterGenerator _generator = CharacterGenerator();

  final List<Character> members;
  final List<Character> forHire;
  final ItemStore itemStore;
  VoidCallback checkGameOverCallback;

  Gang(this.money, this.members, this.forHire, this.itemStore) {
    this.moneyStat = GangStat(money);
  }

  factory Gang.fromJson(Map<String, dynamic> json) => _$GangFromJson(json);

  Map<String, dynamic> toJson() => _$GangToJson(this);

  void createMainCharacter(Specialization specialization) {
    mainCharacter = _generator.createCharacter(specialization, tier: 0);
    mainCharacter.setHired = true;
    forHire.addAll(
        _generator.createInitialCharacters(mainCharacter.specializations[0]));
    notifyListeners();
  }

  void addMoney(int value) {
    money += value;
    moneyStat.setValue(money);
    //Игрок потратил деньги и у него может не хватить денег для найма персонажей
    if (value < 0 && checkGameOverCallback != null) checkGameOverCallback();
    notifyListeners();
  }

  void refreshWantedLevel() {
    int totalWanted =
        [mainCharacter, ...members].fold(0, (p, c) => p + c.wantedLevel);
    wantedLevel = totalWanted / (members.length + 1);
    notifyListeners();
  }

  ///Обновляет счетчики "Залечь на дно"
  void updateSkippedMissionsCounters() {
    [mainCharacter, ...members].forEach((char) => char.updateWantedIdle());
    notifyListeners();
  }

  ///Периодически обновляет список доступных для найма персонажей
  void addCharacters(int totalMissionsFinished) {
    List<Character> charactersToAdd =
        _generator.addNewCharactersForHire(totalMissionsFinished);
    if (charactersToAdd.isNotEmpty)
      charactersToAdd.forEach((char) => addCharacterForHire(char));
  }

  void addCharacterForHire(Character character) {
    forHire.add(character);
    notifyListeners();
  }

  void hireCharacter(Character character) {
    if (!character.canBeHired(money)) return;

    members.add(character);
    addMoney(-character.hireCost);
    forHire.remove(character);
    character.setHired = true;
    refreshWantedLevel();
  }

  ///Вызывается из класса Police. Удаляет арестованных персонажей [arrested]
  ///из списка банды
  void onArrestComplete(List<ArrestedCharacter> arrested) {
    List<Character> arrestedChars =
        arrested.map((aChar) => aChar.character).toList();
    members.removeWhere((char) => arrestedChars.contains(char));

    //арест лидера банды приводит к его замене другим персонажем
    if (arrestedChars.contains(mainCharacter) && members.isNotEmpty)
      mainCharacter = members.removeAt(0);

    notifyListeners();
  }

  ///Вызывается для освобожденного из-под ареста персонажа [character]
  void onCharacterRelease(Character character) {
    members.add(character);
    notifyListeners();
  }

  List<Specialization> getForHireSpecs() {
    Set<Specialization> result = Set();
    forHire.forEach((char) => result.addAll(char.specializations));
    return result.toList();
  }

  int getNetWorth() {
    int result = money + itemStore.getNetWorth();
    [mainCharacter, ...members].forEach((char) {
      char.items.items
          .forEach((item) => result += (item.price ~/ Item.sellCoefficient));
    });
    return result;
  }

  void reset() {
    money = startMoney;
    wantedLevel = 0.0;
    mainCharacter = null;
    members.clear();
    forHire.clear();
    itemStore.items.clear();
  }
}

class GangStat extends ChangeNotifier {
  int value;
  bool isUpdated = false;

  GangStat(this.value);

  void setValue(int newValue) {
    value = newValue;
    isUpdated = true;
    notifyListeners();
  }

  void resetUpdated() {
    isUpdated = false;
    notifyListeners();
  }
}
