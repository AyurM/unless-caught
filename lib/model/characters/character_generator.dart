import 'dart:math';
import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/character_nicknames.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/utils/utils.dart';

class CharacterGenerator {
  static const int _startCharacters = 4;
  static const int _missionsPerAddNewCharacters = 3;
  static const int _charactersPerAddNewCharacters = 1;
  static const int _skills = 3;

  ///Стартовый уровень наемников (0 - _characterTiers), влияет на начальные уровни
  ///навыков, уровень разыскиваемости и стоимость найма
  static const int _characterTiers = 2;

  //Character tiers
  static const List<double> _tierProbabilities = [0.5, 0.35, 0.15];
  static const List<List<int>> _mainSkillTiers = [
    [20, 25],
    [25, 30],
    [30, 35]
  ];
  static const List<List<int>> _secondarySkillTiers = [
    [10, 15],
    [13, 18],
    [16, 21]
  ];
  static const List<List<int>> _startWantedLevelTiers = [
    [0, 1],
    [10, 15],
    [20, 25]
  ];
  static const List<List<int>> _hireCostTiers = [
    [10, 15],
    [15, 20],
    [20, 25]
  ];

  final Random _random = Random(DateTime.now().millisecond);

  ///Среди начальных персонажей не должно быть персонажей со
  ///специализацей главного персонажа [spec]. Все стартовые
  ///персонажи должны иметь разные специализации
  List<Character> createInitialCharacters(Specialization spec) {
    List<Character> result = [];
    List<Specialization> specs = []
      ..addAll(Specialization.values)
      ..remove(spec);

    for (int i = 0; i < _startCharacters; i++) {
      Character character = createCharacter(
          specs.removeAt(_random.nextInt(specs.length)),
          tier: 0);
      result.add(character);
    }
    return result;
  }

  List<Character> addNewCharactersForHire(int totalMissionsFinished) {
    List<Character> result = [];
    if (totalMissionsFinished % _missionsPerAddNewCharacters == 0) {
      for (int i = 0; i < _charactersPerAddNewCharacters; i++) {
        result.add(createRandomCharacter());
      }
    }
    return result;
  }

  ///Создает персонажа случайной специализации и уровня [tier]. Если [tier]
  ///не задан, то уровень выбирается случайным образом в соответствии с
  ///вероятностями [_tierProbabilities].
  Character createRandomCharacter({int tier}) {
    if (tier == null) tier = _getRandomTier();

    List<Specialization> specs = Specialization.values;
    Specialization randomSpec = specs[_random.nextInt(specs.length)];
    Skill mainSkill = mainSkillMap[randomSpec];

    return Character(
        _getCharacterName(randomSpec),
        _generateSkills(mainSkill, tier),
        [randomSpec],
        ItemCollection<Equipment>(items: []),
        _generateHireCost(tier),
        _generateWantedLevel(tier));
  }

  ///Создает персонажа указанной специализации [spec] и уровня [tier]. Если [tier]
  ///не задан, то уровень выбирается случайным образом в соответствии с
  ///вероятностями [_tierProbabilities].
  Character createCharacter(Specialization spec, {int tier}) {
    if (tier == null) tier = _getRandomTier();

    Skill mainSkill = mainSkillMap[spec];
    return Character(
        _getCharacterName(spec),
        _generateSkills(mainSkill, tier),
        [spec],
        ItemCollection<Equipment>(items: []),
        _generateHireCost(tier),
        _generateWantedLevel(tier));
  }

  Map<Skill, int> _generateSkills(Skill mainSkill, int tier) {
    List<Skill> skills = []
      ..addAll(Skill.values)
      ..remove(mainSkill);

    Map<Skill, int> result = {};
    result[mainSkill] = _mainSkillTiers[tier][0] +
        _random.nextInt(_mainSkillTiers[tier][1] - _mainSkillTiers[tier][0]);

    for (int i = 0; i < _skills - 1; i++) {
      Skill randomSecondarySkill =
          skills.removeAt(_random.nextInt(skills.length));
      result[randomSecondarySkill] = _secondarySkillTiers[tier][0] +
          _random.nextInt(
              _secondarySkillTiers[tier][1] - _secondarySkillTiers[tier][0]);
    }
    return result;
  }

  int _generateHireCost(int tier) {
    return (_hireCostTiers[tier][0] +
            _random
                .nextInt(_hireCostTiers[tier][1] - _hireCostTiers[tier][0])) *
        10;
  }

  String _getCharacterName(Specialization specialization) {
    List<String> names = characterNickNames[specialization];
    return names[_random.nextInt(names.length)];
  }

  int _generateWantedLevel(int tier) {
    return (_startWantedLevelTiers[tier][0] +
        _random.nextInt(
            _startWantedLevelTiers[tier][1] - _startWantedLevelTiers[tier][0]));
  }

  int _getRandomTier() {
    double roll = _random.nextDouble();
    if (roll <= _tierProbabilities[0])
      return 0;
    else if (roll > _tierProbabilities[0] &&
        roll <= _tierProbabilities[0] + _tierProbabilities[1])
      return 1;
    else
      return _characterTiers;
  }
}
