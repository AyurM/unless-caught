import 'dart:convert';
import 'dart:io';

import 'package:crime_game/model/game_world.dart';
import 'package:path_provider/path_provider.dart';

class GameLoader{
  static const String saveFileName = "saved_game.json";

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/$saveFileName');
  }

  Future<GameWorld> loadGame() async{
    final file = await _localFile;
    String contents = await file.readAsString();
    return GameWorld.fromJson(jsonDecode(contents));
  }

  Future<File> saveGame(GameWorld gameWorld) async {
    final file = await _localFile;
    JsonEncoder encoder = JsonEncoder.withIndent(' ');
    String gameWorldJson = encoder.convert(gameWorld);
    return file.writeAsString(gameWorldJson);
  }
}