import 'package:json_annotation/json_annotation.dart';

part 'map_position.g.dart';

@JsonSerializable()
class MapPosition{
  final double top;
  final double left;

  const MapPosition(this.top, this.left);

  factory MapPosition.fromJson(Map<String, dynamic> json) => _$MapPositionFromJson(json);

  Map<String, dynamic> toJson() => _$MapPositionToJson(this);
}