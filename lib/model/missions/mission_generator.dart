import 'dart:math';

import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/missions/mission_reward.dart';
import 'package:crime_game/model/police/dirty_police.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';

//Skills for mission
const List<int> _numberOfRequiredSkills = [2, 3, 4];

//Задает минимальное и максимальное значение уровня навыка, требуемого для
//миссии соответствующей сложности
const List<List<int>> _requiredSkillValues = [
  [22, 27],
  [25, 30],
  [30, 35]
];

//Difficulty progression
//Коэффициенты для повышения требований к миссиям по мере прохождения игры
//Требования к миссиям перестают расти после прохождения _maxMissionsCompleted миссий
//для легких, средних и сложных миссий соответственно
const List<int> _maxMissionsCompleted = [30, 30, 35];
//Требования к миссиям вырастают не более, чем на _maxRequirementIncrease очков
const List<int> _maxRequirementIncrease = [10, 15, 20];
//Показатели степеней для нелинейного роста сложности миссий
const List<double> _progressExponent = [1.5, 1.9, 1.8];

//Скрытый порог провала миссии
const int _missionFailThreshold = -10;

//Rewards for mission
//Задает минимальное и максимальное значение кредитов за прохождение
//миссии соответствующей сложности. Показатели умножаются на 10
const List<List<int>> _creditRewardValues = [
  [15, 18],
  [20, 24],
  [28, 35]
];

//Задает минимальное и максимальное значение опыта за прохождение
//миссии соответствующей сложности
const List<List<int>> _xpRewardValues = [
  [1, 1],
  [1, 2],
  [2, 3]
];

//Duration
//Задает минимальное и максимальное значение длительности миссии
//соответствующей сложности в секундах
const List<List<int>> _durationValues = [
  [10, 15],
  [18, 24],
  [27, 34]
];

//Penalties
//Разница между имеющимся и требующимся уровнями навыка влияет на итоговую
//длительность миссии (как в +, так и в -). Эти константы задают максимально
//возможное относительное изменение длительности миссии.
const List<List<double>> _penaltyValues = [
  [0.1, 0.15],
  [0.15, 0.2],
  [0.2, 0.25]
];

//Wanted levels
//Задает минимальное и максимальное значение уровня разыскиваемости для миссии
//соответствующей сложности
const List<List<int>> _wantedValues = [
  [2, 3],
  [4, 5],
  [6, 7]
];

const int _startMissions = 3;

//Позиция маркера миссии на карте
const int _maxMapTop = 436;
const int _maxMapLeft = 495;
const int _mapEdgeOffset = 40;

const Map<String, String> _missionImagesMap = {
  Strings.drivingLockpickMission: "images/missions/car_theft.jpg",
  Strings.drivingHackingMission: "images/missions/atm.jpg",
  Strings.drivingGunsMission: "images/missions/truck.jpg",
  Strings.drivingSpeechMission: "images/missions/car_theft_day.jpg",
  Strings.lockpickHackingMission: "images/missions/safe.jpg",
  Strings.lockpickGunsMission: "images/missions/pawnshop.jpg",
  Strings.lockpickSpeechMission: "images/missions/shop_theft.jpg",
  Strings.gunsHackingMission: "images/missions/shop_robbery.jpg",
  Strings.gunsSpeechMission: "images/missions/street_robbery.jpg",
  Strings.hackingSpeechMission: "images/missions/bank_account.jpg"
};

class MissionGenerator {
  final Random _random = Random(DateTime.now().millisecond);

  ///Создать миссию со сложностью [difficulty] и списком требуемых
  ///навыков [skills]. Если [skills] не задано, то требования задаются
  ///случайным образом.
  Mission createMission(int difficulty, MissionPool missionPool,
      [List<Skill> skills]) {
    List<Skill> skillKeys = _getSkillKeys(difficulty, skills);
    Map<Skill, int> requirements = Map.fromIterables(skillKeys,
        _getSkillRequirements(difficulty, missionPool.getTotalMissions()));
    Map<Skill, double> maxPenalties =
        Map.fromIterables(skillKeys, _getPenaltyValues(difficulty));
    String name = _getMissionName(requirements.keys.toList());

    Mission result = Mission.generate(
        name,
        _missionImagesMap[name],
        difficulty,
        _missionFailThreshold,
        MissionReward(
            credits: _getCreditsReward(difficulty),
            xp: _getXpReward(difficulty),
            wantedLevel: _getWantedLevel(difficulty)),
        _generateRandomMapPosition(),
        _getBaseDuration(difficulty),
        requirements,
        maxPenalties,
        [],
        missionPool);
    return result;
  }

  ///Все стартовые миссии должны проходиться с помощью специализаций [specs]
  ///стартовых персонажей и специализации [mainSpec] персонажа игрока.
  List<Mission> createInitialMissions(Specialization mainSpec,
      List<Specialization> specs, MissionPool missionPool) {
    List<Mission> result = [];
    result.add(createMission(0, missionPool, [
      mainSkillMap[mainSpec],
      mainSkillMap[specs[_random.nextInt(specs.length)]]
    ]));
    result.add(createMission(0, missionPool, [
      mainSkillMap[mainSpec],
      mainSkillMap[specs[_random.nextInt(specs.length)]]
    ]));
    result.add(createMission(1, missionPool, [
      mainSkillMap[mainSpec],
      mainSkillMap[specs.removeAt(_random.nextInt(specs.length))],
      mainSkillMap[specs.removeAt(_random.nextInt(specs.length))]
    ]));
    return result;
  }

  ///Создает миссию "грязных копов" без наград по кредитам и опыту, но с
  ///отрицательным уровнем разыскиваемости.
  Mission createDirtyCopsMission(MissionPool missionPool) {
    List<Skill> skillKeys =
        _getSkillKeys(DirtyPolice.dirtyCopsMissionDifficulty, null);

    //Требования для миссии "грязных копов" рассчитываются отдельно
    List<int> skillValues = [];
    for (int i = 0;
        i < _numberOfRequiredSkills[DirtyPolice.dirtyCopsMissionDifficulty];
        i++) {
      skillValues.add(DirtyPolice.dirtyCopsMissionRequirements[0] +
          _random.nextInt(DirtyPolice.dirtyCopsMissionRequirements[1] -
              DirtyPolice.dirtyCopsMissionRequirements[0] +
              1));
    }

    Map<Skill, int> requirements = Map.fromIterables(skillKeys, skillValues);
    Map<Skill, double> maxPenalties = Map.fromIterables(
        skillKeys, _getPenaltyValues(DirtyPolice.dirtyCopsMissionDifficulty));
    String name = Strings.dirtyPolice;
    int wantedReward = (DirtyPolice.dirtyCopsMissionWantedReward[0] +
            _random.nextInt(DirtyPolice.dirtyCopsMissionWantedReward[1] -
                DirtyPolice.dirtyCopsMissionWantedReward[0] +
                1)) *
        -1;

    return Mission.generate(
        name,
        dirtyCopsMissionPath,
        DirtyPolice.dirtyCopsMissionDifficulty,
        _missionFailThreshold,
        MissionReward(wantedLevel: wantedReward),
        _generateRandomMapPosition(),
        _getBaseDuration(DirtyPolice.dirtyCopsMissionDifficulty),
        requirements,
        maxPenalties,
        [],
        missionPool);
  }

  List<Mission> addMissions(MissionPool missionPool) {
    int totalMissions = missionPool.getTotalMissions();

    //Не добавлять новые миссии, пока не пройдены все стартовые
    if (totalMissions < _startMissions) return null;

    List<int> missionsToCreate =
        _getMissionsAmountToGenerate(missionPool, totalMissions);
    if (missionsToCreate == null) return null;

    List<Mission> result = [];
    for (int i = 0; i < missionsToCreate.length; i++) {
      for (int j = 0; j < missionsToCreate[i]; j++) {
        result.add(createMission(i, missionPool));
      }
    }
    return result;
  }

  ///Возвращает количество миссий каждой сложности, которое должно быть добавлено
  ///в список доступных игроку для прохождения после окончания [missionsFinished] миссий
  List<int> _getMissionsAmountToGenerate(
      MissionPool missionPool, int missionsFinished) {
    List<int> totalMissionsAmount = _getTotalMissionsAmount(missionsFinished);
    //Игроку доступно достаточное количество миссий, добавлять новые не нужно.
    //Отфильтрованы миссии "грязных копов"
    int availableMissions = missionPool.available
        .where((mission) => mission.baseReward.wantedLevel >= 0)
        .length;
    if (availableMissions >= totalMissionsAmount[0]) return null;

    List<int> result = [0, 0, 0];
    //Дополнить количество миссий до случайного числа из диапазона totalMissionsAmount
    int missionsToAdd = totalMissionsAmount[0] +
        _random.nextInt(totalMissionsAmount[1] - totalMissionsAmount[0] + 1) -
        availableMissions;
    List<int> missionsPerDifficulty =
        _getMinMissionsPerDifficultyAmount(missionsFinished);

    //Рассчитать, сколько миссий каждой сложности обязательно сгенерировать
    for (int i = 0; i < 3; i++) {
      result[i] = max(
          missionsPerDifficulty[i] -
              missionPool.available
                  .where((mission) =>
                      mission.difficulty == i &&
                      mission.baseReward.wantedLevel >= 0)
                  .length,
          0);
    }

    //Дополнить до нужного количества случайными миссиями
    for (int i = 0;
        i < missionsToAdd - result.fold(0, (prev, element) => prev + element);
        i++) {
      result[_random.nextInt(3)]++;
    }
    return result;
  }

  ///Возвращает минимальное количество миссий каждой сложности (легко, средне, сложно),
  ///которые должны быть доступны игроку после завершения [missionsFinished] миссий
  List<int> _getMinMissionsPerDifficultyAmount(int missionsFinished) {
    if (missionsFinished >= _startMissions && missionsFinished <= 7)
      return [1, 1, 0];
    else if (missionsFinished > 7 && missionsFinished <= 15)
      return [1, 2, 1];
    else if (missionsFinished > 15 && missionsFinished <= 30)
      return [1, 2, 2];
    else
      return [0, 2, 3];
  }

  ///Возвращает общее минимальное и максимальное количество миссий,
  ///которые должны быть доступны игроку после завершения [missionsFinished] миссий
  List<int> _getTotalMissionsAmount(int missionsFinished) {
    if (missionsFinished >= _startMissions && missionsFinished <= 7)
      return [2, 3];
    else if (missionsFinished > 7 && missionsFinished <= 15)
      return [3, 4];
    else if (missionsFinished > 15 && missionsFinished <= 30)
      return [4, 5];
    else
      return [5, 7];
  }

  List<Skill> _getSkillKeys(int difficulty, List<Skill> skills) {
    int numberOfSkills = _numberOfRequiredSkills[difficulty];
    if (skills == null) return _getRandomSkillKeys(numberOfSkills);

    List<Skill> result = [];
    for (int i = 0; i < skills.length; i++) {
      result.add(skills[i]);
    }

    //при необходимости дополнить список требуемых навыков случайными,
    //избегая повторения навыков
    if (numberOfSkills > skills.length) {
      List<Skill> allSkills = []
        ..addAll(Skill.values)
        ..removeWhere((s) => result.contains(s));
      for (int i = 0; i < numberOfSkills - skills.length; i++) {
        result.add(allSkills.removeAt(_random.nextInt(allSkills.length)));
      }
    }
    return result;
  }

  List<int> _getSkillRequirements(int difficulty, int totalMissions) {
    int numberOfSkills = _numberOfRequiredSkills[difficulty];
    List<int> result = [];
    for (int i = 0; i < numberOfSkills; i++) {
      result.add(_getSkillRequirement(difficulty, totalMissions));
    }
    return result;
  }

  List<double> _getPenaltyValues(int difficulty) {
    int numberOfPenalties = _numberOfRequiredSkills[difficulty];
    List<double> result = [];
    for (int i = 0; i < numberOfPenalties; i++) {
      result.add(_getSkillPenalty(difficulty));
    }
    return result;
  }

  List<Skill> _getRandomSkillKeys(int numberOfSkills) {
    List<Skill> result = [];
    List<Skill> skills = []..addAll(Skill.values);

    for (int i = 0; i < numberOfSkills; i++) {
      result.add(skills.removeAt(_random.nextInt(skills.length)));
    }

    return result;
  }

  int _getSkillRequirement(int difficulty, int totalMissions) {
    return _requiredSkillValues[difficulty][0] +
        _random.nextInt(_requiredSkillValues[difficulty][1] -
            _requiredSkillValues[difficulty][0] +
            1) +
        _getRequirementIncrease(difficulty, totalMissions);
  }

  ///Прибавка к базовым требованиям после прохождения [totalMissions] миссий
  int _getRequirementIncrease(int difficulty, int totalMissions) {
    if (totalMissions >= _maxMissionsCompleted[difficulty])
      return _maxRequirementIncrease[difficulty];
    else {
      if (difficulty == 0)
        return ((1 -
                    pow(1 - totalMissions / _maxMissionsCompleted[0],
                        _progressExponent[0])) *
                _maxRequirementIncrease[0])
            .round();
      else
        return (pow(totalMissions / _maxMissionsCompleted[difficulty],
                    _progressExponent[difficulty]) *
                _maxRequirementIncrease[difficulty])
            .round();
    }
  }

  double _getSkillPenalty(int difficulty) {
    return _penaltyValues[difficulty][0] +
        _random.nextDouble() *
            (_penaltyValues[difficulty][1] - _penaltyValues[difficulty][0]);
  }

  int _getCreditsReward(int difficulty) {
    return (_creditRewardValues[difficulty][0] +
            _random.nextInt(_creditRewardValues[difficulty][1] -
                _creditRewardValues[difficulty][0] +
                1)) *
        10;
  }

  int _getXpReward(int difficulty) {
    return (_xpRewardValues[difficulty][0] +
        _random.nextInt(_xpRewardValues[difficulty][1] -
            _xpRewardValues[difficulty][0] +
            1));
  }

  int _getWantedLevel(int difficulty) {
    return (_wantedValues[difficulty][0] +
        _random.nextInt(
            _wantedValues[difficulty][1] - _wantedValues[difficulty][0] + 1));
  }

  Duration _getBaseDuration(int difficulty) {
    return Duration(
        seconds: (_durationValues[difficulty][0] +
            _random.nextInt(_durationValues[difficulty][1] -
                _durationValues[difficulty][0])));
  }

  MapPosition _generateRandomMapPosition() {
    return MapPosition(
        _mapEdgeOffset + _random.nextDouble() * (_maxMapTop - _mapEdgeOffset),
        _mapEdgeOffset + _random.nextDouble() * (_maxMapLeft - _mapEdgeOffset));
  }

  String _getMissionName(List<Skill> requiredSkills) {
    if (requiredSkills.contains(Skill.driving) &&
        requiredSkills.contains(Skill.lockpick))
      return Strings.drivingLockpickMission;
    else if (requiredSkills.contains(Skill.driving) &&
        requiredSkills.contains(Skill.hacking))
      return Strings.drivingHackingMission;
    else if (requiredSkills.contains(Skill.driving) &&
        requiredSkills.contains(Skill.guns))
      return Strings.drivingGunsMission;
    else if (requiredSkills.contains(Skill.driving) &&
        requiredSkills.contains(Skill.speech))
      return Strings.drivingSpeechMission;
    else if (requiredSkills.contains(Skill.lockpick) &&
        requiredSkills.contains(Skill.hacking))
      return Strings.lockpickHackingMission;
    else if (requiredSkills.contains(Skill.lockpick) &&
        requiredSkills.contains(Skill.guns))
      return Strings.lockpickGunsMission;
    else if (requiredSkills.contains(Skill.lockpick) &&
        requiredSkills.contains(Skill.speech))
      return Strings.lockpickSpeechMission;
    else if (requiredSkills.contains(Skill.guns) &&
        requiredSkills.contains(Skill.hacking))
      return Strings.gunsHackingMission;
    else if (requiredSkills.contains(Skill.guns) &&
        requiredSkills.contains(Skill.speech))
      return Strings.gunsSpeechMission;
    else
      return Strings.hackingSpeechMission;
  }
}
