// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mission_pool.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MissionPool _$MissionPoolFromJson(Map<String, dynamic> json) {
  return MissionPool(
    (json['available'] as List)
        ?.map((e) =>
            e == null ? null : Mission.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['inProgress'] as List)
        ?.map((e) =>
            e == null ? null : Mission.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )
    ..completedMissions =
        (json['completedMissions'] as List)?.map((e) => e as int)?.toList()
    ..failedMissions =
        (json['failedMissions'] as List)?.map((e) => e as int)?.toList();
}

Map<String, dynamic> _$MissionPoolToJson(MissionPool instance) =>
    <String, dynamic>{
      'available': instance.available,
      'inProgress': instance.inProgress,
      'completedMissions': instance.completedMissions,
      'failedMissions': instance.failedMissions,
    };
