// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mission.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Mission _$MissionFromJson(Map<String, dynamic> json) {
  return Mission(
    json['name'] as String,
    json['imagePath'] as String,
    json['difficulty'] as int,
    json['failThreshold'] as int,
    json['baseReward'] == null
        ? null
        : MissionReward.fromJson(json['baseReward'] as Map<String, dynamic>),
    json['mapPosition'] == null
        ? null
        : MapPosition.fromJson(json['mapPosition'] as Map<String, dynamic>),
    json['baseDuration'] == null
        ? null
        : Duration(microseconds: json['baseDuration'] as int),
    (json['requirements'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(_$enumDecodeNullable(_$SkillEnumMap, k), e as int),
    ),
    (json['maxPenalties'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          _$enumDecodeNullable(_$SkillEnumMap, k), (e as num)?.toDouble()),
    ),
    (json['characters'] as List)
        ?.map((e) =>
            e == null ? null : Character.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$MissionToJson(Mission instance) => <String, dynamic>{
      'name': instance.name,
      'imagePath': instance.imagePath,
      'difficulty': instance.difficulty,
      'failThreshold': instance.failThreshold,
      'baseDuration': instance.baseDuration?.inMicroseconds,
      'requirements':
          instance.requirements?.map((k, e) => MapEntry(_$SkillEnumMap[k], e)),
      'maxPenalties':
          instance.maxPenalties?.map((k, e) => MapEntry(_$SkillEnumMap[k], e)),
      'characters': instance.characters,
      'mapPosition': instance.mapPosition,
      'baseReward': instance.baseReward,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$SkillEnumMap = {
  Skill.driving: 'driving',
  Skill.hacking: 'hacking',
  Skill.lockpick: 'lockpick',
  Skill.guns: 'guns',
  Skill.speech: 'speech',
};
