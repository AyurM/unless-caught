import 'dart:async';
import 'dart:math';
import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/missions/mission_reward.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'mission.g.dart';

@JsonSerializable()
class Mission extends ChangeNotifier {
  static const Duration _tickDuration = Duration(milliseconds: 250);
  static const List<int> _charactersNumber = [2, 3, 4];

  final String name;
  final String imagePath;
  final int difficulty;
  final int failThreshold;
  final Duration baseDuration;
  final Map<Skill, int> requirements;
  final Map<Skill, double> maxPenalties;
  final List<Character> characters;
  final MapPosition mapPosition;

  @JsonKey(ignore: true)
  MissionPool missionPool;
  @JsonKey(ignore: true)
  MissionReward reward;

  MissionReward baseReward;

  //скрытые очки миссии, снижение ниже порога failThreshold ведет к провалу миссии
  int _missionPoints = 0;

  int _requiredCharacters;
  int get requiredCharacters => _requiredCharacters;

  Duration _duration;
  Duration get duration => _duration;

  bool _isReadyToStart = false;
  bool get isReadyToStart => _isReadyToStart;

  MissionStatus _status = MissionStatus.available;
  MissionStatus get status => _status;

  // Реализация таймера с возможностью паузы:
  // https://stackoverflow.com/questions/53228993/how-to-implement-persistent-stopwatch-in-flutter
  Stopwatch _watch = Stopwatch();
  Timer _timer;
  Duration _elapsedTime = Duration.zero;
  Duration get elapsedTime => _elapsedTime;

  Mission(
      this.name,
      this.imagePath,
      this.difficulty,
      this.failThreshold,
      this.baseReward,
      this.mapPosition,
      this.baseDuration,
      this.requirements,
      this.maxPenalties,
      this.characters) {
    _init();
  }

  Mission.generate(
      this.name,
      this.imagePath,
      this.difficulty,
      this.failThreshold,
      this.baseReward,
      this.mapPosition,
      this.baseDuration,
      this.requirements,
      this.maxPenalties,
      this.characters,
      this.missionPool) {
    _init();
  }

  factory Mission.fromJson(Map<String, dynamic> json) =>
      _$MissionFromJson(json);

  Map<String, dynamic> toJson() => _$MissionToJson(this);

  void addCharacter(Character character) {
    if (character.isBusy) return;
    if (characters.length >= _requiredCharacters) return;

    characters.add(character);
    _isReadyToStart = canBeStarted(characters);

    if (_isReadyToStart) _duration = _calculateDuration();

    notifyListeners();
  }

  void removeCharacter(Character character) {
    if (!characters.contains(character)) return;

    characters.remove(character);
    _isReadyToStart = canBeStarted(characters);

    if (_isReadyToStart) _duration = _calculateDuration();

    notifyListeners();
  }

  void clearCharacters() {
    characters.clear();
    _isReadyToStart = false;
    notifyListeners();
  }

  double getMissionProgress() {
    return min(_elapsedTime.inMilliseconds / _duration.inMilliseconds, 1.0);
  }

  void changeMissionParameters(int creditsChange, int xpChange,
      int wantedChange, double durationChange, int missionPoints) {
    //изменения наград могут быть меньше 0 и могут привести
    //к отрицательным результатам. в этом случае вернуть 0
    reward.credits = max(0, reward.credits + creditsChange);
    reward.xp = max(0, reward.xp + xpChange);

    //wantedLevel обычных миссий не должен опускаться ниже 0. Такое допускается
    //только для миссий "грязных копов"
    if (baseReward.wantedLevel < 0)
      reward.wantedLevel = reward.wantedLevel + wantedChange;
    else
      reward.wantedLevel = max(0, reward.wantedLevel + wantedChange);

    _duration = _duration * (1 + durationChange);
    //обновить очки миссии
    _missionPoints += missionPoints;
    notifyListeners();
  }

  void start() {
    if (!_isReadyToStart) return;

    _timer = Timer.periodic(_tickDuration, _onTimerTick);
    _watch.start();
    characters.forEach((c) => c.onMissionStart(this));
    missionPool.onMissionStart(this);
    //сменить статус после вызова onMissionStart для корректного создания событий
    //миссии при первом запуске
    _status = MissionStatus.inProgress;

    if (_missionPoints < failThreshold) _fail();

    notifyListeners();
  }

  void pause() {
    if (_status != MissionStatus.inProgress) return;

    _status = MissionStatus.paused;
    _stopTimer();
    notifyListeners();
  }

  void complete() {
    _stopTimer();
    _watch.reset();

    _status = MissionStatus.completed;
    _isReadyToStart = false;
    characters.forEach((c) => c.onMissionComplete(this));
    characters.clear();
    missionPool.onMissionComplete(this);
    notifyListeners();
  }

  void _fail() {
    _stopTimer();
    _watch.reset();

    _status = MissionStatus.failed;
    _isReadyToStart = false;
    characters.forEach((c) => c.onMissionFail(this));
    characters.clear();
    missionPool.onMissionFail(this);
    notifyListeners();
  }

  ///Возвращает максимальное значение уровня навыка [skill] среди персонажей,
  ///назначенных на данную миссию
  int getMaxSkillValue(Skill skill) {
    int result = 0;
    List<Character> charactersWithSkill =
        characters.where((c) => c.skills.containsKey(skill)).toList();

    if (charactersWithSkill.length == 0) return result;

    charactersWithSkill.forEach(
        (c) => {if (c.skills[skill] > result) result = c.skills[skill]});
    return result;
  }

  void _stopTimer() {
    _timer?.cancel();
    _timer = null;
    _watch.stop();
    _elapsedTime = _watch.elapsed;
  }

  void _onTimerTick(Timer timer) {
    _elapsedTime = _watch.elapsed;

    if (_elapsedTime >= _duration) complete();
    //eventHandler должен периодически проверять, не наступило ли событие миссии
    missionPool.eventHandler.onMissionTick(this);
    notifyListeners();
  }

  bool canBeStarted(List<Character> chars) {
    if (chars.length < _requiredCharacters) return false;

    for (final Skill skill in requirements.keys) {
      if (chars.where((c) => c.skills.containsKey(skill)).length == 0)
        return false;
    }
    return true;
  }

  ///Разность между требуемым и имеющимся уровнями навыка влияет на итоговую
  ///длительность. Возвращает относительное изменение длительности миссии по
  ///сравнению с базовой для навыка [skill]
  double _calculatePenalty(Skill skill) {
    double relativeSkill =
        (requirements[skill] - getMaxSkillValue(skill)) / 100.0;
    double result = min(relativeSkill.abs(), maxPenalties[skill]);
    if (relativeSkill < 0)
      return -result;
    else
      return result;
  }

  Duration _calculateDuration() {
    double totalPenalty = 0.0;
    for (final Skill skill in maxPenalties.keys) {
      totalPenalty += _calculatePenalty(skill);
    }
    return baseDuration + baseDuration * totalPenalty;
  }

  void _init() {
    _duration = baseDuration;
    _requiredCharacters = _charactersNumber[difficulty];
    reward = MissionReward(
        credits: baseReward.credits,
        xp: baseReward.xp,
        wantedLevel: baseReward.wantedLevel);
  }
}
