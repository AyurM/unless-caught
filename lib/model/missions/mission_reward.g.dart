// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mission_reward.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MissionReward _$MissionRewardFromJson(Map<String, dynamic> json) {
  return MissionReward(
    credits: json['credits'] as int,
    xp: json['xp'] as int,
    wantedLevel: json['wantedLevel'] as int,
  );
}

Map<String, dynamic> _$MissionRewardToJson(MissionReward instance) =>
    <String, dynamic>{
      'credits': instance.credits,
      'xp': instance.xp,
      'wantedLevel': instance.wantedLevel,
    };
