// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_position.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapPosition _$MapPositionFromJson(Map<String, dynamic> json) {
  return MapPosition(
    (json['top'] as num)?.toDouble(),
    (json['left'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$MapPositionToJson(MapPosition instance) =>
    <String, dynamic>{
      'top': instance.top,
      'left': instance.left,
    };
