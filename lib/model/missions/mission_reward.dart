import 'package:json_annotation/json_annotation.dart';

part 'mission_reward.g.dart';

@JsonSerializable()
class MissionReward {
  int credits;
  int xp;
  int wantedLevel;

  MissionReward({this.credits = 0, this.xp = 0, this.wantedLevel = 0});

  factory MissionReward.fromJson(Map<String, dynamic> json) =>
      _$MissionRewardFromJson(json);

  Map<String, dynamic> toJson() => _$MissionRewardToJson(this);
}
