import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/events/event_generator.dart';
import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/missions/mission_generator.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:flutter/foundation.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:json_annotation/json_annotation.dart';

part 'mission_pool.g.dart';

@JsonSerializable()
class MissionPool extends ChangeNotifier {
  final List<Mission> available;
  final List<Mission> inProgress;

  //При наступлении события все миссии нужно поставить на паузу.
  //После окончания события миссии надо возобновить. Но если
  //миссия была на паузе до наступления события, после окончания
  //возобновлять ее не надо
  final List<Mission> _pausedByEvent = [];

  //Коллбэки передаются из GameWorld
  @JsonKey(ignore: true)
  void Function(Mission) onMissionCompleteWorldCallback;
  @JsonKey(ignore: true)
  void Function(Mission) onMissionFailWorldCallback;

  List<int> completedMissions = [0, 0, 0];
  List<int> failedMissions = [0, 0, 0];

  MissionGenerator _generator = MissionGenerator();
  EventGenerator _eventGenerator = EventGenerator();
  EventHandler _eventHandler;
  EventHandler get eventHandler => _eventHandler;

  MissionPool(this.available, this.inProgress,
      {this.onMissionCompleteWorldCallback, this.onMissionFailWorldCallback}) {
    _eventHandler = EventHandler(onMissionEventStart, onMissionEventComplete);

    //Если игра была загружена из JSON-файла, то надо передать ссылку на этот MissionPool
    available.forEach((m) => m.missionPool = this);
    inProgress.forEach((m) => m.missionPool = this);
  }

  factory MissionPool.fromJson(Map<String, dynamic> json) =>
      _$MissionPoolFromJson(json);

  Map<String, dynamic> toJson() => _$MissionPoolToJson(this);

  void createInitialMissions(
      Specialization mainSpec, List<Specialization> specs) {
    List<Mission> initialMissions =
        _generator.createInitialMissions(mainSpec, specs, this);
    available.addAll(initialMissions);
    notifyListeners();
  }

  void addDirtyCopsMission() {
    addAvailableMission(_generator.createDirtyCopsMission(this));
  }

  void removeDirtyCopsMission() {
    available.removeWhere((mission) => mission.baseReward.wantedLevel < 0);
    notifyListeners();
  }

  bool hasDirtyCopsMission() {
    return available.firstWhere((mission) => mission.baseReward.wantedLevel < 0,
            orElse: () => null) !=
        null;
  }

  void startMission(Mission mission) {
    if (!available.contains(mission)) return;

    mission.start();
  }

  void addAvailableMission(Mission mission) {
    available.add(mission);
    notifyListeners();
  }

  void onMissionStart(Mission mission) {
    //При первом запуске миссии запланировать события для нее
    if (mission.status == MissionStatus.available)
      _eventHandler.addEventSchedules(
          mission, _eventGenerator.createEventsSchedule(mission));

    available.remove(mission);
    if (!inProgress.contains(mission)) inProgress.add(mission);
    notifyListeners();
  }

  void onMissionComplete(Mission mission) {
    _eventHandler.onMissionEnd(mission, true);
    inProgress.remove(mission);
    completedMissions[mission.difficulty]++;
    _addMissions();

    if (onMissionCompleteWorldCallback != null)
      onMissionCompleteWorldCallback(mission);

    notifyListeners();
  }

  void onMissionFail(Mission mission) {
    _eventHandler.onMissionEnd(mission, false);
    inProgress.remove(mission);
    failedMissions[mission.difficulty]++;
    _addMissions();

    if (onMissionFailWorldCallback != null) onMissionFailWorldCallback(mission);

    notifyListeners();
  }

  void onMissionEventStart(Mission mission) {
    //при возникновении события поставить на паузу все миссии,
    //выполняющиеся в данный момент
    inProgress.forEach((m) {
      if (m.status == MissionStatus.inProgress) {
        m.pause();
        _pausedByEvent.add(m);
      }
    });
    notifyListeners();
  }

  void onMissionEventComplete(Mission mission) {
    //после завершения события возобновить миссии,
    //которые были остановлены в onMissionEventStart()
    _pausedByEvent.forEach((m) {
      m.start();
    });
    _pausedByEvent.clear();
    notifyListeners();
  }

  ///Возвращает true, если хотя бы одна доступная миссия может быть начата со
  ///списком персонажей [characters].
  bool hasDoableMission(List<Character> characters) {
    bool result = false;
    for (int i = 0; i < available.length; i++) {
      if (available[i].canBeStarted(characters)) {
        result = true;
        break;
      }
    }
    return result;
  }

  void reset() {
    //TODO: при старте новой игры миссии inProgress завершаются уже в новой игре
    available.clear();
    inProgress.clear();
    completedMissions = [0, 0, 0];
    failedMissions = [0, 0, 0];
  }

  int getTotalMissions() {
    return getTotalMissionsCompleted() + getTotalMissionsFailed();
  }

  int getTotalMissionsCompleted() {
    return completedMissions.fold(0, (p, c) => p + c);
  }

  int getTotalMissionsFailed() {
    return failedMissions.fold(0, (p, c) => p + c);
  }

  void _addMissions() {
    List<Mission> missions = _generator.addMissions(this);
    if (missions == null) return;

    available.addAll(missions);
    notifyListeners();
  }
}
