import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/foundation.dart';

part 'item.g.dart';

enum ItemSlot { head, hand, body, backpack }

abstract class Item extends ChangeNotifier {
  ///При продаже исходная стоимость предмета делится на [sellCoefficient]
  static const int sellCoefficient = 2;

  final String name;
  final String imagePath;
  final int price;

  Item(this.name, this.imagePath, {this.price = 0});
}

@JsonSerializable()
class Equipment extends Item {
  final Map<Skill, int> stats;
  final ItemSlot slot;

  Equipment(this.stats, this.slot, String name, String imagePath,
      {int price = 0})
      : super(name, imagePath, price: price);

  factory Equipment.fromJson(Map<String, dynamic> json) =>
      _$EquipmentFromJson(json);

  Map<String, dynamic> toJson() => _$EquipmentToJson(this);

  bool canBeEquippedBy(Character character) {
    return true;
  }
}

@JsonSerializable()
class ClassEquipment extends Equipment {
  final List<Specialization> requiredClasses;

  ClassEquipment(this.requiredClasses, Map<Skill, int> stats, ItemSlot slot,
      String name, String imagePath,
      {int price = 0})
      : super(stats, slot, name, imagePath, price: price);

  factory ClassEquipment.fromJson(Map<String, dynamic> json) =>
      _$ClassEquipmentFromJson(json);

  Map<String, dynamic> toJson() => _$ClassEquipmentToJson(this);

  @override
  bool canBeEquippedBy(Character character) {
    //проверяются только требования к специальности
    if (requiredClasses == null || requiredClasses.isEmpty) return true;

    for (final Specialization spec in character.specializations) {
      if (requiredClasses.contains(spec)) return true;
    }
    return false;
  }
}
