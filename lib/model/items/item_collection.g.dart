// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_collection.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemCollection<Item> _$ItemCollectionFromJson<Item>(Map<String, dynamic> json) {
  return ItemCollection<Item>(
    items: (json['items'] as List)?.map(_Converter<Item>().fromJson)?.toList(),
  );
}

Map<String, dynamic> _$ItemCollectionToJson<Item>(
        ItemCollection<Item> instance) =>
    <String, dynamic>{
      'items': instance.items?.map(_Converter<Item>().toJson)?.toList(),
    };
