// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemStore _$ItemStoreFromJson(Map<String, dynamic> json) {
  return ItemStore(
    json['items'] == null
        ? null
        : ItemCollection.fromJson(json['items'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ItemStoreToJson(ItemStore instance) => <String, dynamic>{
      'items': instance.items,
    };
