import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'item_shop.g.dart';

enum ItemPurchaseState { affordable, purchased, tooExpensive }

@JsonSerializable()
class ItemShop extends ChangeNotifier {
  final String name;
  final String imagePath;
  final MapPosition position;
  final ItemCollection<Item> items;

  //Буфер для покупаемых предметов. Если купленные предметы удалять сразу, то
  //возникают проблемы с прокруткой ассортимента в ItemsPageView
  final List<Item> _purchased = [];

  ItemShop(this.name, this.imagePath, this.position, this.items);

  factory ItemShop.fromJson(Map<String, dynamic> json) =>
      _$ItemShopFromJson(json);

  Map<String, dynamic> toJson() => _$ItemShopToJson(this);

  void addItem(Item item) {
    items.addItem(item);
    notifyListeners();
  }

  void buyItem(Item item) {
    //Удаление купленных товаров из списка будет выполнено после выхода из
    //экрана магазина, а пока что они помещаются в буфер
    _purchased.add(item);
    notifyListeners();
  }

  ItemPurchaseState canBePurchased(Item item, int money) {
    if (_purchased.contains(item))
      return ItemPurchaseState.purchased;
    else
      return money >= item.price
          ? ItemPurchaseState.affordable
          : ItemPurchaseState.tooExpensive;
  }

  void finishShopping() {
    //Вызывается при выходе из экрана магазина, удаляются купленные предметы и
    //очищается буфер покупок
    _purchased.forEach((i) => items.removeItem(i));
    _purchased.clear();
  }

  void reset() {
    items.clear();
    _purchased.clear();
    notifyListeners();
  }
}
