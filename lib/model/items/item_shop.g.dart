// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_shop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemShop _$ItemShopFromJson(Map<String, dynamic> json) {
  return ItemShop(
    json['name'] as String,
    json['imagePath'] as String,
    json['position'] == null
        ? null
        : MapPosition.fromJson(json['position'] as Map<String, dynamic>),
    json['items'] == null
        ? null
        : ItemCollection.fromJson(json['items'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ItemShopToJson(ItemShop instance) => <String, dynamic>{
      'name': instance.name,
      'imagePath': instance.imagePath,
      'position': instance.position,
      'items': instance.items,
    };
