// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_pool.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemPool _$ItemPoolFromJson(Map<String, dynamic> json) {
  return ItemPool(
    (json['shops'] as List)
        ?.map((e) =>
            e == null ? null : ItemShop.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ItemPoolToJson(ItemPool instance) => <String, dynamic>{
      'shops': instance.shops,
    };
