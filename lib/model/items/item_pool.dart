import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/items/item_generator.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'item_pool.g.dart';

//Кол-во пройденных миссий: [минТир, максТир] генерируемых предметов
const Map<int, List<int>> _shopRefreshMap = {
  6: [0, 1],
  10: [1, 1],
  14: [1, 2],
  19: [1, 2],
  24: [2, 2],
  30: [2, 2]
};
const int _showFirstShop = 3;
const int _missionsPerRefresh = 6;

@JsonSerializable()
class ItemPool extends ChangeNotifier {
  final List<ItemShop> shops;
  final ItemGenerator _generator = ItemGenerator();

  ItemPool(this.shops);

  factory ItemPool.fromJson(Map<String, dynamic> json) =>
      _$ItemPoolFromJson(json);

  Map<String, dynamic> toJson() => _$ItemPoolToJson(this);

  void _addTestShop() {
    ItemShop testShop = ItemShop("Магазин", "images/cart.png",
        MapPosition(80, 60), ItemCollection<Item>(items: []));

    _addInitialItems(testShop, 0);
    shops.add(testShop);
    notifyListeners();
  }

//  void addShop(ItemShop shop) {
//    shops.add(shop);
//    notifyListeners();
//  }
//
//  void removeShop(ItemShop shop) {
//    shops.remove(shop);
//    notifyListeners();
//  }

  void reset() {
    shops.clear();
    notifyListeners();
  }

  void refreshShops(int totalMissionsFinished) {
    if (totalMissionsFinished == _showFirstShop) {
      _addTestShop();
      return;
    }

    if (_shopRefreshMap.containsKey(totalMissionsFinished))
      shops.forEach((shop) {
        _addRandomClassEquipment(
            shop,
            _shopRefreshMap[totalMissionsFinished][0],
            _shopRefreshMap[totalMissionsFinished][1]);
        _addRandomEquipment(shop, tier: _getItemsTier(totalMissionsFinished));
      });

    //после максимального кол-ва миссий, указанного в _shopRefreshMap, обновлять
    //предметы через _missionsPerRefresh миссий
    if (totalMissionsFinished > _shopRefreshMap.keys.last &&
        (_shopRefreshMap.keys.last - totalMissionsFinished) %
                _missionsPerRefresh ==
            0)
      shops.forEach((shop) {
        _addRandomClassEquipment(shop, 2, 2);
        _addRandomEquipment(shop, tier: _getItemsTier(totalMissionsFinished));
      });
  }

  int _getItemsTier(int totalMissionsFinished) {
    if (_shopRefreshMap.containsKey(totalMissionsFinished))
      return _shopRefreshMap[totalMissionsFinished][1];
    else
      return 2;
  }

  ///Добавляет в магазин [shop] случайные классовые предметы
  void _addRandomClassEquipment(ItemShop shop, int minTier, int maxTier) {
    int min = 3;
    int max = 5;
    List<Item> items = _generator.createRandomNumberClassEquipments(
        min, max, minTier, maxTier,
        differentSpecs: true);
    items.forEach((item) => shop.addItem(item));
  }

  ///Добавляет в магазин [shop] случайные предметы, не требующие специализации
  void _addRandomEquipment(ItemShop shop, {int tier}) {
    int itemsToAdd = 1;
    List<Item> items =
        _generator.createRandomEquipments(itemsToAdd, tier: tier);
    items.forEach((item) => shop.addItem(item));
  }

  void _addInitialItems(ItemShop shop, int tier) {
    //добавить в ассортимент магазина предметы для каждой специализации
    for (int i = 0; i < Specialization.values.length; i++) {
      shop.addItem(_createClassEquipment(
          specialization: Specialization.values[i], tier: tier));
    }
    shop.addItem(_createEquipment(tier: tier));
  }

  Item _createClassEquipment({Specialization specialization, int tier}) {
    return _generator.createClassEquipment(
        specialization: specialization, tier: tier);
  }

  Item _createEquipment({int tier}) {
    return _generator.createEquipment(tier: tier);
  }
}
