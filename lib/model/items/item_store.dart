import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'item_store.g.dart';

@JsonSerializable()
class ItemStore extends ChangeNotifier {
  final ItemCollection<Item> items;

  //Буфер для продаваемых предметов
  final List<Item> _sold = [];

  ItemStore(this.items);

  factory ItemStore.fromJson(Map<String, dynamic> json) =>
      _$ItemStoreFromJson(json);

  Map<String, dynamic> toJson() => _$ItemStoreToJson(this);

  void addItem(Item item) {
    items.addItem(item);
    notifyListeners();
  }

  void removeItem(Item item) {
    items.removeItem(item);
    notifyListeners();
  }

  void sellItem(Item item) {
    _sold.add(item);
    notifyListeners();
  }

  bool isItemSold(Item item) {
    return _sold.contains(item);
  }

  int getNetWorth() {
    int result = 0;
    items.items
        .forEach((item) => result += (item.price ~/ Item.sellCoefficient));
    return result;
  }

  void finishSelling() {
    //Вызывается при выходе из экрана предметов, удаляются проданные предметы и
    //очищается буфер продаж
    _sold.forEach((i) => items.removeItem(i));
    _sold.clear();
  }
}
