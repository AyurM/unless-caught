import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_content.dart';
import 'package:crime_game/strings/strings_items.dart';

const Map<Skill, List<EquipmentContent>> skillItems = {
  Skill.driving: drivingItems,
  Skill.hacking: hackingItems,
  Skill.lockpick: lockpickItems,
  Skill.guns: gunsItems,
  Skill.speech: speechItems,
};

const List<EquipmentContent> lockpickItems = [
  EquipmentContent(StringsItems.lockpickItem01, "images/items/item_stethoscope.jpg",
      {Skill.lockpick: 3}, 70, ItemSlot.backpack),
  EquipmentContent(StringsItems.lockpickItem02, "images/items/item_lockpick.jpg",
      {Skill.lockpick: 5}, 120, ItemSlot.backpack),
  EquipmentContent(StringsItems.lockpickItem03, "images/items/item_xray_goggles.jpg",
      {Skill.lockpick: 7}, 250, ItemSlot.head)
];
const List<EquipmentContent> drivingItems = [
  EquipmentContent(StringsItems.drivingItem01, "images/items/item_jacket.jpg",
      {Skill.driving: 3}, 70, ItemSlot.body),
  EquipmentContent(StringsItems.drivingItem02, "images/items/item_ar_goggles.jpg",
      {Skill.driving: 5}, 120, ItemSlot.head),
  EquipmentContent(StringsItems.drivingItem03, "images/items/item_driving_ai.jpg",
      {Skill.driving: 7}, 250, ItemSlot.backpack)
];
const List<EquipmentContent> gunsItems = [
  EquipmentContent(StringsItems.gunsItem01, "images/items/item_vest.jpg",
      {Skill.guns: 3}, 70, ItemSlot.body),
  EquipmentContent(StringsItems.gunsItem02, "images/items/item_recoil_absorber.jpg",
      {Skill.guns: 5}, 120, ItemSlot.hand),
  EquipmentContent(StringsItems.gunsItem03, "images/items/item_monocle.jpg",
      {Skill.guns: 7}, 250, ItemSlot.head)
];
const List<EquipmentContent> hackingItems = [
  EquipmentContent(StringsItems.hackingItem01, "images/items/item_card.jpg",
      {Skill.hacking: 3}, 70, ItemSlot.backpack),
  EquipmentContent(StringsItems.hackingItem02, "images/items/item_jammer.jpg",
      {Skill.hacking: 5}, 120, ItemSlot.backpack),
  EquipmentContent(StringsItems.hackingItem03, "images/items/item_cyberdeck.jpg",
      {Skill.hacking: 7}, 250, ItemSlot.backpack)
];
const List<EquipmentContent> speechItems = [
  EquipmentContent(StringsItems.speechItem01, "images/items/item_psychology_book.jpg",
      {Skill.speech: 3}, 70, ItemSlot.backpack),
  EquipmentContent(StringsItems.speechItem02, "images/items/item_suit.jpg",
      {Skill.speech: 5}, 120, ItemSlot.body),
  EquipmentContent(StringsItems.speechItem03, "images/items/item_earbuds.jpg",
      {Skill.speech: 7}, 250, ItemSlot.head)
];

const List<EquipmentContent> commonItems = [
  EquipmentContent(StringsItems.commonItem01, "images/items/item_gloves.jpg",
      {Skill.driving: 3, Skill.guns: 3}, 140, ItemSlot.hand),
];
