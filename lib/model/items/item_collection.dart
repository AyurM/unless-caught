import 'package:crime_game/model/items/item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'item_collection.g.dart';

@JsonSerializable()
class ItemCollection<Item> {
  static const Map<ItemSlot, int> characterSlotLimits = {
    ItemSlot.head: 1,
    ItemSlot.hand: 1,
    ItemSlot.backpack: 4,
    ItemSlot.body: 1
  };

  @JsonKey(ignore: true)
  Map<ItemSlot, int> limits;

  //Статистика по занятым слотам
  @JsonKey(ignore: true)
  Map<ItemSlot, int> slots = {
    ItemSlot.head: 0,
    ItemSlot.hand: 0,
    ItemSlot.backpack: 0,
    ItemSlot.body: 0
  };

  @_Converter()
  final List<Item> items;

  ItemCollection({this.items}) {
    //обновить сведения о занятых слотах
    items.forEach((item) {
      if (item is Equipment) {
        slots[item.slot]++;
      }
    });
  }

  factory ItemCollection.fromJson(Map<String, dynamic> json) =>
      _$ItemCollectionFromJson<Item>(json);

  Map<String, dynamic> toJson() => _$ItemCollectionToJson(this);

  bool canBeAdded(Item item) {
    //В данной коллекции нет ограничений по слотам (например, магазин или хранилище)
    if (limits == null) return true;

    //Если ограничения по слотам есть, то должен быть свободный слот нужного типа
    if (item is Equipment) return slots[item.slot] < limits[item.slot];

    return true;
  }

  void addItem(Item item) {
    if (!canBeAdded(item)) return;

    items.add(item);
    if (item is Equipment) slots[item.slot]++;
  }

  void removeItem(Item item) {
    if (!items.contains(item)) return;

    items.remove(item);
    if (item is Equipment) slots[item.slot]--;
  }

  void clear() {
    items.clear();
    slots = {
      ItemSlot.head: 0,
      ItemSlot.hand: 0,
      ItemSlot.backpack: 0,
      ItemSlot.body: 0
    };
  }

  List<Item> getItemsWithSlots(ItemSlot slot) {
    List<Item> result = [];
    items.forEach((item) {
      if (item is Equipment && item.slot == slot) result.add(item);
    });
    return result;
  }
}

class _Converter<Item> implements JsonConverter<Item, Object> {
  const _Converter();

  @override
  Item fromJson(Object json) {
    if (json is Map<String, dynamic> && json.containsKey('requiredClasses')) {
      return ClassEquipment.fromJson(json) as Item;
    } else {
      return Equipment.fromJson(json) as Item;
    }
    // This will only work if `json` is a native JSON type:
    //   num, String, bool, null, etc
    // *and* is assignable to `T`.
//    return json as T;
  }

  @override
  Object toJson(Item object) {
    // This will only work if `object` is a native JSON type:
    //   num, String, bool, null, etc
    // Or if it has a `toJson()` function`.
    return object;
  }
}
