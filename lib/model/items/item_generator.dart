import 'dart:math';

import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_content.dart';
import 'package:crime_game/model/items/item_descriptions.dart';
import 'package:crime_game/utils/utils.dart';

const List<double> tierStatMultiplier = [1, 1.4, 1.8, 2.2, 2.6];

class ItemGenerator {
  final Random _random = Random(DateTime.now().millisecond);

  Item createClassEquipment({Specialization specialization, int tier}) {
    if (specialization == null) {
      specialization =
          Specialization.values[_random.nextInt(Specialization.values.length)];
    }

    if (tier == null) tier = _random.nextInt(3);

    List<EquipmentContent> contents = skillItems[mainSkillMap[specialization]];
    EquipmentContent content = contents[min(tier, 2)];

    return ClassEquipment(
        [specialization],
        content.stats,
        content.slot,
        content.name,
        content.imagePath,
        price: content.price);
  }

  ///Создать набор случайных классовых предметов из [amount] предметов.
  ///Если [differentSpecs] = true и [amount] не больше количества специализаций,
  ///то будут созданы предметы для неповторяющихся специализаций.
  List<Item> createExactNumberClassEquipments(int amount,
      {int tier, bool differentSpecs = false}) {
    List<Item> result = [];
    List<Specialization> allSpecs = []..addAll(Specialization.values);

    for (int i = 0; i < amount; i++) {
      Specialization randomSpec = allSpecs[_random.nextInt(allSpecs.length)];

      if (differentSpecs && amount <= Specialization.values.length)
        allSpecs.remove(randomSpec);

      result.add(createClassEquipment(specialization: randomSpec, tier: tier));
    }
    return result;
  }

  ///Создать от [min] до [max] случайных классовых предметов. Уровень каждого
  ///предмета выбирается случайно от [minTier] до [maxTier].
  ///Если [differentSpecs] = true и [amount] не больше количества специализаций,
  ///то будут созданы предметы для неповторяющихся специализаций.
  List<Item> createRandomNumberClassEquipments(int min, int max,
      int minTier, int maxTier, {bool differentSpecs = false}) {
    List<Item> result = [];
    List<Specialization> allSpecs = []..addAll(Specialization.values);

    int amount = min + _random.nextInt(max - min + 1);

    for (int i = 0; i < amount; i++) {
      Specialization randomSpec = allSpecs[_random.nextInt(allSpecs.length)];

      if (differentSpecs && amount <= Specialization.values.length)
        allSpecs.remove(randomSpec);

      int tier = minTier + _random.nextInt(maxTier - minTier + 1);
      result.add(createClassEquipment(specialization: randomSpec, tier: tier));
    }
    return result;
  }

  List<Item> createRandomEquipments(int amount, {int tier}) {
    List<Item> result = [];

    for (int i = 0; i < amount; i++) {
      result.add(createEquipment(tier: tier));
    }
    return result;
  }

  Item createEquipment({int tier}) {
    if (tier == null) tier = _random.nextInt(tierStatMultiplier.length);
    EquipmentContent content = commonItems[_random.nextInt(commonItems.length)];

    return Equipment(_getTierStats(content.stats, tierStatMultiplier[tier]),
        content.slot, content.name, content.imagePath,
        price: _getPrice(content.price, tier));
  }

  Map<Skill, int> _getTierStats(
      Map<Skill, int> baseStats, double tierMultiplier) {
    Map<Skill, int> result = {};
    for (final Skill skill in baseStats.keys)
      result[skill] = (baseStats[skill] * tierMultiplier).round();
    return result;
  }

  int _getPrice(int basePrice, int tier) {
    return basePrice + basePrice * tier;
  }
}
