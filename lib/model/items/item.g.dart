// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Equipment _$EquipmentFromJson(Map<String, dynamic> json) {
  return Equipment(
    (json['stats'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(_$enumDecodeNullable(_$SkillEnumMap, k), e as int),
    ),
    _$enumDecodeNullable(_$ItemSlotEnumMap, json['slot']),
    json['name'] as String,
    json['imagePath'] as String,
    price: json['price'] as int,
  );
}

Map<String, dynamic> _$EquipmentToJson(Equipment instance) => <String, dynamic>{
      'name': instance.name,
      'imagePath': instance.imagePath,
      'price': instance.price,
      'stats': instance.stats?.map((k, e) => MapEntry(_$SkillEnumMap[k], e)),
      'slot': _$ItemSlotEnumMap[instance.slot],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$SkillEnumMap = {
  Skill.driving: 'driving',
  Skill.hacking: 'hacking',
  Skill.lockpick: 'lockpick',
  Skill.guns: 'guns',
  Skill.speech: 'speech',
};

const _$ItemSlotEnumMap = {
  ItemSlot.head: 'head',
  ItemSlot.hand: 'hand',
  ItemSlot.body: 'body',
  ItemSlot.backpack: 'backpack',
};

ClassEquipment _$ClassEquipmentFromJson(Map<String, dynamic> json) {
  return ClassEquipment(
    (json['requiredClasses'] as List)
        ?.map((e) => _$enumDecodeNullable(_$SpecializationEnumMap, e))
        ?.toList(),
    (json['stats'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(_$enumDecodeNullable(_$SkillEnumMap, k), e as int),
    ),
    _$enumDecodeNullable(_$ItemSlotEnumMap, json['slot']),
    json['name'] as String,
    json['imagePath'] as String,
    price: json['price'] as int,
  );
}

Map<String, dynamic> _$ClassEquipmentToJson(ClassEquipment instance) =>
    <String, dynamic>{
      'name': instance.name,
      'imagePath': instance.imagePath,
      'price': instance.price,
      'stats': instance.stats?.map((k, e) => MapEntry(_$SkillEnumMap[k], e)),
      'slot': _$ItemSlotEnumMap[instance.slot],
      'requiredClasses': instance.requiredClasses
          ?.map((e) => _$SpecializationEnumMap[e])
          ?.toList(),
    };

const _$SpecializationEnumMap = {
  Specialization.driver: 'driver',
  Specialization.hacker: 'hacker',
  Specialization.burglar: 'burglar',
  Specialization.gunfighter: 'gunfighter',
  Specialization.talker: 'talker',
};
