import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/items/item.dart';

class ItemContent {
  final String name;
  final String imagePath;
  final Map<Skill, int> stats;
  final int price;

  const ItemContent(this.name, this.imagePath, this.stats, this.price);
}

class EquipmentContent extends ItemContent {
  final ItemSlot slot;

  const EquipmentContent(String name, String imagePath, Map<Skill, int> stats, int price, this.slot) :
        super(name, imagePath, stats, price);
}
