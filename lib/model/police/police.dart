import 'dart:math';

import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/police/police_event.dart';
import 'package:crime_game/model/police/police_event_handler.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'police.g.dart';

@JsonSerializable()
class Police extends ChangeNotifier {
  static const int missionsToAppear = 4;

  //Arrest probability calculations
  static const int _minWantedLevel = 33;
  static const int _maxWantedLevel = 100;
  static const double _minArrestChance = 0.0;
  static const double _maxArrestChance = 1.0;
  static const double _arrestPower = 2.0;

  static const int _minWantedLevelAfterRelease = 10;
  static const int _maxWantedLevelAfterRelease = 15;

  static const int _minBail = 500;
  static const int _maxBail = 1000;
  static const int _minMissionsLeft = 3;
  static const int _maxMissionsLeft = 5;

  final List<ArrestedCharacter> arrested;
  //Временный буфер для хранения освобожденных персонажей, очищается при выходе
  //с экрана внесения залога
  final List<ArrestedCharacter> _released = [];

  final MapPosition position;
  bool isActive;

  @JsonKey(ignore: true)
  final Random _random = Random();
  @JsonKey(ignore: true)
  final PoliceEventHandler eventHandler = PoliceEventHandler();

  Police(this.arrested, this.position, {this.isActive = false});

  factory Police.fromJson(Map<String, dynamic> json) => _$PoliceFromJson(json);

  Map<String, dynamic> toJson() => _$PoliceToJson(this);

  void onMissionFinished(Gang gang) {
    _refreshArrested();

    List<Character> characters = [gang.mainCharacter, ...gang.members];
    List<ArrestedCharacter> arrestedCharacters = [];
    characters.forEach((character) {
      if (_isArrested(character)) {
        character.onArrest();
        ArrestedCharacter arrestedCharacter =
            ArrestedCharacter(character, _getBail(character));
        //Добавить арестованного персонажа в общий список арестованных и в список
        //персонажей, арестованных после последней миссии. Этот список будет
        //показан игроку в отдельном диалоговом окне
        arrested.add(arrestedCharacter);
        arrestedCharacters.add(arrestedCharacter);
      }
    });

    if (arrestedCharacters.isNotEmpty) {
      gang.onArrestComplete(arrestedCharacters);
      eventHandler.raiseEvent(PoliceEvent(arrestedCharacters));
    }

    notifyListeners();
  }

  void releaseCharacter(ArrestedCharacter arrestedCharacter, Gang gang) {
    _released.add(arrestedCharacter);
    //Сбросить уровень разыскиваемости освобожденного персонажа
    arrestedCharacter.character.wantedLevel = _minWantedLevelAfterRelease +
        _random.nextInt(
            _maxWantedLevelAfterRelease - _minWantedLevelAfterRelease + 1);

    //Списать сумму залога и вернуть персонажа в банду
    gang.addMoney(-arrestedCharacter.bail.bail);
    gang.onCharacterRelease(arrestedCharacter.character);
    arrestedCharacter.release();
    notifyListeners();
  }

  void finishReleasing() {
    //Вызывается при выходе из экрана арестованных персонажей для очистки списков
    _released.forEach((i) => arrested.remove(i));
    _released.clear();
    notifyListeners();
  }

  void show() {
    isActive = true;
    notifyListeners();
  }

  void hide() {
    isActive = false;
    notifyListeners();
  }

  void reset() {
    isActive = false;
    arrested.clear();
    notifyListeners();
  }

  void _refreshArrested() {
    arrested.forEach((aChar) => aChar.bail.missionsLeft--);
    //Удалить персонажей, за которых не был вовремя внесен залог
    arrested.removeWhere((aChar) => aChar.bail.missionsLeft < 0);
  }

  bool _isArrested(Character character) {
    return _random.nextDouble() <= _getArrestChance(character.wantedLevel);
  }

  double _getArrestChance(int wantedLevel) {
    if (wantedLevel <= _minWantedLevel) return _minArrestChance;
    if (wantedLevel >= _maxWantedLevel) return _maxArrestChance;

    return pow(
            (wantedLevel - _minWantedLevel) /
                (_maxWantedLevel - _minWantedLevel),
            _arrestPower) *
        (_maxArrestChance - _minArrestChance);
  }

  ///Назначает сумму и срок внесения залога для персонажа [character]
  Bail _getBail(Character character) {
    return Bail(_getBailAmount(character), _getMissionsLeft(character));
  }

  int _getBailAmount(Character character) {
    return ((_maxBail + _minBail) / 2).round();
  }

  int _getMissionsLeft(Character character) {
    return _minMissionsLeft +
        _random.nextInt(_maxMissionsLeft - _minMissionsLeft + 1);
  }
}

@JsonSerializable()
class ArrestedCharacter extends ChangeNotifier {
  Character character;
  Bail bail;

  ArrestedCharacter(this.character, this.bail);

  bool canBeBailedOut(int money) {
    return bail.bail <= money;
  }

  void release() {
    character.isArrested = false;
    notifyListeners();
  }

  factory ArrestedCharacter.fromJson(Map<String, dynamic> json) =>
      _$ArrestedCharacterFromJson(json);

  Map<String, dynamic> toJson() => _$ArrestedCharacterToJson(this);
}

@JsonSerializable()
class Bail {
  int bail;
  int missionsLeft;

  Bail(this.bail, this.missionsLeft);

  factory Bail.fromJson(Map<String, dynamic> json) => _$BailFromJson(json);

  Map<String, dynamic> toJson() => _$BailToJson(this);
}
