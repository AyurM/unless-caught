// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'police.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Police _$PoliceFromJson(Map<String, dynamic> json) {
  return Police(
    (json['arrested'] as List)
        ?.map((e) => e == null
            ? null
            : ArrestedCharacter.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['position'] == null
        ? null
        : MapPosition.fromJson(json['position'] as Map<String, dynamic>),
    isActive: json['isActive'] as bool,
  );
}

Map<String, dynamic> _$PoliceToJson(Police instance) => <String, dynamic>{
      'arrested': instance.arrested,
      'position': instance.position,
      'isActive': instance.isActive,
    };

ArrestedCharacter _$ArrestedCharacterFromJson(Map<String, dynamic> json) {
  return ArrestedCharacter(
    json['character'] == null
        ? null
        : Character.fromJson(json['character'] as Map<String, dynamic>),
    json['bail'] == null
        ? null
        : Bail.fromJson(json['bail'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ArrestedCharacterToJson(ArrestedCharacter instance) =>
    <String, dynamic>{
      'character': instance.character,
      'bail': instance.bail,
    };

Bail _$BailFromJson(Map<String, dynamic> json) {
  return Bail(
    json['bail'] as int,
    json['missionsLeft'] as int,
  );
}

Map<String, dynamic> _$BailToJson(Bail instance) => <String, dynamic>{
      'bail': instance.bail,
      'missionsLeft': instance.missionsLeft,
    };
