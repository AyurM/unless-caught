import 'dart:math';

import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/police/dirty_police.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'bribe.g.dart';

@JsonSerializable()
class Bribe extends ChangeNotifier {
  static const int creditsPerWantedPoint = 25;
  static const int _bribeCharactersNumber = 3;
  static const List<int> _wantedReduction = [10, 20];

  @JsonKey(ignore: true)
  Gang gang;
  @JsonKey(ignore: true)
  DirtyPolice dirtyPolice;
  final MapPosition position;
  final Random _random = Random();

  @JsonKey(ignore: true)
  List<int> wantedReductions = [];
  @JsonKey(ignore: true)
  List<Character> characters;
  @JsonKey(ignore: true)
  List<Character> selected = [];
  @JsonKey(ignore: true)
  int totalBribe = 0;
  @JsonKey(ignore: true)
  bool done = false;

  Bribe({this.position});

  Bribe.create(this.gang, this.dirtyPolice, this.position) {
    init();
  }

  factory Bribe.fromJson(Map<String, dynamic> json) => _$BribeFromJson(json);

  Map<String, dynamic> toJson() => _$BribeToJson(this);

  void onCharacterSelect(int index) {
    if (selected.contains(characters[index])) {
      selected.remove(characters[index]);
      totalBribe -= wantedReductions[index] * creditsPerWantedPoint;
    } else {
      selected.add(characters[index]);
      totalBribe += wantedReductions[index] * creditsPerWantedPoint;
    }
    notifyListeners();
  }

  void bribe() {
    gang.addMoney(-totalBribe);
    for (int i = 0; i < selected.length; i++) {
      selected[i].addWanted(-wantedReductions[i]);
    }
    done = true;
    dirtyPolice.onBribeDone();
    init();
  }

  bool canBeBribed() {
    return selected.isNotEmpty && gang.money >= totalBribe;
  }

  void init() {
    selected.clear();
    wantedReductions.clear();
    totalBribe = 0;

    //Выбрать не более _bribeCharactersNumber самых разыскиваемых членов банды
    characters = [gang.mainCharacter, ...gang.members];
    characters.sort((a, b) => b.wantedLevel.compareTo(a.wantedLevel));
    characters = characters.take(_bribeCharactersNumber).toList();

    _createWantedReductions(characters.length);
    notifyListeners();
  }

  void _createWantedReductions(int number) {
    //Предложить снижение уровня разыскиваемости для каждого из персонажей
    for (int i = 0; i < number; i++) {
      wantedReductions.add(min(
          _wantedReduction[0] +
              _random.nextInt(_wantedReduction[1] - _wantedReduction[0] + 1),
          characters[i].wantedLevel));
    }
  }
}
