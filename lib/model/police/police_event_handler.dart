import 'package:crime_game/model/police/police_event.dart';
import 'package:flutter/foundation.dart';

class PoliceEventHandler extends ChangeNotifier{

  PoliceEvent _event;
  PoliceEvent get event => _event;

  void raiseEvent(PoliceEvent event){
    _event = event;
    notifyListeners();
  }

  void dismiss(){
    _event = null;
  }
}