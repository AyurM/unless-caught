// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dirty_police.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DirtyPolice _$DirtyPoliceFromJson(Map<String, dynamic> json) {
  return DirtyPolice(
    bribe: json['bribe'] == null
        ? null
        : Bribe.fromJson(json['bribe'] as Map<String, dynamic>),
  )
    ..missionsUntilEvent = json['missionsUntilEvent'] as int
    ..eventAvailablePeriod = json['eventAvailablePeriod'] as int;
}

Map<String, dynamic> _$DirtyPoliceToJson(DirtyPolice instance) =>
    <String, dynamic>{
      'missionsUntilEvent': instance.missionsUntilEvent,
      'eventAvailablePeriod': instance.eventAvailablePeriod,
      'bribe': instance.bribe,
    };
