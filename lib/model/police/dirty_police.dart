import 'dart:math';

import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/police/bribe.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'dirty_police.g.dart';

@JsonSerializable()
class DirtyPolice extends ChangeNotifier {
  static const List<int> dirtyCopsMissionRequirements = [40, 45];
  static const List<int> dirtyCopsMissionWantedReward = [7, 10];
  static const int dirtyCopsMissionDifficulty = 2;

  static const double _missionProbability = 0.5;
  static const List<int> _eventCoolDown = [8, 10];
  static const List<int> _eventAvailable = [2, 4];

  int missionsUntilEvent;
  int eventAvailablePeriod;
  Bribe bribe;

  final Random _random = Random();

  @JsonKey(ignore: true)
  MissionPool missionPool;
  @JsonKey(ignore: true)
  Gang gang;

  DirtyPolice({this.missionPool, this.gang, this.bribe});

  factory DirtyPolice.fromJson(Map<String, dynamic> json) =>
      _$DirtyPoliceFromJson(json);

  Map<String, dynamic> toJson() => _$DirtyPoliceToJson(this);

  void onMissionFinish() {
    if (bribe != null) bribe.init();

    if (missionPool.hasDirtyCopsMission() || bribe != null) {
      eventAvailablePeriod--;
      if (eventAvailablePeriod == 0) {
        //Игрок не воспользовался миссией/взяткой в течение eventAvailablePeriod
        //миссий, убрать миссию/взятку
        missionPool.removeDirtyCopsMission();
        bribe = null;
        eventAvailablePeriod = _getEventAvailablePeriod();
      }
    } else {
      missionsUntilEvent--;
      if (missionsUntilEvent == 0) {
        //Добавить ивент для снижения уровня разыскиваемости (миссия или взятка)
        if (_random.nextDouble() <= _missionProbability) {
          missionPool.addDirtyCopsMission();
        } else {
          bribe = Bribe.create(gang, this, MapPosition(200, 150));
        }
        missionsUntilEvent = _getEventCoolDown();
        eventAvailablePeriod = _getEventAvailablePeriod();
      }
    }

    notifyListeners();
  }

  void loadBribe() {
    if (bribe != null) {
      bribe.gang = gang;
      bribe.dirtyPolice = this;
      bribe.init();
    }
  }

  void onBribeDone() {
    bribe = null;
    gang.refreshWantedLevel();
    notifyListeners();
  }

  void reset() {
    missionsUntilEvent = _getEventCoolDown();
    eventAvailablePeriod = _getEventAvailablePeriod();
    bribe = null;
  }

  int _getEventAvailablePeriod() {
    return _eventAvailable[0] +
        _random.nextInt(_eventAvailable[1] - _eventAvailable[0] + 1);
  }

  int _getEventCoolDown() {
    return _eventCoolDown[0] +
        _random.nextInt(_eventCoolDown[1] - _eventCoolDown[0] + 1);
  }
}
