// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bribe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bribe _$BribeFromJson(Map<String, dynamic> json) {
  return Bribe(
    position: json['position'] == null
        ? null
        : MapPosition.fromJson(json['position'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$BribeToJson(Bribe instance) => <String, dynamic>{
      'position': instance.position,
    };
