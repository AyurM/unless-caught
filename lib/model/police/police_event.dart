import 'package:crime_game/model/police/police.dart';

class PoliceEvent{
  final List<ArrestedCharacter> arrested;

  PoliceEvent(this.arrested);
}