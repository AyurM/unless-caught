import 'package:crime_game/model/game_world.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class GameOverModal extends StatelessWidget {
  static const double _maxModalWidth = 400.0;
  final GameWorld gameWorld;

  const GameOverModal({Key key, @required this.gameWorld}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: _maxModalWidth),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: height / 5),
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage("images/game_over.jpg"),
                        fit: BoxFit.contain),
                    borderRadius: BorderRadius.circular(8.0)),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(Strings.gameOver,
                            textAlign: TextAlign.center,
                            style:
                                sectionHeaderTextStyle.apply(fontSizeDelta: 2)),
                      ),
                    ),
                    Expanded(child: SizedBox()),
                    _buildExitButton(context),
                    Expanded(child: SizedBox()),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildExitButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              elevation: 4,
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.exit, style: mainButtonTextStyle),
          onPressed: () => _onExitGameClick(context),
        ),
      ),
    );
  }

  void _onExitGameClick(BuildContext context) {
    gameWorld.gameOver = false;
    Navigator.popUntil(context, ModalRoute.withName("/"));
  }
}
