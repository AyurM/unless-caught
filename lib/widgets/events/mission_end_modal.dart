import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/missions/mission_end_sign.dart';
import 'package:crime_game/widgets/missions/mission_rewards_panel.dart';
import 'package:flutter/material.dart';

class MissionEndModal extends StatelessWidget {
  final EventHandler eventHandler;
  final double _missionPictureAspect = 2.3;
  final double _wantedIconSize = 24.0;

  MissionEndModal(this.eventHandler);

  static const double _maxModalWidth = 400.0;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    Mission mission = eventHandler.completedMission == null
        ? eventHandler.failedMission
        : eventHandler.completedMission;

    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: _maxModalWidth),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 20, vertical: height / 7),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 280),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.white),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _buildName(mission),
                      _buildPicture(mission),
                      _buildRewardRow(mission),
                      _buildEndSign(mission),
                      Expanded(child: SizedBox()),
                      _buildDoneButton(context)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildName(Mission mission) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8), topRight: Radius.circular(8)),
          color: difficultyColor[mission.difficulty]),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(mission.name,
            textAlign: TextAlign.center, style: listItemMissionNameTextStyle),
      ),
    );
  }

  Widget _buildPicture(Mission mission) {
    return AspectRatio(
      aspectRatio: _missionPictureAspect,
      child: Container(
        width: double.infinity,
        child: Image.asset(mission.imagePath, fit: BoxFit.cover),
      ),
    );
  }

  Widget _buildRewardRow(Mission mission) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          if (mission.status == MissionStatus.completed)
            RewardItem(
                text: mission.reward.credits.toString(),
                widget: Icon(moneyIcon, color: moneyIconColor)),
          if (mission.status == MissionStatus.completed)
            RewardItem(
                text: mission.reward.xp.toString(),
                widget: Icon(xpIcon, color: xpIconColor)),
          RewardItem(
              text: mission.reward.wantedLevel.toString(),
              widget: Container(
                width: _wantedIconSize,
                height: _wantedIconSize,
                child: Image.asset(wantedIconPath,
                    color: wantedIconColor, fit: BoxFit.cover),
              )),
        ],
      ),
    );
  }

  Widget _buildEndSign(Mission mission) {
    if (mission.status == MissionStatus.completed)
      return MissionEndSign(isCompleted: true);
    else
      return MissionEndSign(isCompleted: false);
  }

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () {
            eventHandler.completedMission == null
                ? eventHandler.onDismissMissionFailDialog()
                : eventHandler.onDismissMissionCompleteDialog();
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    eventHandler.completedMission == null
        ? eventHandler.onDismissMissionFailDialog()
        : eventHandler.onDismissMissionCompleteDialog();
    return Future<bool>.value(true);
  }
}
