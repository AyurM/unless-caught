import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/events/reward_event_effect.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/reward_effect_display.dart';
import 'package:flutter/material.dart';

class SimpleEventModal extends StatelessWidget {
  final EventHandler eventHandler;
  final SimpleMissionEvent event;

  SimpleEventModal(this.eventHandler) : this.event = eventHandler.activeEvent;

  static const double _maxModalWidth = 400.0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: _maxModalWidth),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 10, bottom: 10),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 280),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.white),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      _buildMissionName(event.mission),
                      _buildPicture(),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(event.description,
                            textAlign: TextAlign.center,
                            style: sectionHeaderTextStyle),
                      ),
                      Divider(height: 1.5),
                      _buildEffectDisplay(event.effect),
                      Expanded(child: SizedBox()),
                      _buildDoneButton(context)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMissionName(Mission mission) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: difficultyColor[mission.difficulty],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0))),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(mission.name,
            textAlign: TextAlign.center, style: infoScreenHeaderTextStyle),
      ),
    );
  }

  Widget _buildPicture() {
    if (event.imagePath == null) return SizedBox();

    return AspectRatio(
      aspectRatio: 2.0,
      child: Container(
        width: double.infinity,
        child: Image.asset(event.imagePath, fit: BoxFit.cover),
      ),
    );
  }

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () {
            eventHandler.onMissionEventEnd(event.mission);
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  Widget _buildEffectDisplay(EventEffect effect) {
    if (effect is RewardEventEffect)
      return RewardEffectDisplay(effect);
    else
      return SizedBox();
  }

  //Нажатие на кнопку "Назад" должно корректно завершать событие
  Future<bool> _onBackPressed() {
    eventHandler.onMissionEventEnd(event.mission);
    return Future<bool>.value(true);
  }
}
