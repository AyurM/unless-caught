import 'package:crime_game/model/events/choice_events/instant_choice_option.dart';
import 'package:crime_game/model/events/choice_events/skill_choice_option.dart';
import 'package:crime_game/model/events/choice_events/choice_mission_event.dart';
import 'package:crime_game/model/events/event_effect.dart';
import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/reward_effect_display.dart';
import 'package:flutter/material.dart';

class ChoiceEventModal extends StatefulWidget {
  final EventHandler eventHandler;
  final ChoiceMissionEvent event;

  ChoiceEventModal(this.eventHandler) : this.event = eventHandler.activeEvent;

  static const double _maxModalWidth = 400.0;

  @override
  _ChoiceEventModalState createState() => _ChoiceEventModalState();
}

class _ChoiceEventModalState extends State<ChoiceEventModal> {
  EventEffect _effect;
  String _effectDescription;

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: skipButtonColor,
    primary: skipButtonColor,
    padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: ConstrainedBox(
            constraints:
                const BoxConstraints(maxWidth: ChoiceEventModal._maxModalWidth),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 10, bottom: 10),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 280),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.white),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      _buildMissionName(widget.event.mission),
                      _buildPicture(),
                      _buildText(),
                      Divider(height: 1.5),
                      ..._buildBottom(context)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMissionName(Mission mission) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: difficultyColor[mission.difficulty],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0))),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(mission.name,
            textAlign: TextAlign.center, style: infoScreenHeaderTextStyle),
      ),
    );
  }

  Widget _buildPicture() {
    if (widget.event.imagePath == null) return SizedBox();

    return AspectRatio(
      aspectRatio: 2.0,
      child: Container(
        width: double.infinity,
        child: Image.asset(widget.event.imagePath, fit: BoxFit.cover),
      ),
    );
  }

  Widget _buildText() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
          _effect == null ? widget.event.description : _effectDescription,
          textAlign: TextAlign.center,
          style: sectionHeaderTextStyle),
    );
  }

  List<Widget> _buildBottom(BuildContext context) {
    List<Widget> result = [];
    if (_effect == null) {
      result.add(Expanded(child: SizedBox()));
      result.add(_buildOptionButtons(context));
    } else {
      result.add(RewardEffectDisplay(_effect));
      result.add(Expanded(child: SizedBox()));
      result.add(_buildDoneButton(context));
    }
    return result;
  }

  Widget _buildOptionButtons(BuildContext context) {
    List<Widget> widgets = widget.event.options.map((option) {
      if (option is InstantChoiceOption)
        return _buildInstantChoiceButton(option, context);
      else
        return _buildSkillChoiceButton(option as SkillChoiceOption);
    }).toList();
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: widgets,
    );
  }

  Widget _buildInstantChoiceButton(
      InstantChoiceOption option, BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: raisedButtonStyle,
          child: Text(option.buttonText, style: mainButtonTextStyle),
          onPressed: () => _chooseInstantOption(option),
        ),
      ),
    );
  }

  Widget _buildSkillChoiceButton(SkillChoiceOption option) {
    String buttonText = option.buttonText +
        " (${option.skillValue}/${option.mission.requirements[option.skill]})";
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
                primary: skillColor[option.skill],
                onPrimary: skillColor[option.skill],
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                )),
            child: Text(buttonText,
                style: mainButtonTextStyle.apply(fontSizeDelta: -6)),
            onPressed: () => _chooseSkillOption(option)),
      ),
    );
  }

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () {
            _effect.applyEffect();
            widget.eventHandler.onMissionEventEnd(widget.event.mission);
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  void _chooseInstantOption(InstantChoiceOption option) {
    setState(() {
      _effect = option.effect;
      _effectDescription = option.resultDescription;
    });
  }

  void _chooseSkillOption(SkillChoiceOption option) {
    setState(() {
      int effectIndex = option.chooseEffect();
      _effect = option.effects[effectIndex];
      _effectDescription = option.resultDescriptions[effectIndex];
    });
  }

  Future<bool> _onBackPressed() {
    //при нажатии кнопки "Назад" выбрать вариант по умолчанию
    if (_effect == null) {
      InstantChoiceOption option =
          widget.event.options.firstWhere((o) => o is InstantChoiceOption);
      if (option == null) {
        SkillChoiceOption otherOption =
            widget.event.options.firstWhere((o) => o is SkillChoiceOption);
        _chooseSkillOption(otherOption);
      } else {
        _chooseInstantOption(option);
      }
      return Future<bool>.value(false);
    } else {
      _effect.applyEffect();
      widget.eventHandler.onMissionEventEnd(widget.event.mission);
      return Future<bool>.value(true);
    }
  }
}
