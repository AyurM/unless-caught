import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/events/simple_mission_event.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/game_world.dart';
import 'package:crime_game/model/police/police_event_handler.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/events/choice_event_modal.dart';
import 'package:crime_game/widgets/events/mission_end_modal.dart';
import 'package:crime_game/widgets/events/simple_event_modal.dart';
import 'package:crime_game/widgets/characters/gang_screen.dart';
import 'package:crime_game/widgets/exit_game_modal.dart';
import 'package:crime_game/widgets/game_over_modal.dart';
import 'package:crime_game/widgets/items/items_screen.dart';
import 'package:crime_game/widgets/missions/map_screen.dart';
import 'package:crime_game/widgets/police/police_event_modal.dart';
import 'package:crime_game/widgets/stat_icon.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainGameScreen extends StatefulWidget {
  @override
  _MainGameScreenState createState() => _MainGameScreenState();
}

class _MainGameScreenState extends State<MainGameScreen> {
  int _pageIndex = 0;

  final _controller = PageController();

  _MainGameScreenState() {
    _controller.addListener(() {
      if (_controller.page.round() != _pageIndex) {
        setState(() {
          _pageIndex = _controller.page.round();
        });
      }
    });
  }

  void _showPageIndex(int index) {
    setState(() {
      _pageIndex = index;
    });
    _controller.animateToPage(
      index,
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeOutCubic,
    );
  }

  @override
  Widget build(BuildContext context) {
    EventHandler eventHandler = Provider.of<EventHandler>(context);
    PoliceEventHandler policeHandler = Provider.of<PoliceEventHandler>(context);
    GameWorld gameWorld = Provider.of<GameWorld>(context);

//    print("Width = " + MediaQuery.of(context).size.width.toString());
//    print("Height = " + MediaQuery.of(context).size.height.toString());
//    print("Aspect = " + MediaQuery.of(context).size.aspectRatio.toString());

    //При возникновении событий миссий показать диалоговое окно
    //после построения виджета. Обращение к eventHandler в MainGameScreen
    //позволяет сделать диалоговые окна глобальными, т.е. появляющимися
    //поверх любого экрана игры
    Future.delayed(Duration.zero,
        () => _showDialogs(gameWorld, eventHandler, policeHandler, context));

    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Scaffold(
          backgroundColor: mainBackgroundColor,
          appBar: AppBar(
              backgroundColor: mainBackgroundColor,
              automaticallyImplyLeading: false,
              titleSpacing: 0,
              title: GameAppBar()),
          bottomNavigationBar: _buildBottomNavigation(),
          body: PageView(
            controller: _controller,
            children: const [MapScreen(), GangScreen(), ItemsScreen()],
          )),
    );
  }

  Widget _buildBottomNavigation() {
    final navBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          backgroundColor: mainBackgroundColor,
          icon: Icon(mapIcon),
          label: Strings.map),
      BottomNavigationBarItem(
          backgroundColor: mainBackgroundColor,
          icon: Icon(gangIcon),
          label: Strings.gang),
      BottomNavigationBarItem(
          backgroundColor: mainBackgroundColor,
          icon: Icon(itemsIcon),
          label: Strings.items)
    ];

    return BottomNavigationBar(
        items: navBarItems,
        currentIndex: _pageIndex,
        type: BottomNavigationBarType.shifting,
        onTap: (int index) {
          _showPageIndex(index);
        });
  }

  void _showDialogs(GameWorld gameWorld, EventHandler eventHandler,
      PoliceEventHandler policeHandler, BuildContext context) {
    if (gameWorld.gameOver) {
      _showGameOverModal(gameWorld, context);
      return;
    }

    if (eventHandler.completedMission != null ||
        eventHandler.failedMission != null)
      _showMissionCompleteModal(eventHandler, policeHandler, context);
    else if (eventHandler.activeEvent != null)
      _showEventModal(eventHandler, context);
  }

  void _showGameOverModal(GameWorld gameWorld, BuildContext context) {
    showDialog<void>(
      context: context,
      builder: (context) => GameOverModal(gameWorld: gameWorld),
    );
  }

  void _showEventModal(EventHandler eventHandler, BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          if (eventHandler.activeEvent is SimpleMissionEvent)
            return SimpleEventModal(eventHandler);
          else
            return ChoiceEventModal(eventHandler);
        });
  }

  void _showMissionCompleteModal(EventHandler eventHandler,
      PoliceEventHandler policeHandler, BuildContext context) {
    //После окна с сообщением об окончании миссии показать сообщение об аресте
    //персонажей (если были аресты)
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return MissionEndModal(eventHandler);
        }).then((_) => _showPoliceEventModal(policeHandler, context));
  }

  void _showPoliceEventModal(
      PoliceEventHandler policeHandler, BuildContext context) {
    if (policeHandler.event == null) return;

    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return PoliceEventModal(policeHandler);
        });
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => ExitGameModal(),
        ) ??
        false;
  }
}

class GameAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Gang>(
      builder: (context, gang, child) {
        return Container(
          child: Row(
            children: [
              Expanded(
                  child: ListTile(
                leading: ChangeNotifierProvider<GangStat>.value(
                    value: gang.moneyStat,
                    child: StatIcon(
                      flare: "assets/flare/coins.flr",
                    )),
                title: Text(
                  gang.money.toString(),
                  style: appBarStatTextStyle,
                ),
              )),
//              Expanded(
//                  child: ListTile(
//                leading: Icon(gangIcon, color: gangIconColor),
//                title: Text(
//                  gang.members.length.toString(),
//                  style: appBarStatTextStyle,
//                ),
//              )),
              Expanded(
                  child: ListTile(
                leading: Container(
                  width: 24,
                  height: 24,
                  child: Image.asset(wantedIconPath,
                      color: wantedIconColor, fit: BoxFit.contain),
                ),
                title: Text(
                  gang.wantedLevel.toStringAsFixed(1) + " %",
                  style: appBarStatTextStyle,
                ),
              )),
            ],
          ),
        );
      },
    );
  }
}
