import 'package:crime_game/model/characters/gang.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const String _defaultAnimationName = "update";

class StatIcon extends StatelessWidget {
  final FlareControls controls = FlareControls();
  final String flare;
  final double size;

  StatIcon({Key key, this.flare, this.size = 32.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GangStat value = Provider.of<GangStat>(context);

    return Container(
        width: size,
        height: size,
        child: (FlareActor(flare,
            fit: BoxFit.contain,
            alignment: Alignment.topCenter,
            animation: value.isUpdated ? _defaultAnimationName : null,
            callback: (name) {
          if (name == _defaultAnimationName) _onComplete(value);
        }, controller: controls)));
  }

  void _onComplete(GangStat value) {
    if (value.isUpdated) value.resetUpdated();
  }
}
