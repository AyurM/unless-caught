import 'package:crime_game/model/events/reward_event_effect.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class RewardEffectDisplay extends StatelessWidget {
  final double _wantedIconSize = 24.0;
  final RewardEventEffect effect;

  RewardEffectDisplay(this.effect);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (effect.creditsChange != 0)
            _buildRewardTile(Icon(moneyIcon, color: moneyIconColor),
                effect.creditsChange, effect.creditsChange > 0),
          if (effect.xpChange != 0)
            _buildRewardTile(Icon(xpIcon, color: xpIconColor), effect.xpChange,
                effect.xpChange > 0),
          if (effect.wantedChange != 0)
            _buildRewardTile(
                Container(
                  width: _wantedIconSize,
                  height: _wantedIconSize,
                  child: Image.asset(wantedIconPath,
                      color: wantedIconColor, fit: BoxFit.cover),
                ),
                effect.wantedChange,
                effect.wantedChange < 0),
          if (effect.durationChange != 0)
            _buildRewardTile(
                Icon(durationIcon, color: durationIconColor),
                (effect.durationChange * 100).floor(),
                effect.durationChange < 0,
                suffix: "%")
        ],
      ),
    );
  }

  Widget _buildRewardTile(Widget widget, int value, bool isGood,
      {String suffix = ""}) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            widget,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Text(
                (value > 0 ? "+" : "") + value.toString() + suffix,
                style: listItemMissionRewardTextStyle.apply(
                    color: isGood ? goodUITextColor : badUITextColor),
              ),
            )
          ],
        ));
  }
}
