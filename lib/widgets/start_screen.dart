import 'dart:async';
import 'dart:math';

import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class StartScreen extends StatefulWidget {
  final bool savedGameFound;
  final Random random = Random();

  StartScreen({Key key, this.savedGameFound}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  Timer _backgroundTimer;
  int _backgroundImage;

  @override
  void initState() {
    _backgroundImage = widget.random.nextInt(startScreens.length);
    _nextBackground();
    super.initState();
  }

  void _nextBackground() {
    setState(() {
      _backgroundImage++;
    });
    _startTimer();
  }

  void _startTimer() {
    _backgroundTimer?.cancel();
    _backgroundTimer = Timer(const Duration(seconds: 10), _nextBackground);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage(
                    startScreens[_backgroundImage % startScreens.length]),
                fit: BoxFit.cover)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 48),
              child: Container(
                width: double.infinity,
                height: 96,
                child: Stack(
                  children: <Widget>[
                    // Stroked text as border.
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          Strings.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: "DaiAtlas",
                            fontSize: 64,
                            foreground: Paint()
                              ..style = PaintingStyle.stroke
                              ..strokeWidth = 6
                              ..color = Colors.black,
                          ),
                        ),
                      ),
                    ),
                    // Solid text as fill.
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          Strings.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "DaiAtlas",
                            fontSize: 64,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(child: SizedBox()),
            if (widget.savedGameFound)
              StartScreenButton(
                  text: Strings.continueGame,
                  verticalPadding: 28,
                  color: mainButtonColor,
                  onPressed: () => _onContinueGameClick(context)),
            StartScreenButton(
                text: Strings.newGame,
                color: mainButtonColor,
                onPressed: () => _onNewGameClick(context)),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: StartScreenButton(
                      text: Strings.help,
                      color: skipButtonColor,
                      onPressed: () {}),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 8, 16, 8),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          elevation: 4,
                          padding: EdgeInsets.symmetric(
                              horizontal: 24.0, vertical: 18.0),
                          primary: skipButtonColor,
                          onPrimary: skipButtonColor,
                          shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.0)),
                          )),
                      child: Icon(Icons.info_outline, color: buttonTextColor),
                      onPressed: () {},
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _backgroundTimer?.cancel();
  }

  void _onNewGameClick(BuildContext context) {
    Navigator.of(context).pushNamed('/character_select');
  }

  void _onContinueGameClick(BuildContext context) {
    Navigator.of(context).pushNamed('/game_screen');
  }
}

class StartScreenButton extends StatelessWidget {
  final String text;
  final Color color;
  final void Function() onPressed;
  final double verticalPadding;
  StartScreenButton(
      {Key key,
      @required this.text,
      @required this.color,
      @required this.onPressed,
      this.verticalPadding = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 4,
            padding: EdgeInsets.symmetric(
                horizontal: 24.0, vertical: verticalPadding),
            primary: color,
            onPrimary: color,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            )),
        child: Text(text, style: mainButtonTextStyle),
        onPressed: onPressed,
      ),
    );
  }
}
