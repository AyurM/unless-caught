import 'package:crime_game/model/game_world.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CharacterSelectScreen extends StatefulWidget {
  @override
  _CharacterSelectScreenState createState() => _CharacterSelectScreenState();
}

class _CharacterSelectScreenState extends State<CharacterSelectScreen> {
  int _index = 0;

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: mainButtonColor,
    padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
    primary: mainButtonColor,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: RadialGradient(
          center: Alignment.center,
          radius: 1,
          colors: [Color(0xBB000000), Colors.black],
        )),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildChooseCharacter(),
            _buildCharacterSelectArea(),
            _buildSpecName(),
            _buildSpecDescription(),
            Expanded(child: SizedBox()),
            Container(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: ElevatedButton(
                  style: raisedButtonStyle,
                  child: Text(Strings.startGame, style: mainButtonTextStyle),
                  onPressed: _startNewGame,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildChooseCharacter() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 40.0, 8.0, 24.0),
      child: Text(Strings.chooseCharacter,
          textAlign: TextAlign.center, style: appBarStatTextStyle),
    );
  }

  Widget _buildCharacterSelectArea() {
    return Stack(
      children: <Widget>[
        CharacterAvatar(index: _index),
        _index == 0
            ? SizedBox()
            : Positioned(
                top: 130.0,
                child: IconButton(
                    icon: Icon(Icons.chevron_left,
                        color: buttonTextColor, size: 40.0),
                    onPressed: () {
                      setState(() {
                        _index--;
                      });
                    }),
              ),
        _index == 4
            ? SizedBox()
            : Positioned(
                top: 130.0,
                right: 0.0,
                child: IconButton(
                    icon: Icon(Icons.chevron_right,
                        color: buttonTextColor, size: 40.0),
                    onPressed: () {
                      setState(() {
                        _index++;
                      });
                    }),
              ),
      ],
    );
  }

  Widget _buildSpecName() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(specNameList[_index], style: mainButtonTextStyle),
    );
  }

  Widget _buildSpecDescription() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(specDescriptionList[_index],
          textAlign: TextAlign.center, style: listItemMissionNameTextStyle),
    );
  }

  void _startNewGame() {
    Provider.of<GameWorld>(context, listen: false).startNewGame(_index);
    Navigator.of(context).popAndPushNamed('/game_screen');
  }
}

class CharacterAvatar extends StatelessWidget {
  final int index;

  final double _characterAvatarSize = 300.0;

  CharacterAvatar({Key key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: Container(
        width: _characterAvatarSize,
        height: _characterAvatarSize,
        child: Image.asset(specLargeImagePathList[index], fit: BoxFit.cover),
      ),
    );
  }
}
