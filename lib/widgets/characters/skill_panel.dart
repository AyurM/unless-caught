import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

///Панель, показывающая навыки персонажа [character]. Отображаются иконка навыка,
///название навыка, относительная шкала уровня навыка и его численное значение.
class SkillPanel extends StatelessWidget {
  static const double _defaultIconSize = 32.0;
  final Character character;
  final double skillIconSize;

  SkillPanel({@required this.character, this.skillIconSize = _defaultIconSize});

  @override
  Widget build(BuildContext context) {
    List<Widget> skills = [];
    for (final Skill skill in character.skills.keys) {
      skills.add(_buildSkillStat(
          character,
          skill,
          character.availableXp > 0 &&
              !character.isBusy &&
              !character.isArrested &&
              character.skills[skill] < Character.maxSkillValue));
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(Strings.skills,
              style: infoScreenSubHeaderTextStyle.apply(color: Colors.white70)),
        ),
        ...skills
      ],
    );
  }

  Widget _buildSkillStat(Character character, Skill skill, bool canBeUpgraded) {
    return Card(
      elevation: 2,
      color: skillColor[skill],
      margin: const EdgeInsets.symmetric(vertical: 2, horizontal: 6),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Container(
              width: skillIconSize,
              height: skillIconSize,
              child: Image.asset(skillIconPath[skill],
                  color: Colors.white, fit: BoxFit.cover),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(skillNameMap[skill],
                      style: listItemSubHeaderTextStyle.apply(
                          color: Colors.white)),
                ),
                FractionallySizedBox(
                  widthFactor: character.skills[skill] / 100,
                  child: Container(
                    height: 6,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.white),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Text(character.skills[skill].toString(),
                style: listItemMissionRewardTextStyle.apply(
                    color: Utils.getSkillValueColor(character, skill,
                        isColored: false))),
          ),
          //показать кнопку повышения навыка, если есть доступный опыт
          canBeUpgraded
              ? Container(
                  margin: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(4),
                      color: goodUITextColor),
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.plus, color: buttonTextColor),
                    onPressed: () => _onUpgradeSkillClick(character, skill),
                  ))
              : SizedBox()
        ],
      ),
    );
  }

  void _onUpgradeSkillClick(Character character, Skill skill) {
    character.upgradeSkill(skill);
  }
}
