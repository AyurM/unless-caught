import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/items/equip_item_modal.dart';
import 'package:crime_game/widgets/items/items_section.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

///Выдвижная панель инвентаря персонажа
class InventoryPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<Character>(context);
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          gradient: LinearGradient(
              colors: [
                skillColor[mainSkillMap[character.specializations[0]]],
                gradientMiddleColor,
                gradientEndColor
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.3, 1.2, 1.5])),
      child: Stack(children: <Widget>[
        Column(
          children: <Widget>[
            _buildTitle(character, character.items.items.length),
            Divider(height: 1.5),
            ItemsSection(character: character),
          ],
        ),
        _buildFab(context, character)
      ]),
    );
  }

  Widget _buildTitle(Character character, int itemsAmount) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(Strings.items + " ($itemsAmount)",
            textAlign: TextAlign.center,
            style: infoScreenHeaderTextStyle.apply(color: Colors.white70)),
      ),
    );
  }

  Widget _buildFab(BuildContext context, Character character) {
    if (character.isBusy || character.isArrested) return SizedBox();

    return Positioned.fill(
      child: Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: FloatingActionButton(
            backgroundColor: mainButtonColor,
            onPressed: () => _onAddItemClick(context, character),
            child: Icon(FontAwesomeIcons.plus, color: buttonTextColor),
          ),
        ),
      ),
    );
  }

  void _onAddItemClick(BuildContext context, Character character) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return ChangeNotifierProvider<Character>.value(
              value: character, child: EquipItemModal());
        });
  }
}
