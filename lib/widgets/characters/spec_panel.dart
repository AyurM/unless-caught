import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';

///Панель, показывающая специализации персонажа [character]
class SpecPanel extends StatelessWidget {
  final Character character;

  SpecPanel({@required this.character});

  @override
  Widget build(BuildContext context) {
    String text = "";
    character.specializations
        .forEach((spec) => text += specNameMap[spec] + " ");
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child:
      Text(text, style: appBarStatTextStyle.apply(color: Colors.white70)),
    );
  }
}
