import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/wanted_indicator.dart';
import 'package:flutter/material.dart';

class CharacterModalItem extends StatelessWidget {
  static const double _defaultPictureSize = 80.0;
  static const double _defaultSkillIconSize = 24.0;

  final Character character;
  final Color borderColor;
  final double pictureSize;
  final double skillIconSize;
  const CharacterModalItem(
      {@required this.character,
      this.borderColor = Colors.white,
      this.pictureSize = _defaultPictureSize,
      this.skillIconSize = _defaultSkillIconSize});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 4,
        margin: const EdgeInsets.all(4.0),
        shape: RoundedRectangleBorder(
            side: BorderSide(color: borderColor, width: 2.0),
            borderRadius: BorderRadius.circular(4.0)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CharacterAvatarBig(
              character: character,
              pictureSize: pictureSize,
              showFrame: true,
            ),
            Expanded(child: SizedBox()),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child:
                      Text(character.name, style: listItemSubHeaderTextStyle),
                ),
                _buildSkills(character),
              ],
            ),
            Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: WantedIndicator(
                  value: character.wantedLevel, size: 48, fontSize: 14),
            ),
            Expanded(child: SizedBox()),
          ],
        ));
  }

  Widget _buildSkills(Character character) {
    List<Widget> widgets = character.skills.keys.map((skill) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Container(
                width: skillIconSize,
                height: skillIconSize,
                child: Image.asset(skillIconPath[skill],
                    color: skillColor[skill], fit: BoxFit.cover),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(character.skills[skill].toString(),
                  style: smallSkillTextStyle.apply(
                      color: Utils.getSkillValueColor(character, skill))),
            ),
          ],
        ),
      );
    }).toList();
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: widgets,
    );
  }
}
