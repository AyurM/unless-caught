import 'package:flutter/material.dart';
import 'package:crime_game/styles.dart';

///Иконка, отображающая количество предметов в инвентаре и располагающаяся
///поверх портрета персонажа
class InventoryNumber extends StatelessWidget {
  final int number;
  final double iconSize;

  const InventoryNumber({Key key, @required this.number, this.iconSize = 16.0})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          color: Colors.white,
          borderRadius: BorderRadius.circular(4.0)),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: Container(
                width: iconSize,
                height: iconSize,
                child: Image.asset("images/backpack.png",
                    color: mainButtonColor, fit: BoxFit.cover),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(number.toString(),
                style: sectionHeaderTextStyle.apply(
                    fontSizeDelta: iconSize - sectionHeaderTextStyle.fontSize)),
          ),
        ],
      ),
    );
  }
}
