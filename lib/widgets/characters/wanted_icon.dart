import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class WantedIcon extends StatelessWidget {
  static const double _defaultFontSize = 18;
  static const double _defaultIconSize = 40;

  final int value;
  final double iconSize;
  final double fontSize;

  const WantedIcon(
      {Key key,
      @required this.value,
      this.iconSize = _defaultIconSize,
      this.fontSize = _defaultFontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: iconSize,
          height: iconSize,
          child: Image.asset(wantedFilledIconPath,
              color: wantedIconColor, fit: BoxFit.cover),
        ),
        Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: Text(value.toString(),
                  style: appBarStatTextStyle.apply(
                      fontSizeDelta: _getFontSizeDelta()))),
        )
      ],
    );
  }

  double _getFontSizeDelta() {
    return fontSize - _defaultFontSize;
  }
}
