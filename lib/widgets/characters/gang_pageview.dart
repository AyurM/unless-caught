import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/widgets/characters/character_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GangPageView extends StatelessWidget {
  final List<Character> characters;
  final int index;
  const GangPageView(this.characters, this.index);

  @override
  Widget build(BuildContext context) {
    PageController _controller = PageController(initialPage: index, keepPage: false);
    return Scaffold(
      body: Container(
        child: PageView.builder(itemBuilder: (context, position) {
          return ChangeNotifierProvider<Character>.value(
              value: characters[position], child: CharacterScreen());
        },
            itemCount: characters.length,
            controller: _controller),
      ),
    );
  }
}