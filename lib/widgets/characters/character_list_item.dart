import 'dart:math';

import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/character_name_panel.dart';
import 'package:crime_game/widgets/characters/small_skill_panel.dart';
import 'package:crime_game/widgets/characters/wanted_indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

class CharacterListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<Character>(context);
    return Card(
      elevation: 2,
      margin: const EdgeInsets.all(6),
      shape: RoundedRectangleBorder(
          side: BorderSide(
              color: skillColor[mainSkillMap[character.specializations[0]]],
              width: 2.0),
          borderRadius: BorderRadius.circular(6.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CharacterNamePanel(character: character),
          Stack(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CharacterAvatarBig(character: character),
                  SmallSkillPanel(character: character),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: WantedIndicator(value: character.wantedLevel),
                  )
                ],
              ),
              if (character.isBusy) _buildBusySign()
            ],
          ),
          character.isHired ? SizedBox() : Divider(height: 1.5),
          _buildHireCost(character, context)
        ],
      ),
    );
  }

  Widget _buildBusySign() {
    return Positioned(
        top: 16,
        left: 96,
        child: Transform.rotate(
            angle: -pi / 8,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: badUITextColor, width: 2.0)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  Strings.onMission,
                  style: mainButtonTextStyle.apply(color: badUITextColor),
                ),
              ),
            )));
  }

  Widget _buildHireCost(Character character, BuildContext context) {
    if (character.isHired) return SizedBox();

    Gang gang = Provider.of<Gang>(context);

    final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
      onPrimary: mainButtonColor,
      onSurface: Colors.grey,
      elevation: 4,
      primary: mainButtonColor,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
    );

    return ListTile(
      leading: Icon(moneyIcon, color: moneyIconColor),
      title: Text(
        character.hireCost.toString(),
        style: smallButtonTextStyle.apply(color: Colors.black),
      ),
      trailing: ElevatedButton(
        style: raisedButtonStyle,
        onPressed: !character.canBeHired(gang.money)
            ? null
            : () => _onHireButtonClick(character, gang),
        child: Text(Strings.hire, style: smallButtonTextStyle),
      ),
    );
  }

  void _onHireButtonClick(Character character, Gang gang) {
    gang.hireCharacter(character);
  }
}
