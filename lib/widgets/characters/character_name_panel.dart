import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:crime_game/styles.dart';

///Панель с именем персонажа [character]. Цвет фона панели определяется
///специализацией персонажа. Используется в карточках персонажей для
///списков GangScreen и PoliceScreen.
class CharacterNamePanel extends StatelessWidget {
  final Character character;

  CharacterNamePanel({@required this.character});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: skillColor[mainSkillMap[character.specializations[0]]],
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(character.name,
              style: infoScreenSubHeaderTextStyle.apply(color: Colors.white)),
        ),
      ),
    );
  }
}
