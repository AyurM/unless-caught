import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/inventory_number.dart';
import 'package:flutter/material.dart';

///Портрет персонажа [character] с информацией о количестве предметов в
///инвентаре и доступных очках опыта. Используется в карточках персонажей
///для списков GangScreen и PoliceScreen, а также в CharacterInfoScreen и
///ArrestedScreen.
class CharacterAvatarBig extends StatelessWidget {
  static const double _defaultPictureSize = 110.0;

  final Character character;
  final double pictureSize;

  ///Если [showItems] равно true, то поверх портрета выводится иконка с
  ///количеством предметов в инвентаре персонажа
  final bool showItems;

  ///Если [showXp] равно true, то над портретом персонажа выводится иконка с
  ///количеством доступных очков опыта
  final bool showXp;

  ///Если [showFrame] равно true, то портрет персонажа обводится цветом,
  ///соответствующим его специализации
  final bool showFrame;

  CharacterAvatarBig(
      {@required this.character,
      this.pictureSize = _defaultPictureSize,
      this.showItems = true,
      this.showXp = true,
      this.showFrame = false});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              width: pictureSize,
              height: pictureSize,
              decoration: _getDecoration(),
              child: Image.asset(character.smallPicPath, fit: BoxFit.cover),
            ),
          ),
        ),
        if (showXp) _buildAvailableXp(character),
        if (showItems) _buildItemsNumber(character)
      ],
    );
  }

  BoxDecoration _getDecoration() {
    if (!showFrame) return null;
    return BoxDecoration(
        border: Border.all(
            width: 3,
            color: skillColor[mainSkillMap[character.specializations[0]]]));
  }

  Widget _buildAvailableXp(Character character) {
    if (character.availableXp == 0) return SizedBox();

    return Positioned(
      top: 2,
      right: -2,
      child: Container(
        decoration: BoxDecoration(
            color: goodUITextColor, borderRadius: BorderRadius.circular(4.0)),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(" +${character.availableXp.toString()} ",
              style: mainButtonTextStyle.apply(
                  fontSizeDelta: pictureSize > _defaultPictureSize ? 4 : 0)),
        ),
      ),
    );
  }

  Widget _buildItemsNumber(Character character) {
    if (character.items.items.isEmpty) return SizedBox();

    return Positioned(
      bottom: 8,
      left: 8,
      child: InventoryNumber(number: character.items.items.length),
    );
  }
}
