import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/widgets/characters/wanted_icon.dart';
import 'package:flutter/material.dart';

class WantedIndicator extends StatelessWidget {
  static const double _defaultFontSize = 18;

  final int value;
  final double size;
  final double fontSize;

  const WantedIndicator(
      {Key key,
      @required this.value,
      this.size = 70.0,
      this.fontSize = _defaultFontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: size,
          height: size,
          child: CircularProgressIndicator(
              value: value / Character.maxWantedLevel,
              backgroundColor: Colors.black12,
              strokeWidth: size / 7,
              valueColor:
                  AlwaysStoppedAnimation<Color>(_getWantedColor(value))),
        ),
        Positioned.fill(
          child: Align(
              alignment: Alignment.center,
              child: WantedIcon(
                  value: value, iconSize: size * 0.6, fontSize: fontSize)),
        )
      ],
    );
  }

  Color _getWantedColor(int wantedLevel) {
    if (wantedLevel <= 25)
      return Colors.green;
    else if (wantedLevel > 25 && wantedLevel <= 50)
      return Colors.orange;
    else if (wantedLevel > 50 && wantedLevel <= 75)
      return Colors.deepOrange;
    else
      return Colors.red;
  }
}
