import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';

///Вертикальная панель с краткой информацией о навыках персонажа (иконка + значение).
///Используется в карточках персонажей для списков GangScreen и PoliceScreen.
class SmallSkillPanel extends StatelessWidget {
  final Character character;
  final double skillIconSize;

  SmallSkillPanel({@required this.character, this.skillIconSize = 26.0});

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = character.skills.keys
        .map((skill) => _buildSkillStat(skill, character.skills[skill]))
        .toList();

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    );
  }

  Widget _buildSkillStat(Skill skill, int value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              width: skillIconSize,
              height: skillIconSize,
              child: Image.asset(skillIconPath[skill],
                  color: skillColor[skill], fit: BoxFit.contain),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: Text(value.toString(),
                style: infoScreenSkillTextStyle.apply(
                    color: Utils.getSkillValueColor(character, skill))),
          )
        ],
      ),
    );
  }
}
