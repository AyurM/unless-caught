import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:flutter/material.dart';

class CharacterDecoration{
  static BoxDecoration getCharacterDecoration(Character character){
    return BoxDecoration(
        gradient: LinearGradient(
            colors: [
              skillColor[mainSkillMap[character.specializations[0]]],
              gradientMiddleColor,
              gradientEndColor
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1.2, 1.5]));
  }
}