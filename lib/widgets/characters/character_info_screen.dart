import 'dart:math';

import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/character_decoration.dart';
import 'package:crime_game/widgets/characters/mission_counter_panel.dart';
import 'package:crime_game/widgets/characters/skill_panel.dart';
import 'package:crime_game/widgets/characters/spec_panel.dart';
import 'package:crime_game/widgets/characters/wanted_indicator.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

///Экран с подробными сведениями о персонаже
class CharacterInfoScreen extends StatelessWidget {
  final double _pictureSize = 128.0;
  final double _wantedIndicatorSize = 80.0;

  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<Character>(context);

    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: CharacterDecoration.getCharacterDecoration(character),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          ScreenHeader(
              title: character.name,
              color: skillColor[mainSkillMap[character.specializations[0]]]),
          _buildPictureAndWantedLevel(character),
          Divider(height: 1.5),
          SpecPanel(character: character),
          Divider(height: 1.5),
          SkillPanel(character: character),
          Divider(height: 1.5),
          MissionCounterPanel(character: character),
          Expanded(child: SizedBox()),
          _buildHireCost(context, character),
        ],
      ),
    );
  }

  Widget _buildPictureAndWantedLevel(Character character) {
    return Stack(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CharacterAvatarBig(
                character: character,
                pictureSize: _pictureSize,
                showItems: false),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: Text(Strings.wanted,
                      style: infoScreenSubHeaderTextStyle.apply(
                          color: Colors.white70)),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: WantedIndicator(
                      value: character.wantedLevel,
                      size: _wantedIndicatorSize,
                      fontSize: 20),
                ),
              ],
            )
          ],
        ),
        if (character.isBusy) _buildBusySign()
      ],
    );
  }

  Widget _buildBusySign() {
    return Positioned(
        top: _pictureSize / 3,
        left: 96,
        child: Transform.rotate(
            angle: -pi / 8,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: badUITextColor, width: 2.0)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  Strings.onMission,
                  style: mainButtonTextStyle.apply(color: badUITextColor),
                ),
              ),
            )));
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: mainButtonColor,
    onSurface: Colors.grey,
    elevation: 4,
    primary: mainButtonColor,
    padding: const EdgeInsets.all(16),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
  );

  Widget _buildHireCost(BuildContext context, Character character) {
    if (character.isHired) return SizedBox();

    Gang gang = Provider.of<Gang>(context);
    return Padding(
        padding: const EdgeInsets.all(16),
        child: ListTile(
          leading: Icon(moneyIcon, color: moneyIconColor, size: 28),
          title: Text(
            character.hireCost.toString(),
            style: mainButtonTextStyle.apply(color: Colors.white),
          ),
          trailing: ElevatedButton(
            style: raisedButtonStyle,
            onPressed: !character.canBeHired(gang.money)
                ? null
                : () => _onHireButtonClick(character, gang),
            child: Text(Strings.hire + " (${gang.money})",
                style: smallButtonTextStyle),
          ),
        ));
  }

  void _onHireButtonClick(Character character, Gang gang) {
    gang.hireCharacter(character);
  }
}
