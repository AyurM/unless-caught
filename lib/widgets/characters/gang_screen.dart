import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/characters/character_list_item.dart';
import 'package:crime_game/widgets/characters/gang_pageview.dart';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class GangScreen extends StatefulWidget {
  const GangScreen();

  @override
  _GangScreenState createState() => _GangScreenState();
}

class _GangScreenState extends State<GangScreen> {
  bool _isGangListExpanded = true;
  bool _isHireListExpanded = true;

  @override
  Widget build(BuildContext context) {
    Gang gang = Provider.of<Gang>(context);
    List<Widget> slivers = [];
    _buildGangSection(slivers, true, Strings.leader, [gang.mainCharacter]);
    _buildGangSection(slivers, _isGangListExpanded, Strings.members,
        gang.members, gang.members.length);
    _buildGangSection(slivers, _isHireListExpanded, Strings.forHire,
        gang.forHire, gang.forHire.length);

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                mainBackgroundColor,
                gradientMiddleColor,
                gradientEndColor
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.3, 1.2, 1.5])),
      child: CustomScrollView(slivers: slivers),
    );
  }

  void _buildGangSection(List<Widget> slivers, bool isExpanded, String title,
      List<Character> characters,
      [int count]) {
    if (characters.isEmpty) return;

    slivers.add(SliverPersistentHeader(
      pinned: false,
      delegate: GangSectionHeader(
          title, isExpanded, count, _getHeaderOnClickCallback(title)),
    ));
    if (isExpanded) {
      slivers.add(SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
        Character character = characters[index];
        List<Character> pageViewCharacters = characters;

        //При прокрутке членов банды включать лидера
        if (title == Strings.leader || title == Strings.members) {
          Gang gang = Provider.of<Gang>(context);
          pageViewCharacters = [gang.mainCharacter, ...gang.members];
        }

        return ChangeNotifierProvider<Character>.value(
          value: character,
          child: GestureDetector(
              //При просмотре членов банды увеличить index на 1, т.к.
              //в начало списка был добавлен лидер
              onTap: () => _onCharacterItemClick(pageViewCharacters,
                  title == Strings.members ? index + 1 : index),
              child: CharacterListItem()),
        );
      }, childCount: characters.length)));
    }
  }

  VoidCallback _getHeaderOnClickCallback(String title) {
    return () => setState(() {
          if (title == Strings.members)
            _isGangListExpanded = !_isGangListExpanded;
          else if (title == Strings.forHire)
            _isHireListExpanded = !_isHireListExpanded;
        });
  }

  void _onCharacterItemClick(List<Character> characters, int index) {
    //TODO: разобраться с route'ами
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => GangPageView(characters, index)));
  }
}

class GangSectionHeader extends SliverPersistentHeaderDelegate {
  final String title;
  final bool isExpanded;
  final int count;
  final VoidCallback onClick;
  const GangSectionHeader(this.title, this.isExpanded,
      [this.count, this.onClick]);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    String countString = count == null ? "" : " ($count)";
    return Card(
      elevation: 2,
      margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              title + countString,
              style: sectionHeaderBoldTextStyle,
            ),
            count == null
                ? SizedBox()
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.white),
                    child: FaIcon(
                      isExpanded
                          ? FontAwesomeIcons.chevronUp
                          : FontAwesomeIcons.chevronDown,
                      color: Colors.black87,
                    ),
                    onPressed: onClick,
                  )
          ],
        ),
      ),
    );
  }

  @override
  double get maxExtent => 60;

  @override
  double get minExtent => 60;

  @override
  bool shouldRebuild(GangSectionHeader oldDelegate) {
    return title != oldDelegate.title ||
        isExpanded != oldDelegate.isExpanded ||
        count != oldDelegate.count;
  }
}
