import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class MissionCounterPanel extends StatelessWidget {
  final Character character;

  MissionCounterPanel({@required this.character});

  @override
  Widget build(BuildContext context) {
    if (!character.hasFinishedMissions()) return SizedBox();

    List<Widget> missions = [];
    for (int difficulty = 0; difficulty < 3; difficulty++) {
      missions.add(_buildMissionsFinishedLine(character, difficulty));
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(Strings.missionsFinished,
                style: listItemSubHeaderTextStyle.apply(color: Colors.white70)),
          ),
          ...missions
        ],
      ),
    );
  }

  Widget _buildMissionsFinishedLine(Character character, int difficulty) {
    String diff;
    if (difficulty == 0)
      diff = Strings.easy;
    else if (difficulty == 1)
      diff = Strings.medium;
    else
      diff = Strings.hard;

    return Text(
        diff +
            ": " +
            character.completedMissions[difficulty].toString() +
            " / " +
            character.failedMissions[difficulty].toString(),
        style: smallMainTextStyle.apply(color: Colors.white70));
  }
}
