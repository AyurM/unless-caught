import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/characters/character_modal_item.dart';
import 'package:crime_game/widgets/missions/mission_requirements_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddParticipantsModal extends StatelessWidget {
  static const double _maxModalWidth = 400.0;
  final double _iconSize = 32.0;

  @override
  Widget build(BuildContext context) {
    Mission mission = Provider.of<Mission>(context);
    Gang gang = Provider.of<Gang>(context);
    List<Character> characters = [gang.mainCharacter, ...gang.members];
    characters.removeWhere((char) => char.isBusy);

    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: _maxModalWidth),
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: ConstrainedBox(
              constraints: const BoxConstraints(minWidth: 280),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    MissionRequirementsPanel(mission, _iconSize),
                    Divider(height: 1.5),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(Strings.addParticipant,
                          style: sectionHeaderTextStyle.apply(
                              color: Colors.black87)),
                    ),
                    _buildGangList(context, mission, characters),
                    Expanded(child: SizedBox()),
                    _buildDoneButton(context)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildGangList(
      BuildContext context, Mission mission, List<Character> characters) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.55,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Scrollbar(
          child: ListView.builder(
              itemCount: characters.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                    onTap: () => _onItemClick(mission, characters[index]),
                    child: CharacterModalItem(
                        character: characters[index],
                        borderColor:
                            mission.characters.contains(characters[index])
                                ? goodUITextColor
                                : Colors.white));
              }),
        ),
      ),
    );
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: mainButtonColor,
    elevation: 4,
    primary: mainButtonColor,
    padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
  );

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: raisedButtonStyle,
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () => Navigator.pop(context),
        ),
      ),
    );
  }

  void _onItemClick(Mission mission, Character character) {
    if (mission.characters.contains(character))
      mission.removeCharacter(character);
    else
      mission.addCharacter(character);
  }
}
