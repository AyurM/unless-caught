import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/widgets/characters/character_info_screen.dart';
import 'package:crime_game/widgets/characters/inventory_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

///Экран персонажа, состоящий из основного экрана со статистикой и дополнительного
///экрана инвентаря
class CharacterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<Character>(context);
    if (character.isHired)
      return Scaffold(
          body: SlidingUpPanel(
              minHeight: 48,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
              panel: ChangeNotifierProvider<Character>.value(
                value: character,
                child: InventoryPanel(),
              ),
              body: ChangeNotifierProvider<Character>.value(
                value: character,
                child: CharacterInfoScreen(),
              )));
    else
      return Scaffold(
          body: ChangeNotifierProvider<Character>.value(
        value: character,
        child: CharacterInfoScreen(),
      ));
  }
}
