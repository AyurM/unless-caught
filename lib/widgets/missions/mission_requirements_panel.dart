import 'dart:math';

import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:flutter/material.dart';

class MissionRequirementsPanel extends StatelessWidget {
  final Mission mission;
  final double iconSize;
  final bool showTitle;
  final bool showCurrentValues;
  final bool showCharactersRequired;

  const MissionRequirementsPanel(this.mission, this.iconSize,
      {this.showTitle = true,
      this.showCurrentValues = true,
      this.showCharactersRequired = true});

  @override
  Widget build(BuildContext context) {
    double size = min(
        MediaQuery.of(context).size.width /
            (mission.requirements.keys.length + 5),
        iconSize);

    List<Widget> widgets = mission.requirements.keys.map((skill) {
      //показать текущее значение данного навыка, которое будет приниматься
      //в расчет при выполнении миссии
      int currentSkillValue = mission.getMaxSkillValue(skill);
      String stringToAdd =
          showCurrentValues ? currentSkillValue.toString() + "/" : "";
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Container(
              width: size,
              height: size,
              child: Image.asset(skillIconPath[skill],
                  color: skillColor[skill], fit: BoxFit.cover),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(stringToAdd + mission.requirements[skill].toString(),
                style: infoScreenSkillTextStyle.apply(
                    color: !showCurrentValues
                        ? skillColor[skill]
                        : currentSkillValue == 0
                            ? badUITextColor
                            : skillColor[skill],
                    fontSizeDelta:
                        mission.requirements.keys.length > 3 ? -2 : 0)),
          )
        ],
      );
    }).toList();

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        if (showTitle)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(Strings.requirements, style: sectionHeaderTextStyle),
          ),
        Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              if (showCharactersRequired)
                _buildCharacterRequirement(mission, context),
              ...widgets
            ])
      ],
    );
  }

  Widget _buildCharacterRequirement(Mission mission, BuildContext context) {
    int charsNow = mission.characters.length;
    String stringToAdd = charsNow.toString() + "/";

    double size = min(
        MediaQuery.of(context).size.width /
            (mission.requirements.keys.length + 5),
        iconSize);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Icon(gangIcon, size: size, color: gangIconColor),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Text(stringToAdd + mission.requiredCharacters.toString(),
              style: infoScreenSkillTextStyle.apply(
                  color: charsNow >= mission.requiredCharacters
                      ? gangIconColor
                      : badUITextColor,
                  fontSizeDelta:
                      mission.requirements.keys.length > 3 ? -2 : 0)),
        )
      ],
    );
  }
}
