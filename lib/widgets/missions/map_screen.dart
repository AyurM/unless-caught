import 'package:crime_game/model/items/item_pool.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/police/bribe.dart';
import 'package:crime_game/model/police/dirty_police.dart';
import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/widgets/items/shop_marker.dart';
import 'package:crime_game/widgets/missions/mission_marker.dart';
import 'package:crime_game/widgets/police/bribe_marker.dart';
import 'package:crime_game/widgets/police/police_marker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MapScreen extends StatefulWidget {
  const MapScreen();

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  @override
  Widget build(BuildContext context) {
    MissionPool pool = Provider.of<MissionPool>(context);
    ItemPool itemPool = Provider.of<ItemPool>(context);
    Police police = Provider.of<Police>(context);
    DirtyPolice dirtyPolice = Provider.of<DirtyPolice>(context);

    List<Mission> missions = [...pool.available, ...pool.inProgress];
    List<Widget> markers = missions.map((mission) {
      return ChangeNotifierProvider<Mission>.value(
        value: mission,
        child: MissionMarker(mission.mapPosition),
      );
    }).toList();

    List<Widget> shops = itemPool.shops.map((s) {
      return ChangeNotifierProvider<ItemShop>.value(
        value: s,
        child: ShopMarker(),
      );
    }).toList();

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Stack(
        children: <Widget>[
          AspectRatio(
              aspectRatio: 495 / 436,
              child: Container(
                height: double.infinity,
                child: Image.asset("images/city_map.png", fit: BoxFit.cover),
              )),
          ...markers,
          ...shops,
          _buildPoliceMarker(police),
          _buildBribeMarker(dirtyPolice)
        ],
      ),
    );
  }

  Widget _buildPoliceMarker(Police police) {
    if (!police.isActive) return SizedBox();

    return ChangeNotifierProvider<Police>.value(
      value: police,
      child: PoliceMarker(),
    );
  }

  Widget _buildBribeMarker(DirtyPolice dirtyPolice) {
    if (dirtyPolice.bribe == null) return SizedBox();

    return ChangeNotifierProvider<Bribe>.value(
      value: dirtyPolice.bribe,
      child: BribeMarker(),
    );
  }
}
