import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/events/event_handler.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/add_participants_modal.dart';
import 'package:crime_game/widgets/characters/gang_pageview.dart';
import 'package:crime_game/widgets/missions/mission_end_sign.dart';
import 'package:crime_game/widgets/missions/mission_progress_bar.dart';
import 'package:crime_game/widgets/missions/mission_requirements_panel.dart';
import 'package:crime_game/widgets/missions/mission_rewards_panel.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MissionScreen extends StatelessWidget {
  final double _missionPictureAspect = 2.3;
  final double _skillIconSize = 48.0;
  final double _gangListHeight = 98.0;

  @override
  Widget build(BuildContext context) {
    Mission mission = Provider.of<Mission>(context);

    EventHandler eventHandler = Provider.of<EventHandler>(context);

    bool isMissionFinished = eventHandler.failedMission == mission ||
        eventHandler.completedMission == mission;

    Future.delayed(
        Duration.zero,
        isMissionFinished
            ? () {
                //После окончания миссии появится диалоговое окно с сообщением о
                //завершении миссии. Закрыть его, затем закрыть окно миссии.
                //Диалоговое окно откроется заново
                Navigator.pop(context);
                Navigator.pop(context);
              }
            : () {});

    if (isMissionFinished)
      return SizedBox();
    else
      //WillPopScope для очистки назначенных персонажей после нажатия
      //кнопки Назад, если миссия не была запущена
      return WillPopScope(
        onWillPop: () => _onBackPressed(mission),
        child: Scaffold(
          body: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              ScreenHeader(
                  title: mission.name,
                  color: difficultyColor[mission.difficulty],
                  onClose: () => _onCloseButtonClick(mission)),
              MissionProgressBar(mission: mission),
              Stack(
                children: <Widget>[
                  _buildPicture(mission),
                  _buildRewards(mission)
                ],
              ),
              _buildRequirements(mission),
              Divider(height: 1.5),
              _buildGang(mission, context),
              Expanded(child: SizedBox()),
              _buildStartButton(mission),
            ],
          ),
        ),
      );
  }

  Widget _buildPicture(Mission mission) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 28.0),
      child: AspectRatio(
        aspectRatio: _missionPictureAspect,
        child: Container(
          width: double.infinity,
          child: Image.asset(mission.imagePath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _buildRewards(Mission mission) {
    return Positioned.fill(
      child: Align(
          alignment: Alignment.bottomCenter,
          child: MissionRewardsPanel(reward: mission.reward)),
    );
  }

  Widget _buildRequirements(Mission mission) {
    if (mission.status == MissionStatus.completed) return SizedBox();

    return MissionRequirementsPanel(mission, _skillIconSize);
  }

  Widget _buildGang(Mission mission, BuildContext context) {
    if (mission.status == MissionStatus.completed)
      return MissionEndSign(isCompleted: true);
    else if (mission.status == MissionStatus.failed)
      return MissionEndSign(isCompleted: false);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(Strings.participants,
                    textAlign: TextAlign.center, style: sectionHeaderTextStyle),
              )),
            ]),
        _buildGangList(context, mission)
      ],
    );
  }

  Widget _buildAddCharacterButton(BuildContext context, Mission mission) {
    //Если миссия уже запущена, скрыть кнопку добавления участников
    if (mission.status == MissionStatus.inProgress ||
        mission.status == MissionStatus.paused)
      return SizedBox();
    else
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: FloatingActionButton(
          onPressed: () => _onAddParticipantClick(mission, context),
          child: Icon(FontAwesomeIcons.plus, color: buttonTextColor),
          backgroundColor: mainButtonColor,
        ),
      );
  }

  Widget _buildGangList(BuildContext context, Mission mission) {
    List<Widget> gangSlivers = [];

    gangSlivers.add(SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return CharacterMissionItem(mission, mission.characters[index]);
    }, childCount: mission.characters.length)));

    gangSlivers.add(SliverList(
      delegate:
          SliverChildListDelegate([_buildAddCharacterButton(context, mission)]),
    ));

    return Container(
      height: _gangListHeight,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomScrollView(
            scrollDirection: Axis.horizontal, slivers: gangSlivers),
      ),
    );
  }

  Widget _buildStartButton(Mission mission) {
    if (mission.status == MissionStatus.completed) return SizedBox();

    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              elevation: 4,
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child:
              Text(_getStartButtonTitle(mission), style: mainButtonTextStyle),
          onPressed: mission.isReadyToStart
              ? () => _onStartButtonClick(mission)
              : null,
        ),
      ),
    );
  }

  String _getStartButtonTitle(Mission mission) {
    if (mission.status == MissionStatus.available)
      return Strings.startMission;
    else if (mission.status == MissionStatus.inProgress)
      return Strings.pauseMission;
    else
      return Strings.resumeMission;
  }

  void _onStartButtonClick(Mission mission) {
    if (mission.status == MissionStatus.available ||
        mission.status == MissionStatus.paused)
      mission.start();
    else if (mission.status == MissionStatus.inProgress) mission.pause();
  }

  void _onCloseButtonClick(Mission mission) {
    if (mission.status == MissionStatus.available) mission.clearCharacters();
  }

  void _onAddParticipantClick(Mission mission, BuildContext context) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return ChangeNotifierProvider<Mission>.value(
              value: mission, child: AddParticipantsModal());
        });
  }

  Future<bool> _onBackPressed(Mission mission) {
    _onCloseButtonClick(mission);
    return Future<bool>.value(true);
  }
}

class CharacterMissionItem extends StatelessWidget {
  final double _characterPictureSize = 68.0;

  final Character character;
  final Mission mission;
  const CharacterMissionItem(this.mission, this.character);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onCharacterItemClick(
          context, mission.characters, mission.characters.indexOf(character)),
      child: Card(
        elevation: 2,
        margin: const EdgeInsets.symmetric(vertical: 3, horizontal: 4),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: skillColor[mainSkillMap[character.specializations[0]]],
                width: 2.0),
            borderRadius: BorderRadius.circular(4.0)),
        child: Stack(children: <Widget>[
          _buildCharacterPicture(),
          if (character.availableXp > 0) _buildUpgradeSign()
        ]),
      ),
    );
  }

  Widget _buildCharacterPicture() {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          width: _characterPictureSize,
          height: _characterPictureSize,
          child: Image.asset(character.smallPicPath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _buildUpgradeSign() {
    return Positioned(
      top: 4,
      right: 4,
      child: Container(
        width: 16,
        height: 16,
        decoration:
            BoxDecoration(color: goodUITextColor, shape: BoxShape.circle),
      ),
    );
  }

  void _onCharacterItemClick(
      BuildContext context, List<Character> characters, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => GangPageView(characters, index)));
  }
}
