import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/map_position.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/missions/mission_list_item.dart';
import 'package:crime_game/widgets/missions/mission_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

const Map<String, String> _missionIconsMap = {
  Strings.drivingLockpickMission: "images/missions/icon_car.png",
  Strings.drivingHackingMission: "images/missions/icon_atm.png",
  Strings.drivingGunsMission: "images/missions/icon_truck.png",
  Strings.drivingSpeechMission: "images/missions/icon_car.png",
  Strings.lockpickHackingMission: "images/missions/icon_safe.png",
  Strings.lockpickGunsMission: "images/missions/icon_pawnshop.png",
  Strings.lockpickSpeechMission: "images/missions/icon_shop.png",
  Strings.gunsHackingMission: "images/missions/icon_shop.png",
  Strings.gunsSpeechMission: "images/missions/icon_robbery.png",
  Strings.hackingSpeechMission: "images/missions/icon_bank_account.png"
};

class MissionMarker extends StatelessWidget {
  final MapPosition position;

  final double _markerSize = 64.0;
  final double _iconSize = 30.0;

  MissionMarker(this.position);

  @override
  Widget build(BuildContext context) {
    Mission mission = Provider.of<Mission>(context);
    if (mission.status == MissionStatus.inProgress ||
        mission.status == MissionStatus.paused)
      return _buildInProgressMarker(context, mission);
    else
      return _buildAvailableMarker(context, mission);
  }

  Widget _buildInProgressMarker(BuildContext context, Mission mission) {
    return Positioned(
      top: position.top,
      left: position.left,
      child: GestureDetector(
          onTap: () => _onMarkerTap(context, mission),
          child: Stack(children: <Widget>[
            Icon(FontAwesomeIcons.mapMarker,
                color: mission.baseReward.wantedLevel < 0
                    ? policeColor
                    : difficultyColor[mission.difficulty],
                size: _markerSize),
            _buildProgress(mission)
          ])),
    );
  }

  Widget _buildAvailableMarker(BuildContext context, Mission mission) {
    if (mission.baseReward.wantedLevel < 0)
      return _buildDirtyCopsMissionMarker(context, mission);

    return Positioned(
      top: position.top,
      left: position.left,
      child: GestureDetector(
        onTap: () => _onMarkerTap(context, mission),
        child: Stack(children: <Widget>[
          Icon(FontAwesomeIcons.mapMarker,
              color: difficultyColor[mission.difficulty], size: _markerSize),
          Positioned(
            top: 12,
            left: 17,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: Container(
                width: _iconSize,
                height: _iconSize,
                child: Image.asset(_missionIconsMap[mission.name],
                    color: Colors.white, fit: BoxFit.cover),
              ),
            ),
          )
        ]),
      ),
    );
  }

  Widget _buildDirtyCopsMissionMarker(BuildContext context, Mission mission) {
    return Positioned(
      top: position.top,
      left: position.left,
      child: GestureDetector(
          onTap: () => _onMarkerTap(context, mission),
          child: Stack(children: <Widget>[
            Icon(FontAwesomeIcons.mapMarker,
                color: policeColor, size: _markerSize),
            Positioned(
              top: 6,
              left: 11,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  width: _iconSize,
                  height: _iconSize,
                  child: Image.asset(dirtyCopsMissionMarkerPath,
                      fit: BoxFit.cover),
                ),
              ),
            )
          ])),
    );
  }

  Widget _buildProgress(Mission mission) {
    return Positioned(
      top: 8,
      left: 13.5,
      child: CircularProgressIndicator(
          strokeWidth: 8,
          value: mission.getMissionProgress(),
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
    );
  }

  void _onMarkerTap(BuildContext context, Mission mission) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => _onMissionItemTap(context, mission),
            child: ChangeNotifierProvider<Mission>.value(
              value: mission,
              child: MissionListItem(),
            ),
          );
        });
  }

  void _onMissionItemTap(BuildContext context, Mission mission) {
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider.value(
                value: mission, child: MissionScreen())));
  }
}
