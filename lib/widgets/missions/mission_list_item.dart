import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/widgets/missions/mission_end_sign.dart';
import 'package:crime_game/widgets/missions/mission_progress_bar.dart';
import 'package:crime_game/widgets/missions/mission_requirements_panel.dart';
import 'package:crime_game/widgets/missions/mission_rewards_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

class MissionListItem extends StatelessWidget {
  final double _skillIconSize = 40.0;
  final double _missionPictureAspect = 2.3;

  @override
  Widget build(BuildContext context) {
    Mission mission = Provider.of<Mission>(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _buildName(mission),
        MissionProgressBar(mission: mission),
        Stack(children: <Widget>[
          _buildPicture(mission),
          _buildRewards(mission),
        ]),
        _buildRequirements(mission)
      ],
    );
  }

  Widget _buildName(Mission mission) {
    return Container(
      width: double.infinity,
      color: difficultyColor[mission.difficulty],
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(mission.name,
            textAlign: TextAlign.center, style: listItemMissionNameTextStyle),
      ),
    );
  }

  Widget _buildPicture(Mission mission) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 28.0),
      child: AspectRatio(
        aspectRatio: _missionPictureAspect,
        child: Container(
          width: double.infinity,
          child: Image.asset(mission.imagePath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _buildRewards(Mission mission) {
    return Positioned.fill(
      child: Align(
          alignment: Alignment.bottomCenter,
          child: MissionRewardsPanel(reward: mission.reward)),
    );
  }

  Widget _buildRequirements(Mission mission) {
    if (mission.status == MissionStatus.completed)
      return MissionEndSign(isCompleted: true);
    else if (mission.status == MissionStatus.failed)
      return MissionEndSign(isCompleted: false);

    return MissionRequirementsPanel(mission, _skillIconSize,
        showTitle: false,
        showCurrentValues: false,
        showCharactersRequired: false);
  }
}
