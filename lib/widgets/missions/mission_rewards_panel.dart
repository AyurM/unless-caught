import 'package:crime_game/model/missions/mission_reward.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class MissionRewardsPanel extends StatelessWidget {
  final double _wantedIconSize = 24.0;
  final MissionReward reward;

  const MissionRewardsPanel({Key key, this.reward}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      color: Colors.white,
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          RewardItem(
              text: reward.credits.toString(),
              widget: Icon(moneyIcon, color: moneyIconColor)),
          RewardItem(
              text: reward.xp.toString(),
              widget: Icon(xpIcon, color: xpIconColor)),
          Container(
            width: 1,
            height: 32,
            color: Colors.black38,
          ),
          RewardItem(
              text: reward.wantedLevel.toString(),
              widget: Container(
                width: _wantedIconSize,
                height: _wantedIconSize,
                child: Image.asset(wantedIconPath,
                    color: wantedIconColor, fit: BoxFit.cover),
              )),
        ],
      ),
    );
  }
}

class RewardItem extends StatelessWidget {
  final String text;
  final Widget widget;

  const RewardItem({Key key, this.text, this.widget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: widget,
          ),
          Text(
            text,
            style: listItemMissionRewardTextStyle,
          )
        ],
      ),
    );
  }
}
