import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class MissionEndSign extends StatelessWidget {
  final bool isCompleted;

  const MissionEndSign({Key key, this.isCompleted}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
          isCompleted ? Strings.missionCompleted : Strings.missionFailed,
          textAlign: TextAlign.center,
          style: missionEndMessageTextStyle.apply(
              color: isCompleted ? goodUITextColor : badUITextColor)),
    );
  }
}
