import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_status.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class MissionProgressBar extends StatelessWidget {
  final Mission mission;

  const MissionProgressBar({Key key, this.mission}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (mission.status == MissionStatus.completed ||
        mission.status == MissionStatus.available) return SizedBox();

    return Container(
      color: difficultyColor[mission.difficulty],
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FractionallySizedBox(
            widthFactor: mission.getMissionProgress(),
            child: Container(
              height: 6,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3), color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
