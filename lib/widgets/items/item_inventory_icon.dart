import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/items/item_info_screen.dart';
import 'package:flutter/material.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

///Кликабельная иконка предмета в инвентаре персонажа [character]
class ItemInventoryIcon extends StatelessWidget {
  final double size;
  final Character character;

  const ItemInventoryIcon({Key key, this.character, this.size = 75.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Item item = Provider.of<Item>(context);

    //при нажатии на иконку показать меню с возможными действиями
    return PopupMenuButton<String>(
      onSelected: (value) {
        if (character.isBusy) return;
        if (value == 'unequip')
          _onUnequipClick(context, item, character);
        else if (value == 'info') _onItemInfoClick(context, item);
      },
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
                border:
                    Border.all(width: 3, color: Colors.black26)),
            child: Image.asset(item.imagePath, fit: BoxFit.contain),
          ),
        ),
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'unequip',
          child: ListTile(
              dense: true,
              leading: Icon(FontAwesomeIcons.times,
                  color:
                      skillColor[mainSkillMap[character.specializations[0]]]),
              title: Text(Strings.remove, style: listItemSubHeaderTextStyle)),
        ),
        PopupMenuDivider(height: 1.5),
        PopupMenuItem<String>(
          value: 'info',
          child: ListTile(
              dense: true,
              leading: Icon(FontAwesomeIcons.info,
                  color:
                      skillColor[mainSkillMap[character.specializations[0]]]),
              title: Text(Strings.info, style: listItemSubHeaderTextStyle)),
        )
      ],
    );
  }

  void _onUnequipClick(
      BuildContext context, Equipment item, Character character) {
    character.unequip(item);
    ItemStore store = Provider.of<ItemStore>(context);
    store.addItem(item);
  }

  void _onItemInfoClick(BuildContext context, Equipment item) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider<Item>.value(
                value: item, child: ItemInfoScreen(owner: character))));
  }
}

class EmptyInventoryIcon extends StatelessWidget {
  final ItemSlot slot;
  final double size;

  const EmptyInventoryIcon({Key key, @required this.slot, this.size = 75.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
              color: Colors.black12,
              border: Border.all(width: 3, color: Colors.black26),
              borderRadius: BorderRadius.circular(8.0)),
          child: Padding(
            padding: EdgeInsets.all(size / 5),
            child: Image.asset(itemSlotIconPath[slot],
                color: Colors.black38, fit: BoxFit.contain),
          ),
        ),
      ),
    );
  }
}
