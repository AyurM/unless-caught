import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/items/item_list_item.dart';
import 'package:crime_game/widgets/items/items_pageview.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class ItemsScreen extends StatelessWidget {
  final double _emptyIconSize = 128;
  const ItemsScreen();

  @override
  Widget build(BuildContext context) {
    ItemStore store = Provider.of<ItemStore>(context);
    List<Item> items = store.items.items;
    if (items.isEmpty)
      return _buildEmptyUI();
    else {
      items.sort((a, b) => a.name.compareTo(b.name));
      return Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    mainBackgroundColor,
                    gradientMiddleColor,
                    gradientEndColor
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.3, 1.2, 1.5])),
          child: ListView.builder(
              itemCount: items.length,
              itemBuilder: (BuildContext context, int index) {
                return ChangeNotifierProvider.value(
                    value: items[index],
                    child: GestureDetector(
                        onTap: () => _onItemTap(context, items.toList(), index),
                        child: ItemListItem(false)));
              }));
    }
  }

  Widget _buildEmptyUI() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  mainBackgroundColor,
                  gradientMiddleColor,
                  gradientEndColor
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.3, 1.2, 1.5])),
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: _emptyIconSize,
              height: _emptyIconSize,
              child: Image.asset("images/treasure.png", fit: BoxFit.cover),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(Strings.emptyItemsScreen,
                  textAlign: TextAlign.center,
                  style: sectionHeaderTextStyle.apply(color: Colors.white70)),
            ),
          ],
        )));
  }

  void _onItemTap(BuildContext context, List<Item> items, int index) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ItemsPageView(items, index)));
  }
}
