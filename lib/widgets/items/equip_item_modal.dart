import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/items/item_stats_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

///Диалоговое окно выбора предметов
class EquipItemModal extends StatelessWidget {
  static const double _maxModalWidth = 400.0;

  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<Character>(context);
    ItemStore store = Provider.of<ItemStore>(context);
    List<Equipment> equipment = [];
    store.items.items.forEach((i) {
      if (i is Equipment) equipment.add(i);
    });

    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: _maxModalWidth),
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: ConstrainedBox(
              constraints: const BoxConstraints(minWidth: 280),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          skillColor[
                              mainSkillMap[character.specializations[0]]],
                          gradientMiddleColor,
                          gradientEndColor
                        ],
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        stops: [0.6, 1.2, 1.5]),
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    if (equipment.isEmpty)
                      ..._buildEmptyUI()
                    else
                      ..._buildChooseItemUI(
                          context, store, character, equipment),
                    _buildDoneButton(context)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildEmptyUI() {
    List<Widget> result = [];
    result.add(Expanded(child: SizedBox()));
    result.add(Padding(
        padding: const EdgeInsets.all(16),
        child: Text(Strings.noItemsToWear,
            textAlign: TextAlign.center,
            style: sectionHeaderTextStyle.apply(color: Colors.white70))));
    result.add(Expanded(child: SizedBox()));
    return result;
  }

  List<Widget> _buildChooseItemUI(BuildContext context, ItemStore store,
      Character character, List<Equipment> items) {
    List<Widget> result = [];
    result.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(Strings.chooseItem,
          style: sectionHeaderTextStyle.apply(color: Colors.white70)),
    ));
    result.add(Divider(height: 1.5));
    result.add(_buildItemsList(context, store, character, items));
    result.add(Expanded(child: SizedBox()));
    return result;
  }

  Widget _buildItemsList(BuildContext context, ItemStore store,
      Character character, List<Equipment> items) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.66,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Scrollbar(
          child: ListView.builder(
              itemCount: items.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                    onTap: () => _onItemClick(store, items[index], character),
                    child: ItemModalItem(items[index], character));
              }),
        ),
      ),
    );
  }

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              elevation: 4,
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () => Navigator.pop(context),
        ),
      ),
    );
  }

  void _onItemClick(ItemStore store, Equipment item, Character character) {
    if (!item.canBeEquippedBy(character) || !character.hasFreeSlot(item))
      return;

    character.equip(item);
    store.removeItem(item);
  }
}

class ItemModalItem extends StatelessWidget {
  final Equipment item;
  final Character character;
  final double _pictureSize = 80.0;
  const ItemModalItem(this.item, this.character);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2,
        margin: const EdgeInsets.symmetric(vertical: 3, horizontal: 4),
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.white, width: 1.0),
            borderRadius: BorderRadius.circular(8.0)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  width: _pictureSize,
                  height: _pictureSize,
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: 3, color: Utils.getItemColor(item))),
                  child: Image.asset(item.imagePath, fit: BoxFit.cover),
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _buildErrorText(),
                  ItemStatsPanel(item: item),
                ],
              ),
            )
          ],
        ));
  }

  Widget _buildErrorText() {
    String text;
    if (!item.canBeEquippedBy(character))
      text = Strings.wrongSpec;
    else if (!character.hasFreeSlot(item))
      text = Strings.noSlot;
    else
      return SizedBox();

    //Вывести сообщение о невозможности экипировать предмет
    return Padding(
        padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: smallMainTextStyle.apply(color: Colors.red),
        ));
  }
}
