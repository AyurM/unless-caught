import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/widgets/items/item_inventory_icon.dart';
import 'package:flutter/material.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

class ItemsSection extends StatelessWidget {
  final double _itemIconSize = 75.0;
  final Character character;

  const ItemsSection({Key key, this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!character.isHired) return SizedBox();

    List<Widget> clothSlivers = [];
    _buildInventorySection(clothSlivers, ItemSlot.head);
    _buildInventorySection(clothSlivers, ItemSlot.body);
    _buildInventorySection(clothSlivers, ItemSlot.hand);

    List<Widget> miscSlivers = [];
    _buildInventorySection(miscSlivers, ItemSlot.backpack);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(Strings.clothes,
              style: infoScreenSubHeaderTextStyle.apply(color: Colors.white70)),
        ),
        _buildScrollView(context, clothSlivers),
        Text(Strings.misc,
            style: infoScreenSubHeaderTextStyle.apply(color: Colors.white70)),
        _buildScrollView(context, miscSlivers),
      ],
    );
  }

  Widget _buildScrollView(BuildContext context, List<Widget> slivers) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: _itemIconSize + 8,
        child: CustomScrollView(
            scrollDirection: Axis.horizontal, slivers: slivers),
      ),
    );
  }

  void _buildInventorySection(List<Widget> slivers, ItemSlot slot) {
    List<Item> items = character.items.getItemsWithSlots(slot);

    slivers.add(SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return ChangeNotifierProvider<Item>.value(
        value: items[index],
        child: ItemInventoryIcon(
          character: character,
          size: _itemIconSize,
        ),
      );
    }, childCount: items.length)));

    //Добавить пустые иконки для незанятых слотов
    slivers.add(SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return EmptyInventoryIcon(slot: slot, size: _itemIconSize);
    }, childCount: ItemCollection.characterSlotLimits[slot] - items.length)));
  }
}
