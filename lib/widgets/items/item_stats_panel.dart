import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class ItemStatsPanel extends StatelessWidget {
  final double _skillIconSize = 24.0;
  final Item item;

  const ItemStatsPanel({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (item is Equipment) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_buildItemName(), _buildItemStats()],
      );
    } else
      return _buildItemName();
  }

  Widget _buildItemName() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(item.name,
          textAlign: TextAlign.center, style: listItemSubHeaderTextStyle),
    );
  }

  Widget _buildItemStats() {
    List<Widget> widgets = (item as Equipment)
        .stats
        .keys
        .map((skill) => _buildItemStat(skill))
        .toList();
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: widgets,
    );
  }

  Widget _buildItemStat(Skill skill) {
    int value = (item as Equipment).stats[skill];
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Container(
            width: _skillIconSize,
            height: _skillIconSize,
            child: Image.asset(skillIconPath[skill],
                color: skillColor[skill], fit: BoxFit.cover),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Text((value > 0 ? "+" : "") + value.toString(),
              style: smallSkillTextStyle.apply(
                  color: value > 0 ? goodUITextColor : badUITextColor)),
        ),
      ],
    );
  }
}
