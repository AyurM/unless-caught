import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/items/item_stats_panel.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class ItemListItem extends StatelessWidget {
  final double _itemPictureSize = 64.0;
  final bool showPrice;

  ItemListItem(this.showPrice);

  @override
  Widget build(BuildContext context) {
    Item item = Provider.of<Item>(context);
    return Card(
      elevation: 2,
      margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildItemPicture(item),
          Expanded(
            child: ItemStatsPanel(item: item),
          ),
          if (showPrice) _buildItemPrice(item),
        ],
      ),
    );
  }

  Widget _buildItemPicture(Item item) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          width: _itemPictureSize,
          height: _itemPictureSize,
          decoration: BoxDecoration(
              border: Border.all(width: 3, color: _getBorderColor(item))),
          child: Image.asset(item.imagePath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _buildItemPrice(Item item) {
    if (item.price == 0) return SizedBox();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Icon(moneyIcon, color: moneyIconColor),
          ),
          Text(item.price.toString(),
              style: smallButtonTextStyle.apply(color: Colors.black))
        ],
      ),
    );
  }

  Color _getBorderColor(Item item) {
    if (item is ClassEquipment) {
      if (item.requiredClasses.isNotEmpty)
        return skillColor[mainSkillMap[item.requiredClasses[0]]];
    }

    return mainBackgroundColor;
  }
}
