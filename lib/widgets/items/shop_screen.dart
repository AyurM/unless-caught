import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/widgets/items/item_list_item.dart';
import 'package:crime_game/widgets/items/items_pageview.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class ShopScreen extends StatelessWidget {
  const ShopScreen();

  @override
  Widget build(BuildContext context) {
    ItemShop shop = Provider.of<ItemShop>(context);
    List<Item> items = shop.items.items;
    items.sort((a, b) => a.price.compareTo(b.price));
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Colors.cyan[900],
                  Color(0xFF3c0d42),
                  Color(0xFF1f1054)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.3, 1.2, 1.5])),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            ScreenHeader(title: shop.name, color: Colors.cyan[900]),
            Expanded(
              child: Container(
                  child: ListView.builder(
                      padding: const EdgeInsets.all(0),
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        Item item = items[index];
                        return ChangeNotifierProvider.value(
                            value: item,
                            child: GestureDetector(
                                onTap: () =>
                                    _onItemTap(context, shop, items, index),
                                child: ItemListItem(true)));
                      })),
            ),
          ],
        ),
      ),
    );
  }

  void _onItemTap(
      BuildContext context, ItemShop shop, List<Item> items, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ItemsPageView(items, index, shop: shop)));
  }
}
