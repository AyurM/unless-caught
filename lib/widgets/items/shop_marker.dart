import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/widgets/items/shop_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class ShopMarker extends StatelessWidget {
  final double _markerSize = 64.0;
  final double _iconSize = 36.0;

  @override
  Widget build(BuildContext context) {
    ItemShop shop = Provider.of<ItemShop>(context);
    return Positioned(
      top: shop.position.top,
      left: shop.position.left,
      child: GestureDetector(
          onTap: () => _onMarkerTap(context, shop),
          child: Stack(children: <Widget>[
            Icon(FontAwesomeIcons.mapMarker, color: Colors.cyan, size: _markerSize),
            _buildShopIcon(shop)
          ])),
    );
  }

  Widget _buildShopIcon(ItemShop shop) {
    return Positioned(
      top: 12,
      left: 12,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          width: _iconSize,
          height: _iconSize,
          child: Image.asset(shop.imagePath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  void _onMarkerTap(BuildContext context, ItemShop itemShop) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider.value(
                value: itemShop, child: ShopScreen())));
  }
}