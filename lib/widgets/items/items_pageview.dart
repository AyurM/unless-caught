import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/widgets/items/item_info_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ItemsPageView extends StatelessWidget {
  final List<Item> items;
  final int index;
  final ItemShop shop;
  final Character owner;

  const ItemsPageView(this.items, this.index, {this.shop, this.owner});

  @override
  Widget build(BuildContext context) {
    PageController _controller = PageController(initialPage: index);
    return Scaffold(
      body: Container(
        child: PageView.builder(
            itemBuilder: (context, position) {
              return ChangeNotifierProvider<Item>.value(
                  value: items[position],
                  child: ItemInfoScreen(shop: shop, owner: owner));
            },
            itemCount: items.length,
            controller: _controller),
      ),
    );
  }
}
