import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/items/item_pool.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

///Экран сведений о предмете. Также используется для покупки предмета из
///магазина [shop] или продажи из хранилища/инвентаря персонажа [owner]
class ItemInfoScreen extends StatelessWidget {
  final ItemShop shop;
  final Character owner;
  final double _pictureSize = 96.0;
  final double _skillIconSize = 32.0;

  const ItemInfoScreen({Key key, this.shop, this.owner}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Item item = Provider.of<Item>(context);
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
          body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Utils.getItemColor(item),
                  Color(0xFF3c0d42),
                  Color(0xFF1f1054)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.3, 1.2, 1.5])),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            ScreenHeader(
                title: item.name,
                color: Utils.getItemColor(item),
                onClose: () => _onClose(context)),
            _buildItemPicture(item),
            Divider(height: 1.5),
            _buildRequirements(item),
            _buildStats(item),
            Expanded(child: SizedBox()),
            if (shop != null)
              _buildBuyUI(context, item)
            else
              _buildSellUI(context, item)
          ],
        ),
      )),
    );
  }

  Widget _buildItemPicture(Item item) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          width: _pictureSize,
          height: _pictureSize,
          child: Image.asset(item.imagePath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _buildRequirements(Item item) {
    if (item is ClassEquipment) {
      List<Widget> widgets = item.requiredClasses
          .map((rClass) => Card(
                elevation: 2,
                color: Color(0xfff2f2f2),
                margin: const EdgeInsets.symmetric(vertical: 2, horizontal: 6),
                shape: RoundedRectangleBorder(
                    side:
                        BorderSide(color: Utils.getItemColor(item), width: 2.0),
                    borderRadius: BorderRadius.circular(0.0)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Container(
                    width: double.infinity,
                    child: Text(specNameMap[rClass],
                        textAlign: TextAlign.center,
                        style: appBarStatTextStyle.apply(
                            color: skillColor[mainSkillMap[rClass]])),
                  ),
                ),
              ))
          .toList();
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(Strings.requirements,
                style:
                    infoScreenSubHeaderTextStyle.apply(color: Colors.white70)),
          ),
          ...widgets,
          Divider(height: 1.5)
        ],
      );
    }
    return SizedBox();
  }

  Widget _buildStats(Item item) {
    if (item is Equipment) {
      List<Widget> skills = [];
      for (final Skill skill in item.stats.keys) {
        skills.add(_buildSkillStat(item, skill, item.stats[skill]));
      }

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(Strings.stats,
                style:
                    infoScreenSubHeaderTextStyle.apply(color: Colors.white70)),
          ),
          ...skills
        ],
      );
    }

    return SizedBox();
  }

  Widget _buildSkillStat(Item item, Skill skill, int value) {
    return Card(
      elevation: 2,
      color: Color(0xfff2f2f2),
      margin: const EdgeInsets.symmetric(vertical: 2, horizontal: 6),
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Utils.getItemColor(item), width: 2.0),
          borderRadius: BorderRadius.circular(0.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                width: _skillIconSize,
                height: _skillIconSize,
                child: Image.asset(skillIconPath[skill],
                    color: skillColor[skill], fit: BoxFit.cover),
              ),
            ),
            Expanded(
              child:
                  Text(skillNameMap[skill], style: listItemSubHeaderTextStyle),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text((value > 0 ? "+" : "") + value.toString(),
                  style: infoScreenSkillTextStyle.apply(
                      color: value > 0 ? goodUITextColor : badUITextColor)),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBuyUI(BuildContext context, Item item) {
    Gang gang = Provider.of<Gang>(context);
    ItemPurchaseState state = shop.canBePurchased(item, gang.money);

    return Padding(
        padding: const EdgeInsets.all(16),
        child: ListTile(
          leading: Icon(moneyIcon, color: moneyIconColor, size: 28),
          title: Text(
            item.price.toString(),
            style: mainButtonTextStyle.apply(color: Colors.white),
          ),
          trailing: ElevatedButton(
            style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.all(16),
                primary: mainButtonColor,
                onPrimary: mainButtonColor,
                onSurface: state == ItemPurchaseState.purchased
                    ? skipButtonColor
                    : Colors.grey,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                )),
            onPressed: state == ItemPurchaseState.affordable
                ? () => _onBuyClick(item, gang)
                : null,
            child: Text(
                state == ItemPurchaseState.purchased
                    ? Strings.bought
                    : Strings.buy + " (${gang.money})",
                style: smallButtonTextStyle),
          ),
        ));
  }

  Widget _buildSellUI(BuildContext context, Item item) {
    ItemStore store = Provider.of<ItemStore>(context);
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.all(16),
            primary: mainButtonColor,
            onPrimary: mainButtonColor,
            onSurface: skipButtonColor,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            )),
        onPressed:
            store.isItemSold(item) ? null : () => _onSellClick(context, item),
        child: Text(
            store.isItemSold(item)
                ? Strings.sold
                : Strings.sell + " (${item.price ~/ Item.sellCoefficient})",
            style: smallButtonTextStyle),
      ),
    );
  }

  void _onBuyClick(Item item, Gang gang) {
    shop.buyItem(item);
    gang.addMoney(-item.price);
    gang.itemStore.addItem(item);
  }

  void _onSellClick(BuildContext context, Item item) {
    Gang gang = Provider.of<Gang>(context);
    ItemStore store = Provider.of<ItemStore>(context);

    if (owner != null) owner.unequip(item);

    //пометить предмет, как проданный
    store.sellItem(item);

    gang.addMoney(item.price ~/ Item.sellCoefficient);
    //вернуть предмет в первый попавшийся магазин
    ItemPool itemPool = Provider.of<ItemPool>(context);
    itemPool.shops[0].addItem(item);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    _onClose(context);
    return Future<bool>.value(true);
  }

  void _onClose(BuildContext context) {
    //завершить возможные покупки/продажи
    if (shop != null) shop.finishShopping();
    ItemStore store = Provider.of<ItemStore>(context, listen: false);
    store.finishSelling();
  }
}
