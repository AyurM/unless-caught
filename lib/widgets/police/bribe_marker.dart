import 'package:crime_game/model/police/bribe.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/police/bribe_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class BribeMarker extends StatelessWidget {
  final double _markerSize = 64.0;
  final double _iconSize = 35.0;

  @override
  Widget build(BuildContext context) {
    Bribe bribe = Provider.of<Bribe>(context);

    return Positioned(
        top: bribe.position.top,
        left: bribe.position.left,
        child: GestureDetector(
            onTap: () => _onMarkerTap(context, bribe),
            child: Stack(children: <Widget>[
              Icon(FontAwesomeIcons.mapMarker,
                  color: policeColor, size: _markerSize),
              _buildBribeIcon()
            ])));
  }

  Widget _buildBribeIcon() {
    return Positioned(
      top: 8,
      left: 13,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          width: _iconSize,
          height: _iconSize,
          child: Image.asset(dirtyCopsBribeMarkerPath, fit: BoxFit.cover),
        ),
      ),
    );
  }

  void _onMarkerTap(BuildContext context, Bribe bribe) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider.value(
                value: bribe, child: BribeScreen())));
  }
}
