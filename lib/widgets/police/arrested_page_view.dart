import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/widgets/police/arrested_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ArrestedPageView extends StatelessWidget {
  final List<ArrestedCharacter> characters;
  final int index;
  const ArrestedPageView(this.characters, this.index);

  @override
  Widget build(BuildContext context) {
    PageController _controller =
        PageController(initialPage: index, keepPage: false);
    return Scaffold(
      body: Container(
        child: PageView.builder(
            itemBuilder: (context, position) {
              return ChangeNotifierProvider<ArrestedCharacter>.value(
                  value: characters[position], child: ArrestedScreen());
            },
            itemCount: characters.length,
            controller: _controller),
      ),
    );
  }
}
