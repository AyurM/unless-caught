import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/character_decoration.dart';
import 'package:crime_game/widgets/characters/inventory_panel.dart';
import 'package:crime_game/widgets/characters/mission_counter_panel.dart';
import 'package:crime_game/widgets/characters/skill_panel.dart';
import 'package:crime_game/widgets/characters/spec_panel.dart';
import 'package:crime_game/widgets/police/bail_panel.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

///Экран арестованного персонажа, состоящий из основного экрана со статистикой
///и дополнительного экрана инвентаря
class ArrestedScreen extends StatelessWidget {
  final double _pictureSize = 128.0;

  @override
  Widget build(BuildContext context) {
    ArrestedCharacter arrested = Provider.of<ArrestedCharacter>(context);
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
          body: SlidingUpPanel(
              minHeight: 48,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
              panel: ChangeNotifierProvider<Character>.value(
                value: arrested.character,
                child: InventoryPanel(),
              ),
              body: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: CharacterDecoration.getCharacterDecoration(
                    arrested.character),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    ScreenHeader(
                        title: arrested.character.name,
                        color: skillColor[mainSkillMap[
                            arrested.character.specializations[0]]],
                        onClose: () => _onClose(context)),
                    _buildPictureAndBailPanel(context, arrested),
                    Divider(height: 1.5),
                    SpecPanel(character: arrested.character),
                    Divider(height: 1.5),
                    SkillPanel(character: arrested.character),
                    Divider(height: 1.5),
                    MissionCounterPanel(character: arrested.character),
                  ],
                ),
              ))),
    );
  }

  Widget _buildPictureAndBailPanel(
      BuildContext context, ArrestedCharacter arrested) {
    return Stack(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CharacterAvatarBig(
                character: arrested.character,
                pictureSize: _pictureSize,
                showItems: false),
            if (arrested.character.isArrested)
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  BailPanel(
                      bail: arrested.bail,
                      textColor: Colors.white,
                      isColumn: false),
                  _buildBailButton(context, arrested)
                ],
              )
            else
              Padding(
                padding: const EdgeInsets.all(8),
                child: Text(Strings.bailed,
                    textAlign: TextAlign.center,
                    style: infoScreenHeaderTextStyle),
              )
          ],
        ),
      ],
    );
  }

  Widget _buildBailButton(BuildContext context, ArrestedCharacter arrested) {
    Gang gang = Provider.of<Gang>(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 4,
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
            primary: mainButtonColor,
            onPrimary: mainButtonColor,
            onSurface: Colors.grey,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            )),
        child: Text(Strings.bailOut + "\n(${gang.money})",
            textAlign: TextAlign.center, style: smallButtonTextStyle),
        onPressed: arrested.canBeBailedOut(gang.money)
            ? () => _onBailButtonClick(context, arrested)
            : null,
      ),
    );
  }

  void _onBailButtonClick(BuildContext context, ArrestedCharacter arrested) {
    Police police = Provider.of<Police>(context);
    Gang gang = Provider.of<Gang>(context);
    police.releaseCharacter(arrested, gang);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    _onClose(context);
    return Future<bool>.value(true);
  }

  void _onClose(BuildContext context) {
    Police police = Provider.of<Police>(context);
    police.finishReleasing();
  }
}
