import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/police/bribe.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/wanted_indicator.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BribeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Bribe bribe = Provider.of<Bribe>(context);

    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          ScreenHeader(title: Strings.bribe, color: policeColor),
          _buildPicture(),
          if (!bribe.done) ..._buildBribeInProgressUI(context, bribe),
          if (bribe.done) ..._buildBribeDoneUI()
        ],
      ),
    );
  }

  Widget _buildPicture() {
    return AspectRatio(
      aspectRatio: 2.3,
      child: Container(
        width: double.infinity,
        child: Image.asset(dirtyCopsBribeImagePath, fit: BoxFit.cover),
      ),
    );
  }

  List<Widget> _buildBribeInProgressUI(BuildContext context, Bribe bribe) {
    return <Widget>[
      _buildCharacterList(bribe),
      Expanded(child: SizedBox()),
      _buildBribeCost(context, bribe),
    ];
  }

  List<Widget> _buildBribeDoneUI() {
    return <Widget>[
      Expanded(child: SizedBox()),
      Padding(
        padding: const EdgeInsets.all(8),
        child: Text(Strings.bribeDone,
            textAlign: TextAlign.center, style: sectionHeaderTextStyle),
      ),
      Expanded(child: SizedBox()),
    ];
  }

  Widget _buildCharacterList(Bribe bribe) {
    List<Character> characters = bribe.characters;
    return Container(
      height: 305,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Scrollbar(
          child: ListView.builder(
              padding: const EdgeInsets.all(0),
              itemCount: characters.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () => bribe.onCharacterSelect(index),
                  child: ChangeNotifierProvider<Character>.value(
                    value: characters[index],
                    child: CharacterBribeItem(
                        character: characters[index],
                        wantedReduction: bribe.wantedReductions[index],
                        price: bribe.wantedReductions[index] *
                            Bribe.creditsPerWantedPoint,
                        isSelected: bribe.selected.contains(characters[index])),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget _buildBribeCost(BuildContext context, Bribe bribe) {
    Gang gang = Provider.of<Gang>(context);
    return Padding(
        padding: const EdgeInsets.all(16),
        child: ListTile(
          leading: Icon(moneyIcon, color: moneyIconColor, size: 28),
          title: Text(
            bribe.totalBribe.toString(),
            style: infoScreenSkillTextStyle,
          ),
          trailing: ElevatedButton(
            style: ElevatedButton.styleFrom(
                elevation: 4,
                padding: const EdgeInsets.all(16),
                primary: mainButtonColor,
                onPrimary: mainButtonColor,
                onSurface: Colors.grey,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                )),
            onPressed: !bribe.canBeBribed() ? null : () => bribe.bribe(),
            child: Text(Strings.bribe + " (${gang.money})",
                style: smallButtonTextStyle),
          ),
        ));
  }
}

class CharacterBribeItem extends StatelessWidget {
  static const double _defaultPictureSize = 80.0;
  static const double _defaultIconSize = 24.0;

  final Character character;
  final bool isSelected;
  final int wantedReduction;
  final int price;
  final double pictureSize;
  final double skillIconSize;
  const CharacterBribeItem(
      {@required this.character,
      @required this.wantedReduction,
      @required this.price,
      this.isSelected = false,
      this.pictureSize = _defaultPictureSize,
      this.skillIconSize = _defaultIconSize});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 4,
        margin: const EdgeInsets.all(4.0),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: isSelected ? goodUITextColor : Colors.white, width: 2.0),
            borderRadius: BorderRadius.circular(4.0)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CharacterAvatarBig(
              character: character,
              pictureSize: pictureSize,
              showFrame: true,
            ),
            Expanded(child: SizedBox()),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 6),
                  child:
                      Text(character.name, style: listItemSubHeaderTextStyle),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: WantedIndicator(
                      value: character.wantedLevel, size: 48, fontSize: 14),
                ),
              ],
            ),
            Expanded(child: SizedBox()),
            _buildBribeOffer()
          ],
        ));
  }

  Widget _buildBribeOffer() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: _defaultIconSize,
                  height: _defaultIconSize,
                  child: Image.asset(wantedIconPath,
                      color: wantedIconColor, fit: BoxFit.cover),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Text(
                    "-" + wantedReduction.toString(),
                    style: infoScreenSkillTextStyle,
                  ),
                )
              ],
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(moneyIcon, color: moneyIconColor),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Text(
                  price.toString(),
                  style: infoScreenSkillTextStyle,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
