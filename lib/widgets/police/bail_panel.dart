import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';

class BailPanel extends StatelessWidget {
  final Bail bail;
  final Color textColor;
  final bool isColumn;

  BailPanel(
      {@required this.bail,
      this.textColor = Colors.black,
      this.isColumn = true});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: isColumn ? _buildBailColumn() : _buildBailRow(),
    );
  }

  Widget _buildBailColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _buildBailItem(Strings.bailAmount, bail.bail,
            Icon(moneyIcon, color: moneyIconColor, size: 20)),
        _buildBailItem(Strings.bailDeadline, bail.missionsLeft,
            Icon(durationIcon, color: durationIconColor, size: 20)),
      ],
    );
  }

  Widget _buildBailRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _buildBailItem(Strings.bailAmount, bail.bail,
            Icon(moneyIcon, color: moneyIconColor, size: 20)),
        _buildBailItem(Strings.bailDeadline, bail.missionsLeft,
            Icon(durationIcon, color: durationIconColor, size: 20)),
      ],
    );
  }

  Widget _buildBailItem(String text, int value, Widget widget) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[_buildBailText(text), _buildBailValue(value, widget)],
    );
  }

  Widget _buildBailText(String text) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(text,
          textAlign: TextAlign.center,
          style: listItemSubHeaderTextStyle.apply(color: textColor)),
    );
  }

  Widget _buildBailValue(int value, Widget widget) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        widget,
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(
            value.toString(),
            style: infoScreenSkillTextStyle.apply(color: textColor),
          ),
        )
      ],
    );
  }
}
