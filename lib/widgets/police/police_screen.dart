import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/police/arrested_list_item.dart';
import 'package:crime_game/widgets/police/arrested_page_view.dart';
import 'package:crime_game/widgets/screen_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PoliceScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Police police = Provider.of<Police>(context);

    if (police.arrested.isEmpty) return _buildEmptyUI();

    return Scaffold(
        body: Container(
      decoration: _buildBackground(),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          ScreenHeader(title: Strings.police, color: policeColor),
          Expanded(
              child: Container(child: _buildArrestedList(police.arrested))),
        ],
      ),
    ));
  }

  Widget _buildArrestedList(List<ArrestedCharacter> arrested) {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      itemCount: arrested.length,
      itemBuilder: (BuildContext context, int index) {
        return ChangeNotifierProvider.value(
            value: arrested[index],
            child: GestureDetector(
                onTap: () => _onArrestedItemClick(context, arrested, index),
                child: ArrestedListItem()));
      },
    );
  }

  Widget _buildEmptyUI() {
    return Scaffold(
      body: Container(
        decoration: _buildBackground(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            ScreenHeader(title: Strings.police, color: policeColor),
            Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(Strings.noArrested,
                  textAlign: TextAlign.center,
                  style: sectionHeaderBoldTextStyle.apply(color: Colors.white)),
            ),
            Expanded(child: SizedBox()),
          ],
        ),
      ),
    );
  }

  BoxDecoration _buildBackground() {
    return BoxDecoration(
        image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                policeColor.withOpacity(1.0), BlendMode.hardLight),
            image: ExactAssetImage(policeBackgroundPath),
            fit: BoxFit.cover));
  }

  void _onArrestedItemClick(
      BuildContext context, List<ArrestedCharacter> characters, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ArrestedPageView(characters, index)));
  }
}
