import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/police/police_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PoliceMarker extends StatelessWidget {
  final double _iconSize = 48.0;

  @override
  Widget build(BuildContext context) {
    Police police = Provider.of<Police>(context);

    return Positioned(
      top: police.position.top,
      left: police.position.left,
      child: GestureDetector(
          onTap: () => _onMarkerTap(context, police),
          child: _buildPoliceIcon()),
    );
  }

  Widget _buildPoliceIcon() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      child: Container(
        width: _iconSize,
        height: _iconSize,
        child: Image.asset(policeMarkerPath, fit: BoxFit.cover),
      ),
    );
  }

  void _onMarkerTap(BuildContext context, Police police) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider.value(
                value: police, child: PoliceScreen())));
  }
}