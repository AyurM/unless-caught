import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/police/police_event.dart';
import 'package:crime_game/model/police/police_event_handler.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:crime_game/styles.dart';
import 'package:crime_game/widgets/characters/character_modal_item.dart';
import 'package:flutter/material.dart';

class PoliceEventModal extends StatelessWidget {
  final PoliceEventHandler eventHandler;
  final PoliceEvent event;

  PoliceEventModal(this.eventHandler) : this.event = eventHandler.event;

  static const double _maxModalWidth = 400.0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(),
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: _maxModalWidth),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: 280),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.white),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      _buildTitle(),
                      _buildPicture(),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(Strings.arrestInfo,
                            textAlign: TextAlign.center,
                            style: sectionHeaderTextStyle),
                      ),
                      Divider(height: 1.5),
                      _buildArrestedList(context),
                      Expanded(child: SizedBox()),
                      _buildDoneButton(context)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: policeColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(Strings.arrest,
            textAlign: TextAlign.center, style: listItemMissionNameTextStyle),
      ),
    );
  }

  Widget _buildPicture() {
    return AspectRatio(
      aspectRatio: 2.0,
      child: Container(
        width: double.infinity,
        child: Image.asset(policeArrestPath, fit: BoxFit.cover),
      ),
    );
  }

  Widget _buildDoneButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
              primary: mainButtonColor,
              onPrimary: mainButtonColor,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              )),
          child: Text(Strings.done, style: mainButtonTextStyle),
          onPressed: () {
            eventHandler.dismiss();
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  Widget _buildArrestedList(BuildContext context) {
    List<Character> characters =
        event.arrested.map((aChar) => aChar.character).toList();
    return Container(
      height: 210,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Scrollbar(
          child: ListView.builder(
              itemCount: characters.length,
              itemBuilder: (BuildContext context, int index) {
                return CharacterModalItem(character: characters[index]);
              }),
        ),
      ),
    );
  }

  //Нажатие на кнопку "Назад" должно корректно завершать событие
  Future<bool> _onBackPressed() {
    eventHandler.dismiss();
    return Future<bool>.value(true);
  }
}
