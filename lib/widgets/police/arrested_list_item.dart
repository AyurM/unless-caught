import 'package:crime_game/model/police/police.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:crime_game/widgets/characters/character_avatar_big.dart';
import 'package:crime_game/widgets/characters/character_name_panel.dart';
import 'package:crime_game/widgets/characters/small_skill_panel.dart';
import 'package:crime_game/widgets/police/bail_panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crime_game/styles.dart';

class ArrestedListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ArrestedCharacter arrested = Provider.of<ArrestedCharacter>(context);
    return Card(
      elevation: 2,
      margin: const EdgeInsets.all(6),
      shape: RoundedRectangleBorder(
          side: BorderSide(
              color: skillColor[
                  mainSkillMap[arrested.character.specializations[0]]],
              width: 2.0),
          borderRadius: BorderRadius.circular(6.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CharacterNamePanel(character: arrested.character),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CharacterAvatarBig(character: arrested.character),
              SmallSkillPanel(character: arrested.character),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: BailPanel(bail: arrested.bail),
              )
            ],
          ),
        ],
      ),
    );
  }
}
