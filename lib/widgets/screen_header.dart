import 'package:crime_game/styles.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ScreenHeader extends StatelessWidget {
  final String title;
  final Color color;
  final Function onClose;

  const ScreenHeader({Key key, this.title, this.color, this.onClose}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: color,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(child: _buildName()),
            _buildCloseButton(context)
          ],
        ),
      ),
    );
  }

  Widget _buildCloseButton(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 32.0, 16.0, 0.0),
        child: IconButton(
          icon: Icon(FontAwesomeIcons.times, color: buttonTextColor),
          onPressed: () {
            if(onClose != null)
              onClose();
            Navigator.pop(context);
          }
        ));
  }

  Widget _buildName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 32.0, 16.0, 0.0),
      child: Text(title,
          textAlign: TextAlign.center, style: infoScreenHeaderTextStyle),
    );
  }
}
