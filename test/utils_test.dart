import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/utils/utils.dart';
import 'package:test/test.dart';

void main() {
  test('main skills are mapped correctly', () {
    List<Specialization> specs = [Specialization.driver, Specialization.hacker];
    List<Skill> skills = Utils.getMainSkills(specs);
    expect(skills, [Skill.driving, Skill.hacking]);
  });
}