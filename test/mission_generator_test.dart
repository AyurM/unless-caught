import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_generator.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:test/test.dart';

void main() {
  MissionGenerator generator;
  MissionPool missionPool;

  setUp((){
    generator = MissionGenerator();
    missionPool = MissionPool([], []);
  });

  tearDown((){
    generator = null;
    missionPool = null;
  });

  test('generate easy mission without skill requirements', () {
    Mission mission = generator.createMission(0, missionPool);
    expect(mission.requirements.keys.length, 2);
    expect(mission.requiredCharacters, 2);
  });

  test('generate medium mission without skill requirements', () {
    Mission mission = generator.createMission(1, missionPool);
    //print(mission.toString());
    expect(mission.requirements.keys.length, 3);
    expect(mission.requiredCharacters, 3);
  });

  test('generate hard mission without skill requirements', () {
    Mission mission = generator.createMission(2, missionPool);
    expect(mission.requirements.keys.length, 4);
    expect(mission.requiredCharacters, 4);
  });

  test('generate hard mission without complete skill requirements', () {
    List<Skill> skills = [Skill.hacking, Skill.speech];
    Mission mission = generator.createMission(2, missionPool, skills);

    expect(mission.requirements.keys.contains(Skill.hacking), true);
    expect(mission.requirements.keys.contains(Skill.speech), true);
    expect(mission.requirements.keys.length, 4);
    expect(mission.requiredCharacters, 4);
  });

  test('generate initial missions', () {
    Specialization mainSpec = Specialization.driver;
    List<Specialization> specs = [
      Specialization.burglar,
      Specialization.gunfighter,
      Specialization.hacker
    ];
    List<Mission> missions = generator.createInitialMissions(mainSpec, specs, missionPool);
    expect(missions.length, 3);
  });
}
