import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/game_world.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/missions/mission.dart';
import 'package:crime_game/model/missions/mission_pool.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/missions/mission_reward.dart';
import 'package:crime_game/strings/strings.dart';
import 'package:test/test.dart';

void main() {
  Mission mission;
  GameWorld gameWorld;
  MissionPool missionPool;

  setUp(() {
    gameWorld = GameWorld.newGame();
    missionPool = gameWorld.missionPool;
    mission = Mission.generate(
        Strings.drivingLockpickMission,
        null,
        0,
        -10,
        MissionReward(credits: 100, xp: 5, wantedLevel: 3),
        null,
        Duration(seconds: 10),
        {Skill.driving: 30, Skill.lockpick: 25},
        {Skill.driving: 0.15, Skill.lockpick: 0.3},
        [],
        missionPool);
  });

  tearDown(() {
    mission = null;
    gameWorld = null;
  });

  test('cant start mission without characters', () {
    expect(mission.isReadyToStart, false);
  });

  test('cant start mission without required skills', () {
    Character char1 = Character("char1", {Skill.driving: 20},
        [Specialization.driver], ItemCollection(items: []), 50, 0);
    Character char2 = Character("char2", {Skill.hacking: 40},
        [Specialization.hacker], ItemCollection(items: []), 100, 0);

    mission.addCharacter(char1);
    expect(mission.isReadyToStart, false);
    mission.addCharacter(char2);
    expect(mission.isReadyToStart, false);
  });

  test('cant start mission without required number of characters', () {
    Character char1 = Character(
        "char1",
        {Skill.driving: 30, Skill.lockpick: 40},
        [Specialization.driver],
        ItemCollection(items: []),
        50,
        0);

    mission.addCharacter(char1);
    expect(mission.isReadyToStart, false);
  });

  test('can start mission with required skills', () {
    Character char1 = Character("char1", {Skill.driving: 20},
        [Specialization.driver], ItemCollection(items: []), 50, 0);
    Character char2 = Character("char2", {Skill.lockpick: 40},
        [Specialization.burglar], ItemCollection(items: []), 100, 0);

    mission.addCharacter(char1);
    expect(mission.isReadyToStart, false);
    mission.addCharacter(char2);
    expect(mission.isReadyToStart, true);
  });

  test('mission starts and completes correctly', () {
    int xp = 5;
    Character main = Character("main", {Skill.hacking: 30},
        [Specialization.hacker], ItemCollection(items: []), 50, 0);
    Character char1 = Character("char1", {Skill.driving: 20},
        [Specialization.driver], ItemCollection(items: []), 50, 0);
    Character char2 = Character("char2", {Skill.lockpick: 40},
        [Specialization.burglar], ItemCollection(items: []), 100, 0);
    gameWorld.gang.mainCharacter = main;

    mission.addCharacter(char1);
    mission.addCharacter(char2);

    mission.start();
    expect(missionPool.available.length, 0);
    expect(missionPool.inProgress.length, 1);
    expect(char1.isBusy, true);
    expect(char2.isBusy, true);

    mission.complete();
    expect(char1.completedMissions[0], 1);
    expect(char2.completedMissions[0], 1);
    expect(char1.completedMissions[1], 0);
    expect(char2.completedMissions[1], 0);
    expect(char1.completedMissions[2], 0);
    expect(char2.completedMissions[2], 0);
    expect(char1.isBusy, false);
    expect(char2.isBusy, false);
    expect(char1.availableXp, xp);
    expect(char2.availableXp, xp);
    expect(char1.wantedLevel, 3);
    expect(char2.wantedLevel, 3);

    expect(missionPool.inProgress.length, 0);
    expect(missionPool.completedMissions[0], 1);
    expect(missionPool.completedMissions[1], 0);
    expect(missionPool.completedMissions[2], 0);
    expect(gameWorld.gang.money, Gang.startMoney + 100);
  });

  test('skill penalty calculation', () {
    //При расчетах должно использоваться максимальное значение навыка lockpick
    Character char1 = Character("char1", {Skill.driving: 40},
        [Specialization.driver], ItemCollection(items: []), 50, 0);
    Character char2 = Character("char2", {Skill.lockpick: 30},
        [Specialization.burglar], ItemCollection(items: []), 100, 0);
    Character char3 = Character("char3", {Skill.lockpick: 25},
        [Specialization.burglar], ItemCollection(items: []), 100, 0);

    mission.addCharacter(char1);
    mission.addCharacter(char2);
    mission.addCharacter(char3);
    expect(mission.duration, Duration(milliseconds: 8500));
  });

  test('max skill penalty calculation', () {
    Character char1 = Character("char1", {Skill.driving: 50},
        [Specialization.driver], ItemCollection(items: []), 50, 0);
    Character char2 = Character("char2", {Skill.lockpick: 75},
        [Specialization.burglar], ItemCollection(items: []), 100, 0);

    mission.addCharacter(char1);
    mission.addCharacter(char2);
    expect(mission.duration, Duration(milliseconds: 5500));
  });

  test('if mission is not ready to start duration equals base duration', () {
    Character char1 = Character("char1", {Skill.driving: 40},
        [Specialization.driver], ItemCollection(items: []), 50, 0);

    mission.addCharacter(char1);
    expect(mission.isReadyToStart, false);
    expect(mission.duration, Duration(seconds: 10));
  });

  test('no progress if mission is not started', () async {
    await Future.delayed(Duration(milliseconds: 300), () {});
    expect(mission.getMissionProgress(), 0.0);
  });
}
