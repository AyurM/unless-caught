import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/character_generator.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:test/test.dart';

void main() {
  test('generate initial characters', () {
    CharacterGenerator generator = CharacterGenerator();
    Specialization mainSpec = Specialization.driver;
    List<Character> characters = generator.createInitialCharacters(mainSpec);
    expect(characters.length, 4);
    expect(characters.where((c) => c.specializations.contains(mainSpec)).length,
        0);
  });
}
