import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/items/item_shop.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:test/test.dart';

void main() {
//  ItemShop shop;
//  ItemStore store;
//
//  setUp(() {
//    shop = ItemShop(null, null, null, ItemCollection<Item>(items: []));
//    store = ItemStore(ItemCollection<Item>(items: []));
//  });
//
//  tearDown((){
//    store = null;
//    shop = null;
//  });

  test('Purchased items are removed from the shop after shopping is finished',
      () {
    ItemShop shop = ItemShop(null, null, null, ItemCollection<Item>(items: []));
    ClassEquipment testItem1 =
        ClassEquipment([], {}, ItemSlot.head, "testName1", null, price: 100);
    ClassEquipment testItem2 =
        ClassEquipment([], {}, ItemSlot.head, "testName2", null, price: 80);
    ClassEquipment testItem3 =
        ClassEquipment([], {}, ItemSlot.head, "testName3", null, price: 50);
    shop.addItem(testItem1);
    shop.addItem(testItem2);
    shop.addItem(testItem3);
    expect(shop.items.items.length, 3);
    shop.buyItem(testItem1);
    shop.buyItem(testItem3);
    expect(shop.items.items.length, 3);
    shop.finishShopping();
    expect(shop.items.items.length, 1);
    expect(shop.items.items.contains(testItem1), false);
    expect(shop.items.items.contains(testItem3), false);
  });

  test('Sold items are removed from the store after selling is finished', () {
    ItemStore store = ItemStore(ItemCollection<Item>(items: []));
    ClassEquipment testItem1 =
        ClassEquipment([], {}, ItemSlot.head, "testName1", null, price: 100);
    ClassEquipment testItem2 =
        ClassEquipment([], {}, ItemSlot.head, "testName2", null, price: 80);
    ClassEquipment testItem3 =
        ClassEquipment([], {}, ItemSlot.head, "testName3", null, price: 50);
    store.addItem(testItem1);
    store.addItem(testItem2);
    store.addItem(testItem3);
    expect(store.items.items.length, 3);
    store.sellItem(testItem1);
    store.sellItem(testItem3);
    expect(store.items.items.length, 3);
    store.finishSelling();
    expect(store.items.items.length, 1);
    expect(store.items.items.contains(testItem1), false);
    expect(store.items.items.contains(testItem3), false);
  });
}
