import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/gang.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:crime_game/model/items/item_store.dart';
import 'package:test/test.dart';

void main() {
  Gang gang;
  Character char1, char2;

  setUp(() {
    gang = Gang(Gang.startMoney, [], [], ItemStore(ItemCollection(items: [])));
    char1 = Character(
        "char1", {}, [Specialization.driver], ItemCollection(items: []), 50, 0);
    char2 = Character(
        "char2", {}, [Specialization.hacker], ItemCollection(items: []), 80, 0);
  });

  tearDown(() {
    gang = null;
    char1 = null;
    char2 = null;
  });

  test('create main character and initial characters', () {
    gang.createMainCharacter(Specialization.gunfighter);
    expect(gang.forHire.length, 4);
  });

  test('hire character', () {
    Character main = Character("main", {}, [Specialization.gunfighter],
        ItemCollection(items: []), 50, 0);
    gang.mainCharacter = main;
    gang.addCharacterForHire(char1);
    gang.addCharacterForHire(char2);
    gang.hireCharacter(char1);

    expect(gang.money, Gang.startMoney - 50);
    expect(gang.members.length, 1);
    expect(gang.members.contains(char1), true);
    expect(gang.forHire.length, 1);
    expect(gang.forHire.contains(char1), false);
    expect(gang.forHire.contains(char2), true);
  });
}
