import 'package:crime_game/model/characters/character.dart';
import 'package:crime_game/model/characters/skill.dart';
import 'package:crime_game/model/characters/specialization.dart';
import 'package:crime_game/model/items/item.dart';
import 'package:crime_game/model/items/item_collection.dart';
import 'package:test/test.dart';

void main() {
  Character character;

  setUp(() {
    character = Character("character", {Skill.driving: 20, Skill.guns: 40},
        [Specialization.driver], ItemCollection<Equipment>(items: []), 100, 0);
  });

  tearDown(() {
    character = null;
  });

  test('cannot upgrade skill without xp', () {
    character.upgradeSkill(Skill.driving);
    expect(character.getSkillValue(Skill.driving), 20);
  });

  test('main skill upgraded by mainSkillUpgradeAmount value', () {
    character.addXp(3);
    character.upgradeSkill(Skill.driving);
    expect(character.getSkillValue(Skill.driving),
        20 + Character.mainSkillUpgradeAmount);
    expect(character.availableXp, 2);
  });

  test('secondary skill upgraded by 1', () {
    character.addXp(1);
    character.upgradeSkill(Skill.guns);
    expect(character.getSkillValue(Skill.guns), 41);
  });

  test('if character doesnt have skill, 0 is returned', () {
    expect(character.getSkillValue(Skill.hacking), 0);
  });

  test('cant be hired without money', () {
    expect(character.canBeHired(99), false);
  });

  test('can hire', () {
    expect(character.canBeHired(100), true);
  });

  test('equipping item adds stats', () {
    ClassEquipment testItem1 = ClassEquipment([], {
      Skill.driving: 5,
      Skill.guns: -1,
      Skill.hacking: 7
    }, ItemSlot.head, "testName1", null, price: 100);
    character.equip(testItem1);
    expect(character.items.items.length, 1);
    //Навык Skill.driving увеличен на 5
    expect(character.skills[Skill.driving], 25);
    expect(character.getSkillChangeDegree(Skill.driving), 1);
    //Навык Skill.guns уменьшен на 1
    expect(character.skills[Skill.guns], 39);
    expect(character.getSkillChangeDegree(Skill.guns), -1);
    //У персонажа изначально нет навыка "Хакерство", поэтому предмет не добавляет этот навык
    expect(character.skills.keys.contains(Skill.hacking), false);
    expect(character.getSkillChangeDegree(Skill.hacking), 0);
  });

  test('unequipping item removes stats', () {
    ClassEquipment testItem1 = ClassEquipment([], {
      Skill.driving: 5,
      Skill.guns: -5,
      Skill.hacking: 7
    }, ItemSlot.head, "testName1", null, price: 100);
    character.equip(testItem1);
    character.unequip(testItem1);
    expect(character.items.items.isEmpty, true);
    expect(character.skills[Skill.driving], 20);
    expect(character.skills[Skill.guns], 40);
  });

  test('can equip item if slot is empty', () {
    ClassEquipment testItem1 = ClassEquipment([], {
      Skill.driving: 5,
      Skill.guns: -5,
      Skill.hacking: 7
    }, ItemSlot.head, "testName1", null, price: 100);
    //проверка специализации
    expect(testItem1.canBeEquippedBy(character), true);
    //проверка свободного слота
    expect(character.hasFreeSlot(testItem1), true);
    character.equip(testItem1);
    expect(character.items.items.contains(testItem1), true);

    //предмет для другого слота
    ClassEquipment testItem2 = ClassEquipment(
        [], {Skill.hacking: 7}, ItemSlot.backpack, "testName2", null,
        price: 100);
    expect(testItem2.canBeEquippedBy(character), true);
    expect(character.hasFreeSlot(testItem2), true);
    character.equip(testItem2);
    expect(character.items.items.contains(testItem2), true);
  });

  test('cannot equip item if slot is not empty', () {
    ClassEquipment testItem1 = ClassEquipment([], {
      Skill.driving: 5,
      Skill.guns: -5,
      Skill.hacking: 7
    }, ItemSlot.hand, "testName1", null, price: 100);
    character.equip(testItem1);

    ClassEquipment testItem2 = ClassEquipment([], {
      Skill.driving: 10,
    }, ItemSlot.hand, "testName2", null, price: 100);
    //проверка специализации
    expect(testItem2.canBeEquippedBy(character), true);
    //проверка свободного слота (слот занят)
    expect(character.hasFreeSlot(testItem2), false);
    character.equip(testItem2);
    expect(character.items.items.contains(testItem2), false);
  });

  test('if character is arrested, then inventory is cleared', () {
    ClassEquipment testItem1 = ClassEquipment([], {
      Skill.driving: 5,
    }, ItemSlot.hand, "testName1", null, price: 100);
    character.equip(testItem1);

    ClassEquipment testItem2 = ClassEquipment([], {
      Skill.guns: 10,
    }, ItemSlot.backpack, "testName2", null, price: 100);
    character.equip(testItem2);

    character.onArrest();
    expect(character.items.items.length, 0);
    expect(character.skills[Skill.driving], 20);
    expect(character.skills[Skill.guns], 40);
  });

  test('min wanted level is 0', () {
    character.wantedLevel = 5;
    character.addWanted(-10);
    expect(character.wantedLevel, 0);
  });

  test('max wanted level is maxWantedLevel', () {
    character.wantedLevel = 95;
    character.addWanted(10);
    expect(character.wantedLevel, Character.maxWantedLevel);
  });
}
